#!/bin/bash
#-----------------------------------------------------------------------
#
# This script removes old plugin builds so we can make NEW plugin builds
#
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh

#-------------Check script input ---------------------------------------
function usage {
    echo "Usage: remove_old_builds.sh -n "
    echo "       -n   Name: Name of the plugin to be removed. No file extensions, the script will "
    echo "                  find and destroy all versions."
    echo "       e.g.:  remove_old_builds.sh -n Weaponiser"
    exit 1
}

while getopts ":n:" opt; do
    case ${opt} in
    n ) # Plugin name
        PLUGIN_NAME="$OPTARG"
        ;;
    \? ) echo "Invalid option."
        usage
        ;;
    : )
        echo "Parameter required for -$OPTARG"
        usage
        ;;
    * ) echo "no"
        usage
    esac
done
shift $((OPTIND -1))

if [[ -z "$PLUGIN_NAME" ]]; then
    echo "No plugin name given."
    usage
fi
#----Functions to remove files--------------------------------------------

function remove_files_Mac {
    echo_and_run rm -rf "/Users/krotos/Library/Audio/Plug-Ins/VST/$1.vst"
    echo_and_run rm -rf "/Users/krotos/Library/Audio/Plug-Ins/VST3/$1.vst3"
    echo_and_run rm -rf "/Users/krotos/Library/Audio/Plug-Ins/Components/$1.component"
    echo_and_run rm -rf "/Library/Application Support/Avid/Audio/Plug-Ins/$1.aaxplugin"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.vst"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.vst3"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.component"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.aaxplugin"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.vst"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.vst3"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.component"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH/$1.aaxplugin"
    echo_and_run rm -rf "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH/"*.txt
}

function remove_files_Win {
    if [[ "$JOB_ARCH" == "64" ]]; then
        echo_and_run rm -rf "/cygdrive/c/Program Files/Steinberg/VstPlugins/$1".dll  
    else
        echo_and_run rm -rf "/cygdrive/c/Program Files (x86)/Steinberg/VstPlugins/$1".dll  
    fi
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.aaxplugin"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.dll"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.aaxdll"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.vst3"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.aaxplugin"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.dll"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.aaxdll"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"$1.vst3"
    echo_and_run rm -rf "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/"*.txt"

}

#----Main part of the script---------------------------------------------
set_system_var
set_job_arch_var
set_job_project_var
set_job_project_version_var


remove_files_"$SYSTEM" "$PLUGIN_NAME"






