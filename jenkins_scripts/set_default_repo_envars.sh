#!/bin/bash
#-----------------------------------------------------------------------
#
#   This script will setup environment variables for setup_repos.sh
#
#   source this script from jenkins before running setup_repos.sh
#-----------------------------------------------------------------------

set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh


set_job_project_var

case "$JOB_PROJECT" in
        "Dehumaniser")
            echo "Setting up default variables for Dehumaniser"
            export JUCE_TAG="4.0.2"
            export JCF_BRANCH="DHII-build-with-logs"
            ;;
        "Simple Monsters")
            echo "Setting up default variables for Simple Monsters"
            export JUCE_TAG="4.0.2"
            export JCF_BRANCH="DHII-build-with-logs"
            ;;
        "Analysis Tool")
            export JUCE_TAG="4.3.0"
            export JCF_BRANCH="develop"
            ;;
        "Reformer Pro")
            echo "Setting up default variables for Reformer Pro"
            export JUCE_TAG="4.3.1"
            export KROTOS_JUCE_BRANCH="develop"
            export JCF_BRANCH="develop"
            export KROTOS_MODULES_TAG="ReformerPro"
            export KROTOS_WAVETABLES_BRANCH="develop"
        ;;
        "Reformer")
            echo "Setting up default variables for Reformer"
            export JUCE_TAG="4.3.1"
            export JCF_TAG="REFv1.1.0"
            export KROTOS_JUCE_BRANCH="develop"
        ;;
        "Weaponiser")
            echo "Setting up default variables for Weaponiser"
            export JUCE_TAG="4.3.1"
            export JCF_BRANCH="develop"
            export ASF_BRANCH="Weaponiser"
            export KROTOS_MODULES_TAG="Weaponiser"
            export KROTOS_WAVETABLES_BRANCH="develop" 
            export KROTOS_JUCE_BRANCH="develop"
        ;;
        "KrotosHost")
        	echo "Setting up default variables for KrotosHost"
        	export JUCE_BRANCH="master"
        ;;
        "Igniter")
            echo "Setting up default variables for Igniter"
            export JUCE_TAG="5.3.2"
        ;;
        *)
        echo "error: unknown project: $JOB_PROJECT"
        exit 1
esac

