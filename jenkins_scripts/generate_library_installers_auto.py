#!/usr/bin/env python
###############################################################
#
#   This script 
#
###############################################################
import os
import argparse
import sys
import library_creation_definitions as libdefs

current_dir = os.path.dirname(os.path.realpath(__file__))
if sys.platform == "darwin":
    insertdir = os.path.join(current_dir , "../installer_scripts_mac")
    sys.path.insert(0,insertdir)
    import bundle_installer_definitions as bun_inst
elif sys.platform == "win32":
    insertdir = os.path.join(current_dir , "..\installer_scripts_win")
    sys.path.insert(0,insertdir)
    import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("bundle_name", help="Name for the installer")
parser.add_argument("library_root", help="Full path to the folder containing libraries")
parser.add_argument("output_path", help="Where the installer will be delivered")

#parser.add_argument("-l","--list", action='append', help="List of suitable libraries")
args = parser.parse_args()

bundle_name = args.bundle_name
output_path    = args.output_path
library_root   = libdefs.get_samples_path(args.library_root)
libraries = libdefs.get_library_list(library_root)

if sys.platform == "win32":
    print library_root + " is now"
    library_root = library_root.replace('/', '\\')
    print library_root

print "****************************"
print "Creating installer for " + args.bundle_name + "..."

print "Inlcuding libraries:"
for str in libraries:
    print os.path.basename(str)
###############################################################
#
#   Script starts here
#
###############################################################
output_path = libdefs.get_output_path(output_path)
if not os.path.exists(output_path):
    print "ERROR: Output path does not exist. " + output_path
    exit(1)

print "\n****************************"
print "Generating installer:"


if sys.platform == "darwin":
    bun_inst.create_bundle_auto(bundle_name, libdefs.get_samples_path_suffix(library_root), libraries, output_path)
elif sys.platform == "win32":
    bun_inst.create_inno_script(bundle_name)
    for str in libraries:
        str = str.replace('/', '\\')
        bun_inst.append_inno_script(bundle_name,libdefs.get_samples_path_suffix(library_root),str)
    bun_inst.compile_installer(bundle_name)
    bun_inst.sign_installer(bundle_name)
    bun_inst.move_to_dropbox(bundle_name, output_path)
    