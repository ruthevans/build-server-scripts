rem :::  usage: make_installer.bat path/to/innoscript.iss

Set INNO_SCRIPT=%~1

Set INSTALLER_PATH=C:\Dev\Documents\%PROJECT_NAME% Installers\Installer %VERSION% %ARCHITECTURE%
md "%INSTALLER_PATH%"

rem Create folders to copy installers
set INSTALLER_DESKTOP_FOLDER=C:\Users\Krotos\Desktop\Jenkins Installers\%PROJECT_NAME%
mkdir "%INSTALLER_DESKTOP_FOLDER%"

rem Move wrapped dll 
copy "%WRAPPED_FOLDER_PATH%\%PROJECT_NAME%.dll" "%INSTALLER_PATH%\%PROJECT_NAME%.dll" /y
call :AssertSuccess %errorlevel% "Copy of %PROJECT_NAME%.dll to %INSTALLER_PATH% folder"

rem Move wrapped aax
xcopy  /i /s /y "%WRAPPED_FOLDER_PATH%\%PROJECT_NAME%.aaxplugin" "%INSTALLER_PATH%\%PROJECT_NAME%.aaxplugin"
call :AssertSuccess %errorlevel% "Copy of %PROJECT_NAME%.aaxplugin folder to %INSTALLER_PATH% folder"

rem Make installer and override output
"C:\Program Files (x86)\Inno Setup 5\iscc.exe" "%INNO_SCRIPT%"
call :AssertSuccess %errorlevel% "Make installer"



exit /B 0
rem End of script

:AssertSuccess
:: %1 is the error level
:: %2 is the message to display
set SUCCEED_CODE=0
if not %1 == %SUCCEED_CODE% (
echo %2 failed
rem exit %1
)
echo %2 succeeded
echo.
rem exit /B 0