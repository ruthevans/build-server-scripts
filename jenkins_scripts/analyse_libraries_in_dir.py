#!/usr/bin/env python
###############################################################
#
#   This script 
#
###############################################################
import os
import argparse
import subprocess
import library_creation_definitions as libdefs

parser = argparse.ArgumentParser()
parser.add_argument("library_root", help="Full path to the folder containing libraries")
args = parser.parse_args()

library_root = args.library_root
library_root = libdefs.get_samples_path(library_root)

reformer_libraries_path="/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"

# get the path
print "Looking for libraries in folder " + library_root + "..."

# list the subpaths
librarylist=libdefs.get_library_list(library_root)

print "\n****************************\nLibraries to analyse:"
if not librarylist:
    print "ERROR: No suitable libraries were found"
    exit(1)
for str in librarylist:
    print os.path.basename(str)

# extra: test that dropbox is up to date


# run analysis
print "\n****************************\nStarting analysis:"
FNULL = open(os.devnull, 'w')
for str in librarylist:
    print str
    analysisargs = "OfflineAnalysisCommandLineTool \"" + str + "\" false 44100 88200 48000 96000 176400 192000"
    returncode = subprocess.call(analysisargs, stdout=FNULL, stderr=FNULL, shell=True)
    if not returncode == 0:
        print "ERROR: Problem found with the offline analysis tool.\nError code:", returncode
        exit(2)
    if not os.path.isdir(os.path.join(reformer_libraries_path, str)):
        print "ERROR: Offline analysis tool failed to analyse library"
        exit(3)
    

    #extra: A progress bar here would be nice maybe

# Fix databases!
# The offline analysis tools creates databases with a d infront of them to show they are decrypted. We need to remove these Ds
print "\n****************************\nRunning database fixes:"
for str in librarylist:
    library=os.path.basename(str)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/44100/D_Clustered_44100_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/44100/Clustered_44100_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/48000/D_Clustered_48000_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/48000/Clustered_48000_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/88200/D_Clustered_88200_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/88200/Clustered_88200_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/96000/D_Clustered_96000_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/96000/Clustered_96000_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/176400/D_Clustered_176400_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/176400/Clustered_176400_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)
    wrong_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/192000/D_Clustered_192000_256.db")
    right_name=os.path.join("/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries", library, "Clustering/192000/Clustered_192000_256.db")
    if os.path.isfile(wrong_name):
        os.rename(wrong_name, right_name)