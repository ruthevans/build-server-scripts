#!/usr/local/bin/python3.6
# (This dumb ol' dropbox API has to be on python 3 but is not currently compatible with python 3.7)
#
#   Script: is_dropbox_folder_current.py
#
#   Description:  Exits 0 if the folder given is up to date with dropbox
#   Usage:        is_dropbox_folder_current.py "/Development/Reformer/Reformer Libraries/Krotos/Examples/Example library/Samples"
#
import argparse
import platform
import dropbox
import os

parser = argparse.ArgumentParser()
parser.add_argument("dropboxpath", help="Path to the dropbox folder to be checked")
args = parser.parse_args()

# The dropbox path must start with a '/' but we're not expecting the given path to have one
online_folder="/" + args.dropboxpath

if (platform.system()=="Windows"):
    dropbox_prefix="B:\\Dropbox (Krotos)"
elif(platform.system()=="Darwin"):
    dropbox_prefix="/Volumes/Transcend/Dropbox (Krotos)"
    
offline_folder=dropbox_prefix + online_folder

# This is from dropbox and will grant this script access to our dropbox
access_token="tXhT_WyDNLAAAAAAAAADVLbYnKsYzWjOVVB2lRUzczqS1qi9UPuLyPqf4I_AHOR1"
dbx = dropbox.Dropbox(access_token)
current=True
recursive=True
for entry in dbx.files_list_folder(online_folder, recursive).entries:
    # This gets the size of the file but i don't think we need that actually
    #online_size=entry.size
    test_path = dropbox_prefix + entry.path_display
    if not os.path.exists(test_path):
        print("Warning: file does not exist locally: ", test_path)
        current=False
if(current is False):
    print("ERROR: The dropbox directory is NOT up to date.")
    exit(1)
else:
    print("This directory is up to date :)")
    exit(0)