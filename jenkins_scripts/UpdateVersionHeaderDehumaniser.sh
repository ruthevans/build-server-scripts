#!/bin/bash
echo "Building file"


if [ -f Source/DehumaniserVersion.h ]
then
echo "Removing previous file"
rm Source/DehumaniserVersion.h
fi

gitDataFile="Source/DehumaniserVersion.h"


echo "Get Information from system"

# Date and time that we are running this build
buildDate=`date "+%F %H:%M:%S"`
echo buildDate: $buildDate

# Last hash from the current branch
lastCommitHashTemp=`git log --pretty=format:"%h" -1`
if [ -n "$lastCommitHashTemp" ]
then
lastCommitHash=$lastCommitHashTemp
else
lastCommitHash=""
fi

echo lastCommitHash: $lastCommitHash

# Date and time of the last commit on this branch
lastCommitDateTemp=`git log --pretty=format:"%ad" --date=short -1`
if [ -n "$" ]
then
lastCommitDate=$lastCommitDateTemp
else
lastCommitDate=""
fi

echo lastCommitDate: $lastCommitDate

# Comment from the last commit on this branch
lastCommitCommentTemp=`git log --pretty=format:"%s" -1`
if [ -n "$" ]
then
lastCommitComment=$lastCommitCommentTemp
else
lastCommitComment=""
fi

echo lastCommitComment: $lastCommitComment

# Current branch in use
currentBranchTemp=`git rev-parse --abbrev-ref HEAD`
if [ -n "$currentBranchTemp" ]
then
currentBranch=$currentBranchTemp
else
currentBranch=""
fi

# Last tag applied to this branch
lastRepoTagTemp=`git describe --abbrev=0 --tags`
if [ -n "$lastRepoTagTemp" ]
then
lastRepoTag=$lastRepoTagTemp
else
lastRepoTag="0.0.0"
fi

echo lastRepoTag: $lastRepoTag

# Git describe string
lastDescribeTemp=`git describe`
if [ -n "$lastDescribeTemp" ]
then
lastDescribe=$lastDescribeTemp
else
lastDescribe="0.0.0"
fi

echo lastDescribe: $lastDescribe

# Build the file with all the information in it
echo "Create header file"

echo "/*" >> $gitDataFile
echo "  ==============================================================================" >> $gitDataFile
echo "" >> $gitDataFile
echo " DehumaniserVersion.h" >> $gitDataFile
echo " Auto generated on: $buildDate" >> $gitDataFile
echo "" >> $gitDataFile
echo "  ==============================================================================" >> $gitDataFile
echo "*/" >> $gitDataFile
echo "" >> $gitDataFile
echo "" >> $gitDataFile
echo "" >> $gitDataFile
echo "#define BUILD_DATE              \"$buildDate\"" >> $gitDataFile
echo "#define GIT_CURRENT_BRANCH      \"$currentBranch\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_HASH    \"$lastCommitHash\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_DATE    \"$lastCommitDate\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_COMMENT \"$lastCommitComment\"" >> $gitDataFile
echo "#define GIT_LAST_REPO_TAG       \"$lastRepoTag\"" >> $gitDataFile
echo "#define AUTOVERSION             $lastRepoTag" >> $gitDataFile

echo "" >> $gitDataFile
echo "" >> $gitDataFile

echo "#define DH_VERSION_STRING       \"$lastDescribe\"" >> $gitDataFile

echo "" >> $gitDataFile
echo "" >> $gitDataFile
