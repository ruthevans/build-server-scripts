#!/bin/bash
#-----------------------------------------------------------------------
# Copies plugins to dropbox
# Plugins should be pre-wrapped before this script is called.
# PROJECT, JOB_PROJECT_VERSION and ARCH should be set. BUILD_NUMBER and BUILD_TAG will be set by jenkins.
# The script should be run from the workspace so that the git commands get the correct details.
#
#       ./copy_plugins_to_dropbox.sh
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh 

#-----------------------------------------------------------------------
# Set up variables for the rest of the script
function setup_variables {
    set_system_var
    set_repo_tag
    set_repo_branch
    set_repo_commit
    set_project_name
    if [[ "$SYSTEM" == "Mac" ]]; then
        # Mac build server detected
        LATEST_DIR="LatestMacBuilds"
        DROPBOX="/Users/krotos/Dropbox (Krotos)"
        JENKINS_BUILDS="/Users/krotos/Desktop/Jenkins Builds/KrotosHost/unwrappedRelease"
        WRAPPED_JENKINS_BUILDS="/Users/krotos/Desktop/Jenkins Builds/KrotosHost/wrappedRelease"
        ZIP_COMMAND="/usr/bin/zip"
        ZIP_OPTIONS="-r"
    elif [[ "$SYSTEM" == "Win" ]]; then
        # Windows build server detected
        LATEST_DIR="LatestWindowsBuilds"
        DROPBOX="/cygdrive/c/Dropbox (Krotos)"
        JENKINS_BUILDS="/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/KrotosHost/unwrappedRelease"
        WRAPPED_JENKINS_BUILDS="/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/KrotosHost/wrappedRelease"
        ZIP_COMMAND="/cygdrive/c/Program Files/7-Zip/7z.exe"
        ZIP_OPTIONS="a -tzip"
        alias find=/cygdrive/c/cygwin64/bin/find.exe
    else
        echo "Unknown system detected."
        exit 1
    fi

}

#-----------------------------------------------------------------------
# Get the version/Last tag applied to this commit
function set_repo_tag {
    REPO_TAG=`git tag -l --points-at HEAD | sed 's:.*/::'`
#Sometimes there just isn't a tag ok
}

#-----------------------------------------------------------------------
# Get the version/Last tag applied to this branch
function set_repo_commit {
    GIT_COMMIT=`git log --pretty=format:"%h" -1`
    if [[ "$GIT_COMMIT" == "" ]];then
    	GIT_COMMIT="commit unknown"
    fi
}

#-----------------------------------------------------------------------
# Current branch in use
function set_repo_branch {
    REPO_BRANCH=`echo $GIT_BRANCH | sed 's/\//_/g'`
    if [[ "$REPO_BRANCH" == "" ]];then
    	REPO_BRANCH="branch unknown"
    fi
}

#-----------------------------------------------------------------------
# Dehumaniser and Simple monsters are a special case
function set_project_name {
    BUILD_REF="KrotosHost $REPO_TAG Build$BUILD_NUMBER"
}

#-----------------------------------------------------------------------
function copy_release_notes {
# See if there are release notes to include
    if [[ $SYSTEM == "Win" ]]; then
        /cygdrive/c/cygwin64/bin/find.exe . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"KrotosHost" \;
    else
        /usr/bin/find . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"KrotosHost" \;
    fi
}

#-----------------------------------------------------------------------
function zip_plugins {
    copy_release_notes

    # set up values before the copy starts
    BUILD_REF="$JOB_PROJECT Plugins $REPO_TAG Build$BUILD_NUMBER"
    LAST_ARCHIVE="$JOB_PROJECT Plugins"

    create_build_details
    echo_and_run cd "$WRAPPED_JENKINS_BUILDS"
    echo_and_run "$ZIP_COMMAND" $ZIP_OPTIONS "$BUILD_REF.zip" *.aaxplugin *.dll *.vst *.component *.txt

}

#-----------------------------------------------------------------------
# Create build_details.txt
function create_build_details {
	mkdir -p "$WRAPPED_JENKINS_BUILDS"
    printf "Build date:"                           >  "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    date                                           >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nJenkins build job: $JOB_BASE_NAME"   >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nJenkins build number: $BUILD_NUMBER" >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit commit: $GIT_COMMIT"             >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit branch: $GIT_BRANCH"             >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit tag: $REPO_TAG"                  >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
}

#-----------------------------------------------------------------------
# copy plugins to latest folder
function copy_plugins_to_latest {
    mkdir -p "$DROPBOX/Boiler Plate/Jenkins Builds"/"$LATEST_DIR"
    #copy plugins to latest
    rm -rf "$DROPBOX/Boiler Plate/Jenkins Builds/$LATEST_DIR/$LAST_ARCHIVE"*.zip
    echo_and_run cp -rf "$WRAPPED_JENKINS_BUILDS/$BUILD_REF.zip"  "$DROPBOX/Boiler Plate/Jenkins Builds/$LATEST_DIR"
    echo "Created $DROPBOX/Boiler Plate/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip"
}

#-----------------------------------------------------------------------
# copy plugins to the archive
    function copy_plugins_to_archive {
    mkdir -p "$DROPBOX/Boiler Plate/Jenkins Builds/Archive/$SYSTEM/"
    echo_and_run cp -rf "$WRAPPED_JENKINS_BUILDS/$BUILD_REF.zip"  "$DROPBOX/Boiler Plate/Jenkins Builds/Archive/$SYSTEM/"
    echo "Created $DROPBOX/Boiler Plate/Jenkins Builds/Archive/$SYSTEM/$BUILD_REF.zip"
}

#-----------------------------------------------------------------------
# remove the zip file from wrapped dir
function cleanup {
    rm -rf "$WRAPPED_JENKINS_BUILDS"/"$BUILD_REF".zip
}
#-----------------------------------------------------------------------
#
function checkup {
    
    if [[ -e  "$DROPBOX/Boiler Plate/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip" ]] && \
       [[ -e  "$DROPBOX/Boiler Plate/Jenkins Builds/Archive/$SYSTEM/$BUILD_REF.zip" ]]; then
        echo "Everything is okay."
    else
        echo "Error: cannot confirm plugins were safely copied to dropbox at:"
        echo "$DROPBOX/Boiler Plate/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip"
        echo "$DROPBOX/Boiler Plate/Jenkins Builds/Archive/$SYSTEM/$BUILD_REF.zip"
        return 1
    fi
}

#-----------------------------------------------------------------------
setup_variables
zip_plugins
copy_plugins_to_latest
copy_plugins_to_archive
cleanup
checkup