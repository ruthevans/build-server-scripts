#-----------------------------------------------------------------------
# function:  wrap_binary 
#
# purpose: use to wrap binaries, see usage function for details
#-----------------------------------------------------------------------
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh
#-------------Check script input ---------------------------------------

function usage {
    echo "Usage: wrap_binary.sh -b <path to binary> -i <id/wcguid>"
    echo "       -b   Binary: Full path and name of the binary/plugin to be wrapped."
    echo "       -o   Output Full path including filename of the wrapped output (optional)"
    echo "       -i   ID  : GUID to use for wrapping"
    echo "       e.g.:  wrap_binary.sh -b \"C:\\Users\\Krotos\\AnalysisTool.exe\"  -i 58B4E230-8BCF-11E7-90EF-005056875CC3"
    exit 1
}

while getopts ":b:o:i:" opt; do
    case ${opt} in
    b ) # binary to wrap
        UNWRAPPED_INPUT="$OPTARG"
        ;;
    o ) # Output
        WRAPPED_OUTPUT="$OPTARG"
        ;;
    i ) # GUID to use
        WRAPTOOL_GUID=$OPTARG
        ;;
    \? ) echo "Invalid option."
        usage
        ;;
    : )
        echo "Parameter required for -$OPTARG"
        usage
        ;;
    * ) echo "no"
        usage
    esac
done
shift $((OPTIND -1))

if [[ -z $UNWRAPPED_INPUT ]] || [[ -z $WRAPTOOL_GUID ]]; then
    usage
fi
#-----------------------------------------------------------------------

function wrap_binary_Win {
    $WRAPTOOL wrap --verbose --account krotosltd --password 80439777Krotosilok \
    --keyfile "C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12" \
    --keypassword "KrotosHighConcept2019!" \
    --wcguid $WRAPTOOL_GUID \
    --in "$UNWRAPPED_INPUT" \
    --out "$WRAPPED_OUTPUT"
    $WRAPTOOL verify --in "$WRAPPED_OUTPUT"
}
function wrap_binary_Mac {
    $WRAPTOOL wrap --verbose --account krotosltd --signid "Krotos LTD" \
    --wcguid $WRAPTOOL_GUID \
    --in "$UNWRAPPED_INPUT" \
    --out "$WRAPPED_OUTPUT"
    $WRAPTOOL verify --in "$WRAPPED_OUTPUT"
}

#-------------'Main' code starts here ---------------------------------------
if [[ -z $WRAPPED_OUTPUT ]]; then WRAPPED_OUTPUT="$UNWRAPPED_INPUT"; fi
if [[ -z $SYSTEM ]]; then set_system_var; fi
if [[ -z $WRAPTOOL ]]; then set_wraptool_var; fi

if [[ ! -e "$UNWRAPPED_INPUT" ]]; then
	echo "Error: Cannot find binary at: $UNWRAPPED_INPUT"
	exit 1
fi

verify_plugin_is_wrapped "$UNWRAPPED_INPUT"
if [[ $? == 0 ]]; then
    echo "This binary has already been wrapped and wrap_binary.sh has better things to do than to rewrap it."
else
    wrap_binary_$SYSTEM
fi

