#!/bin/bash
#-----------------------------------------------------------------------
#
# This script triggers a build of the given jenkins job
#
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh

#-------------Check script input ---------------------------------------
function usage {
    echo "Usage: build_job.sh -j 'Reformer/Installers/Reformer Installer 64bit' "
    echo "       -j   Name: Full path to the jenkins job to build."
    echo "       e.g.:  build_job.sh -j 'Reformer/Installers/Reformer Installer 64bit'"
    exit 1
}

while getopts ":j:" opt; do
    case ${opt} in
    j ) 
        JOB_PATH=$OPTARG
        ;;
    \? ) echo "Invalid option."
        usage
        ;;
    : )
        echo "Parameter required for -$OPTARG"
        usage
        ;;
    * ) echo "no"
        usage
    esac
done
shift $((OPTIND -1))

if [[ -z $JOB_PATH ]]; then
    echo "No plugin name given."
    usage
fi



set_system_var
if [[ "$SYSTEM" == "Mac" ]]; then
	java -jar /Users/krotos/.jenkins/war/WEB-INF/jenkins-cli.jar -auth ruthevans:113b87898b69760864517209fa6030c637 -s http://localhost:8080/ build "$JOB_PATH"
elif [[ "$SYSTEM" == "Win" ]]; then
	java -jar "C:\Users\Jenkins\AppData\Local\Jenkins\war\WEB-INF\lib\cli-2.278.jar" -s http://localhost:8080/ build "$JOB_PATH"
else
	echo "Error: unknown OS detected"
fi
