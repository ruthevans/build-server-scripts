#!/usr/bin/env python
###############################################################
#
#   This script overwrites the postbuildCommand in the given
#   jucer file. Plugins will be copied only.
#
################################################################
import platform
import argparse
import xml.etree.ElementTree as elementTree
import os.path

####variables #################################################
parser = argparse.ArgumentParser()
parser.add_argument("jucer_file", help="Path to the jucer file to overwrite")
args = parser.parse_args()
jucer_file = args.jucer_file


current_system=platform.system()
################################################################
def set_postbuildcommand_windows(xml_file):
    tree = elementTree.parse(jucer_file)
    root = tree.getroot()
    for configuration in root.iter('CONFIGURATION'):
        #remove any post build commands
        configuration.set('postbuildCommand', "")
        #also turn off copy step which generates post build commands
        configuration.set('enablePluginBinaryCopyStep', "0")

    tree.write(jucer_file)
################################################################
def set_postbuildcommand_mac(xml_file):
    tree = elementTree.parse(jucer_file)
    root = tree.getroot()
    for xcodemac in root.iter('XCODE_MAC'):
        xcodemac.set('postbuildCommand', "")
        print "Updated postbuildCommand for XCODE_MAC"
    tree.write(jucer_file)
################################################################

print "Updating jucer file for " + current_system + "..."
if not os.path.isfile(jucer_file):
    print "Error: Cannot find "+ jucer_file
    quit(1)

if current_system == "Windows":
    set_postbuildcommand_windows(jucer_file)
elif current_system == "Darwin":
    set_postbuildcommand_mac(jucer_file)
else:
    print "Error: unkown operating system."
    quit(2)
print("Finished removing post build commands.")