#!/bin/bash
#-----------------------------------------------------------------------
#
# This script sets up the correct repos for Jenkins
#
#-----------------------------------------------------------------------
unamestr=`uname`
echo "system: $unamestr"
if [[ "$unamestr" == "Darwin" ]]; then
    #Mac build server detected
    SDKS_AND_LIBS="/Users/krotos/Dev/SDKs and Libraries"
elif [[ "$unamestr" == "CYGWIN_NT-10.0" ]]; then
    #Windows build server detected
    SDKS_AND_LIBS="/cygdrive/c/Dev/SDKs and Libraries"
else
    echo "Unknown system detected."
    exit 1
fi

git config --global user.name "Ruth Evans"
git config --global user.email "ruth@krotosaudio.com"

#-----------------------------------------------------------------------
### set JUCE
if [[ $JUCE_BRANCH ]]
then
	echo "Setting JUCE to $JUCE_BRANCH"
	cd "$SDKS_AND_LIBS/JUCE"
	git reset --hard
	git checkout $JUCE_BRANCH
	git pull
fi

if [[ $JUCE_TAG ]]
then
	echo "Setting JUCE to $JUCE_TAG"
	cd "$SDKS_AND_LIBS/JUCE"
	git reset --hard
	git fetch --tags
	git checkout tags/$JUCE_TAG
fi


#-----------------------------------------------------------------------
### set KrotoJUCE
if [[ $KROTOS_JUCE_BRANCH ]]
then
	echo "Setting KrotosJUCE to $KROTOS_JUCE_BRANCH"
	cd "$SDKS_AND_LIBS/KrotosJUCE"
	git reset --hard
	git checkout  $KROTOS_JUCE_BRANCH
    git pull
fi

if [[ $KROTOS_JUCE_TAG ]]
then
	echo "Setting KrotosJUCE to $KROTOS_JUCE_TAG"
	cd "$SDKS_AND_LIBS/krotosjuce"
	git reset --hard
	git fetch --tags
	git checkout tags/$KROTOS_JUCE_TAG
fi

#-----------------------------------------------------------------------
# set jim-credland-libraries-and-examples 
if [[ $JCF_BRANCH ]]
then
	echo "Setting jim-credland-libraries-and-examples to $JCF_BRANCH"
	cd "$SDKS_AND_LIBS/jim-credland-libraries-and-examples"
	git reset --hard
	git checkout  $JCF_BRANCH
    git pull
fi

if [[ $JCF_TAG ]]
then
	echo "Setting jim-credland-libraries-and-examples to $JCF_TAG"
	cd "$SDKS_AND_LIBS/jim-credland-libraries-and-examples"
	git reset --hard
	git fetch --tags
	git checkout tags/$JCF_TAG
fi

#-----------------------------------------------------------------------
# set adamstarkaudioanalysisframework
if [[ $ASF_BRANCH ]]
then
	echo "Setting adamstarkaudioanalysisframework to $ASF_BRANCH"
	cd "$SDKS_AND_LIBS/adamstarkaudioanalysisframework"
	git reset --hard
	git checkout  $ASF_BRANCH
    git pull
fi

if [[ $ASF_TAG ]]
then
	echo "Setting adamstarkaudioanalysisframework to $ASF_TAG"
	cd "$SDKS_AND_LIBS/adamstarkaudioanalysisframework"
	git reset --hard
	git fetch --tags
	git checkout tags/$ASF_TAG
fi
#-----------------------------------------------------------------------
### set krotos_modules
if [[ $KROTOS_MODULES_BRANCH ]]
then
	echo "Setting krotos_modules to $KROTOS_MODULES_BRANCH"
	cd "$SDKS_AND_LIBS/krotos_modules"
	git reset --hard
	git checkout $KROTOS_MODULES_BRANCH
    git pull
fi

if [[ $KROTOS_MODULES_TAG ]]
then
	echo "Setting krotos_modules to $KROTOS_MODULES_TAG"
	cd "$SDKS_AND_LIBS/krotos_modules"
	git reset --hard
	git fetch --tags
	git checkout tags/$KROTOS_MODULES_TAG
fi


#-----------------------------------------------------------------------
### set krotos_wavetables
if [[ $KROTOS_WAVETABLES_BRANCH ]]
then
	echo "Setting krotos_wavetables to $KROTOS_WAVETABLES_BRANCH"
	cd "$SDKS_AND_LIBS/krotos_wavetables"
	git reset --hard
	git checkout $KROTOS_WAVETABLES_BRANCH
    git pull
fi

if [[ $KROTOS_WAVETABLES_TAG ]]
then
	echo "Setting krotos_wavetables to $KROTOS_WAVETABLES_TAG"
	cd "$SDKS_AND_LIBS/krotos_wavetables"
	git reset --hard
	git fetch --tags
	git checkout tags/$KROTOS_WAVETABLES_TAG
fi
#-----------------------------------------------------------------------
