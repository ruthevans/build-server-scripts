#!/bin/bash
#-----------------------------------------------------------------------
# Cleans up all bash scripts in the current folder to run on windows
# sometimes invisible unix line ends get into the file and windows hates them
#-----------------------------------------------------------------------
set -e #exit script on error
currdir="$(dirname "$BASH_SOURCE")"

for entry in "$currdir"/*
do
    if [[ "$entry" == *.sh ]]; then
        echo "cleaning $entry"
        sed -i 's/\r$//' "$entry"
    fi
done
echo "Done!"
echo "Have a nice day"
