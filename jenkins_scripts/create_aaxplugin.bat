rem :: Wrap and sign by call Pace Eden tool
Rem Set intermediate variables
set JENKINS_BUILD_PATH=C:\Users\Krotos\Desktop\Jenkins Builds
set WRAPPED_FOLDER_PATH=C:\Users\Krotos\Desktop\Jenkins Builds\%PROJECT_NAME%\wrapped%VERSION%%ARCHITECTURE%
set UNWRAPPED_FOLDER_PATH=C:\Users\Krotos\Desktop\Jenkins Builds\%PROJECT_NAME%\unwrapped%VERSION%%ARCHITECTURE%

rem Create folders for unwrapped and wrapped dll
mkdir "%WRAPPED_FOLDER_PATH%"
mkdir "%UNWRAPPED_FOLDER_PATH%"

set MISSING_PARAM=false
if [%ARCHITECTURE%]==[] (
    echo "Please define ARCHITECTURE"
    echo "e.g.: set ARCHITECTURE=64"
    set MISSING_PARAM=true
)
if ["%PROJECT_NAME%"]==[""] (
    echo "Please define PROJECT_NAME"
    echo "e.g.: set PROJECT_NAME=Dehumaniser"
    set MISSING_PARAM=true
)
if ["%PLUGIN%"]==[] (
    echo "Please define PLUGIN"
    echo "e.g.: set PLUGIN=WeaponiserOneShot"
    set MISSING_PARAM=true
)
if [%MISSING_PARAM%]==[true] (
    exit %1
)

if %ARCHITECTURE%==32 (
    set PLATFORM=Win32
)
if %ARCHITECTURE%==64 (
    set PLATFORM=x64
)

rem Go to wrapped builds folder
cd "%WRAPPED_FOLDER_PATH%"

rem Wrap the dll to output a aax file
set AAX_FOLDER_STRUCTURE=%PLUGIN%.aaxplugin\contents\%PLATFORM%


rem Create a directory for wrapped release defined by AAX folder structure
rmdir "%PLUGIN%".aaxplugin /S /Q
mkdir "%AAX_FOLDER_STRUCTURE%"

rem Copy the wrapped file .dllfile inside the aaxplugin folder and change its extension to .aaxplugin
copy "%WRAPPED_FOLDER_PATH%\%PLUGIN%.dll" "%WRAPPED_FOLDER_PATH%\%AAX_FOLDER_STRUCTURE%\%PLUGIN%.aaxplugin" /y 
call :AssertSuccess %errorlevel% "Copy of %PLUGIN%.aaxplugin file to %WRAPPED_FOLDER_PATH% folder"


exit /B 0
rem End of script

:AssertSuccess
:: %1 is the error level
:: %2 is the message to display
set SUCCEED_CODE=0
if not %1 == %SUCCEED_CODE% (
echo %2 failed
exit %1
)
echo %2 succeeded
echo.
exit /B 0