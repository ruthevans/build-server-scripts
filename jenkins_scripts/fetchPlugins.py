#!/usr/bin/python3
#============================================================
#  This build server script fetches plugins from their 
#  solutions/projects and dumps them into the given directory.
#  It will also sign aaxplugins so they can run if they are not wrapped
#
#  Usage: fetchPlugins.py -j Concept -p Concept -v 2017
#  note: project name and plugin name are usually the same but not always
#============================================================

from platform import system
from shutil import copy2
from shutil import copytree
from shutil import rmtree
import os
import argparse
import build_server_lib as BSTools

operatingSystem=system()
windows = "Windows"
osx = "Darwin"

#=====================================
# Set the usage arguments
parser = argparse.ArgumentParser()
parser.add_argument("-p","--pluginName", help="The name of the plugin", required=True)
parser.add_argument("-j","--project", help="The project name as used in jenkins job title", required=True)
#if operatingSystem == windows:
parser.add_argument("-v","--visualStudioVer", help="(WINDOWS ONLY) The version of visual studio used to build the plugin", choices=["2015","2017","2019"])
args = parser.parse_args()

#=====================================
# sign plugin 
def signPlugin(pluginPath, wcguid):
     wcguid = BSTools.getWCGUIDForPlugin(args.pluginName)
     if (wcguid == "NoIdFound"):
          print("WARNING: No WCGUID located for project " + args.project )
          print("The plugin cannot be signed and will not work in Pro Tools.")
          return

     print ("Signing plugin...")
     #depracated
     #returnCode = BSTools.signPlugin(pluginPath, wcguid)
     returnCode = BSTools.signPluginWithCustomerNum(pluginPath, wcguid)
     if returnCode == 0:
          print ("Signing successful")
     else:
          print("WARNING: Signing failed.")
          if (pluginPath.endswith("aaxplugin")):
               print ("This plugin will not work in Pro Tools.")
    
#=====================================
# get and sign files windows style
def fetchFilesWindows():
    if args.visualStudioVer == "":
        visualStudio="VisualStudio2019"
    else:
        visualStudio="VisualStudio" + args.visualStudioVer
    successfulSigning=0
    atLeastOnePluginFound=False
    #although plugins are not wrapped, the "wrapped" dir names are used for the sake of consistency
    outputDir = "B:\\Jenkins Builds\\" + args.project + "\\unwrappedRelease"
    signedDir = "B:\\Jenkins Builds\\" + args.project + "\\wrappedRelease"
    vsPath = "C:\\Dev\\Projects\\" + args.project + "\\Builds\\" + visualStudio + "\\x64\\Release"
    dllFilePath = os.path.join(vsPath,"VST", args.pluginName + ".dll")
    vst3FilePath = os.path.join(vsPath,"VST3\\" + args.pluginName + ".vst3")
    aaxFilePath = os.path.join(vsPath,"AAX\\" + args.pluginName + ".aaxplugin\\Contents\\x64\\" + args.pluginName + ".aaxplugin")
    #master config
    vsPathMaster = "C:\\Dev\\Projects\\" + args.project + "\\Builds\\" + visualStudio + "\\x64\\Master"
    dllFilePathMaster = os.path.join(vsPathMaster,"VST", args.pluginName + ".dll")
    vst3FilePathMaster = os.path.join(vsPathMaster,"VST3\\" + args.pluginName + ".vst3")
    aaxFilePathMaster = os.path.join(vsPathMaster,"AAX\\" + args.pluginName + ".aaxplugin\\Contents\\x64\\" + args.pluginName + ".aaxplugin")
    
    if not os.path.isdir(outputDir): os.makedirs(outputDir)
    # Remove any existing plugins
    jenkinsVstFilePath = os.path.join(outputDir,args.pluginName + ".dll")
    jenkinsVst3FilePath = os.path.join(outputDir,args.pluginName + ".vst3")
    jenkinsAaxFilePath = os.path.join(outputDir,args.pluginName + ".aaxplugin")
    if os.path.isfile(jenkinsVstFilePath):  os.remove(jenkinsVstFilePath)
    if os.path.isfile(jenkinsVst3FilePath): os.remove(jenkinsVst3FilePath)
    if os.path.isdir(jenkinsAaxFilePath): rmtree(jenkinsAaxFilePath)
    # Copy plugins into the target output directory if they exist
    print("Searching for " + args.pluginName + " plugins built in visual studio " + args.visualStudioVer )
    print ("Copying plugins to: " + outputDir)
    # vst plugin
    if os.path.isfile(dllFilePath):
        atLeastOnePluginFound=True
        print ("Copying " + dllFilePath)
        copy2(dllFilePath,outputDir)
        dllName=args.pluginName + ".dll"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, dllName),os.path.join(signedDir,dllName)) != successfulSigning):
            exit(1)
    if os.path.isfile(dllFilePathMaster):
        atLeastOnePluginFound=True
        print ("Copying " + dllFilePathMaster)
        copy2(dllFilePathMaster,outputDir)
        dllName=args.pluginName + ".dll"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, dllName),os.path.join(signedDir,dllName)) != successfulSigning):
            exit(1)
    # vst3 plugin
    if os.path.isfile(vst3FilePath):
        atLeastOnePluginFound=True
        print ("Copying " + vst3FilePath)
        copy2(vst3FilePath,outputDir)
        vst3Name=args.pluginName + ".vst3"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, vst3Name),os.path.join(signedDir,vst3Name)) != successfulSigning):
            exit(1)
    if os.path.isfile(vst3FilePathMaster):
        atLeastOnePluginFound=True
        print ("Copying " + vst3FilePathMaster)
        copy2(vst3FilePathMaster,outputDir)   
        vst3Name=args.pluginName + ".vst3"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, vst3Name),os.path.join(signedDir,vst3Name)) != successfulSigning):
            exit(1)
    # aax plugin
    if os.path.isfile(aaxFilePath):
        atLeastOnePluginFound=True
        print ("Copying " + aaxFilePath)
        internalAaxPath = os.path.join(outputDir, args.pluginName + ".aaxplugin\\Contents\\x64")
        os.makedirs(internalAaxPath)
        copy2(aaxFilePath, internalAaxPath)
        aaxName=args.pluginName + ".aaxplugin\\Contents\\x64\\" + args.pluginName + ".aaxplugin"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, aaxName),os.path.join(signedDir,aaxName)) != successfulSigning):
            exit(1)    
    if os.path.isfile(aaxFilePathMaster):
        atLeastOnePluginFound=True
        print ("Copying " + aaxFilePathMaster)
        internalAaxPath = os.path.join(outputDir, args.pluginName + ".aaxplugin\\Contents\\x64")
        os.makedirs(internalAaxPath)
        copy2(aaxFilePathMaster, internalAaxPath)
        aaxName=args.pluginName + ".aaxplugin\\Contents\\x64\\" + args.pluginName + ".aaxplugin"
        if (BSTools.signPluginWithCustomerNum(os.path.join(outputDir, aaxName),os.path.join(signedDir,aaxName)) != successfulSigning):
            exit(1)
    if (atLeastOnePluginFound == False):
        print("ERROR: No plugins were found! Please check parameters are correct")
        exit(2)
        

#=====================================
# get and sign files windows style
def fetchFilesOSX():
     outputdir = "/Users/krotos/Desktop/Jenkins Builds/" + args.project + "/wrappedRelease"
     vstFilePath  = os.path.join("/Users/krotos/Library/Audio/Plug-Ins/VST", args.pluginName + ".vst")
     vst3FilePath = os.path.join( "/Users/krotos/Library/Audio/Plug-Ins/VST3", args.pluginName + ".vst3")
     auFilePath   = os.path.join("/Users/krotos/Library/Audio/Plug-Ins/Components", args.pluginName + ".component")
     aaxFilePath  = os.path.join("/Library/Application Support/Avid/Audio/Plug-Ins", args.pluginName + ".aaxplugin")
     if not os.path.isdir(outputdir): os.makedirs(outputdir)
     # Remove any existing plugins
     print ("Deleting old plugins...")
     jenkinsVstFilePath  = os.path.join(outputdir,args.pluginName + ".vst")
     jenkinsVst3FilePath = os.path.join(outputdir,args.pluginName + ".vst3")
     jenkinsAuFilePath  = os.path.join(outputdir,args.pluginName + ".component")
     jenkinsAaxFilePath  = os.path.join(outputdir,args.pluginName + ".aaxplugin")
     if os.path.isdir(jenkinsVstFilePath):  rmtree(jenkinsVstFilePath)
     if os.path.isdir(jenkinsVst3FilePath): rmtree(jenkinsVst3FilePath)
     if os.path.isdir(jenkinsAuFilePath):   rmtree(jenkinsAuFilePath)
     if os.path.isdir(jenkinsAaxFilePath):  rmtree(jenkinsAaxFilePath)
     # Copy plugins into the target output directory if they exist
     print("Searching for " + args.pluginName + " plugins built in Xcode. Plugins will be copied to: " + outputdir)
     # vst plugin
     print ("Seeking " + vstFilePath + "...")
     if os.path.isdir(vstFilePath):
        print ("Copying " + args.pluginName + ".vst")
        copytree(vstFilePath, jenkinsVstFilePath)
        signPlugin(jenkinsVstFilePath)
     else:
        print("Not found.")
     # vst3 plugin
     print ("Seeking " + vst3FilePath + "...")
     if os.path.isdir(vst3FilePath):
        print ("Copying " + args.pluginName + ".vst3")
        copytree(vst3FilePath, jenkinsVst3FilePath)
        signPlugin(jenkinsVst3FilePath)
     else:
        print("Not found.")
     # au plugin
     print ("Seeking " + auFilePath + "...")
     if os.path.isdir(auFilePath):
        print ("Copying " + args.pluginName + ".component")
        copytree(auFilePath, jenkinsAuFilePath)
        signPlugin(jenkinsAuFilePath)
     else:
          print("Not found.")
     # aax plugin
     print ("Seeking " + aaxFilePath + "...")
     if os.path.isdir(aaxFilePath):
        print ("Copying " + args.pluginName + ".aaxplugin")
        copytree(aaxFilePath, jenkinsAaxFilePath)
        signPlugin(jenkinsAaxFilePath)

          
     else:
        print("Not found.")
     print ("~~~~ plugins fetched ~~~~")

#=====================================

if operatingSystem == windows:
    fetchFilesWindows()
elif operatingSystem == osx:
     fetchFilesOSX()






