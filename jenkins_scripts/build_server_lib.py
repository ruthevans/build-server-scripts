#!/usr/bin/env python
from platform import system
import os
from subprocess import Popen, PIPE


def getWCGUIDForPlugin(plugin):
    IDs = {
        "Concept"               : "721BDC00-BDA6-11E9-A234-00505692C25A",
        "Simple Concept"        : "721BDC00-BDA6-11E9-A234-00505692C25A",
        "Simple Concept Create" : "721BDC00-BDA6-11E9-A234-00505692C25A"
    }
    return IDs.get(plugin, "NoIdFound")

# Deprecated!!! Please use signPluginWithCustomerNum instead
def signPlugin(path, wcguid):
    windowsCert="C:/Dev/SDKs and Libraries/Windows Self-Signed Certificates/krotos_cert.p12"
    windowsPass="KrotosHighConcept2019!"
    if (system() == "Darwin"):#osx
        wraptool = "/Applications/PACEAntiPiracy/Eden/Fusion/Current/bin/wraptool"
        signCmd = "\"" + wraptool + "\"" +" sign --account krotosltd --signid \"Krotos LTD\" --wcguid " + wcguid + "  --in \"" + path + "\""
        returnCode = os.system(signCmd)

    elif (system() == "Windows"):
        keyfile = "C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12"
        keypass = "KrotosHighConcept2019!"
        wraptool = "C:\\Program Files (x86)\\PACEAntiPiracy\\Eden\\Fusion\\Versions\\4\\wraptool.exe"   
        signCmd = "\"" + wraptool + "\"" +" sign --account krotosltd --signid \"Krotos LTD\" --keyfile \""+ keyfile +"\" --keypassword "+keypass+" --wcguid " + wcguid + "  --in \"" + path + "\""
        signOutput =  subprocess.Popen(signCmd, shell=True, stdout=subprocess.PIPE).stdout
        

    else:
        return 1
    #todo: check return value. Windows have diffulty
    return 0
    
def signPluginWithCustomerNum(inPath, signOutput):
    print("inPath: " + inPath)
    print("Signing plugins...")
    returnCode=100
    windowsCert="C:/Dev/SDKs and Libraries/Windows Self-Signed Certificates/krotos_cert.p12"
    windowsPass="KrotosHighConcept2019!"

    if (system() == "Darwin"):#osx
        wraptool = "/Applications/PACEAntiPiracy/Eden/Fusion/Current/bin/wraptool"
        signCmd = ("\"" + wraptool + "\" " 
        "sign --account krotosltd " 
        "--signid \"Krotos LTD\" "
        "--customernumber FLX8-L739-W373-M564 "
        "--customername \"Krotos LTD\"  "
        "--in \"" + inPath +
        "\" --out \"" + signOutput + "\"")
        returnCode = os.system(signCmd)
        if (returnCode == 0):
            print("plugin successfully signed to: " + signOutput)
        else:
            print("ERROR plugin failed to sign, errorcode:" + str(returnCode))

    elif (system() == "Windows"):
        keyfile = "C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12"
        keypass = "KrotosHighConcept2019!"
        wraptool = "C:\\Program Files (x86)\\PACEAntiPiracy\\Eden\\Fusion\\Versions\\5\\wraptool.exe"   
        popencmd=[wraptool, "sign", 
        "--account", "krotosltd",
        "--signid", "Krotos LTD",
        "--customernumber", "FLX8-L739-W373-M564",
        "--customername", "Krotos LTD",
        "--keyfile", keyfile,
        "--keypassword", keypass,
        "--in", inPath,
        "--out", signOutput]
        p = Popen(popencmd,shell=False, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate()
        rc = p.returncode
        returnCode=rc
        if (returnCode == 0):
            print("plugin successfully signed to: " + signOutput)
        else:
            print("ERROR plugin failed to sign, errorcode:" + str(rc))
            print(err)
    else:
        returnCode=101
    return returnCode
