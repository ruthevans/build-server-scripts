#!/bin/bash
#-----------------------------------------------------------------------
# Copies plugins to dropbox
# Plugins should be pre-wrapped before this script is called.
# PROJECT, JOB_PROJECT_VERSION and ARCH should be set. BUILD_NUMBER and BUILD_TAG will be set by jenkins.
# The script should be run from the workspace so that the git commands get the correct details.
#
#       ./copy_plugins_to_dropbox.sh
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh 

#-----------------------------------------------------------------------
# Set up variables for the rest of the script
function setup_variables {
    set_system_var
    set_repo_tag
    set_repo_branch
    set_repo_commit
    set_job_arch_var
    set_job_project_var
    set_job_project_version_var
    set_project_name
    if [[ "$SYSTEM" == "Mac" ]]; then
        # Mac build server detected
        LATEST_DIR="LatestMacBuilds"
        DROPBOX="/Users/krotos/Dropbox (Krotos)"
        JENKINS_BUILDS="/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"
        WRAPPED_JENKINS_BUILDS="/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"
        ZIP_COMMAND="/usr/bin/zip"
        ZIP_OPTIONS="-r"
    elif [[ "$SYSTEM" == "Win" ]]; then
        # Windows build server detected
        LATEST_DIR="LatestWindowsBuilds"
        DROPBOX="/cygdrive/c/Dropbox (Krotos)"
        JENKINS_BUILDS="/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"
        WRAPPED_JENKINS_BUILDS="/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"
        ZIP_COMMAND="/cygdrive/c/Program Files/7-Zip/7z.exe"
        ZIP_OPTIONS="a -tzip"
        alias find=/cygdrive/c/cygwin64/bin/find.exe
    else
        echo "Unknown system detected."
        exit 1
    fi

}

#-----------------------------------------------------------------------
# Get the version/Last tag applied to this commit
function set_repo_tag {
    REPO_TAG=`git tag -l --points-at HEAD | sed 's:.*/::'`
#Sometimes there just isn't a tag ok
}

#-----------------------------------------------------------------------
# Get the version/Last tag applied to this branch
function set_repo_commit {
    GIT_COMMIT=`git log --pretty=format:"%h" -1`
    if [[ "$GIT_COMMIT" == "" ]];then
    	GIT_COMMIT="commit unknown"
    fi
}

#-----------------------------------------------------------------------
# Current branch in use
function set_repo_branch {
    REPO_BRANCH=`echo $GIT_BRANCH | sed 's/\//_/g'`
    if [[ "$REPO_BRANCH" == "" ]];then
    	REPO_BRANCH="branch unknown"
    fi
}

#-----------------------------------------------------------------------
# Dehumaniser and Simple monsters are a special case
function set_project_name {
    if [[ "$JOB_PROJECT" == "Simple Monsters" ]]; then
    	PROJECT_DIR="Dehumaniser/Simple Monster VST"
    	PROJECT_NAME="Simple Monsters"
    elif [[ "$JOB_PROJECT" == "Dehumaniser" ]]; then
        PROJECT_DIR="Dehumaniser/Dehumaniser VST"
        PROJECT_NAME="Dehumaniser II"
    elif [[ "$JOB_PROJECT" == "Reformer Pro" ]]; then
    	PROJECT_DIR="Reformer/Reformer Pro"
    	PROJECT_NAME="Reformer Pro"
    elif [[ "$JOB_PROJECT" == "Igniter" ]]; then
    	PROJECT_DIR="Engines"
    	PROJECT_NAME="Engine"
    elif [[ "$JOB_PROJECT" == "Concept" ]]; then
        PROJECT_DIR="Synth"
        PROJECT_NAME="Concept"
    else
    	PROJECT_DIR="$JOB_PROJECT"
    	PROJECT_NAME="$JOB_PROJECT"
    fi

    # set up values before the copy starts
    if [[ "$PROJECT_VERSION" == "Demo" ]]; then
    	BUILD_REF="$PROJECT_NAME $PROJECT_VERSION $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER"
    	INSTALLER_NAME="$PROJECT_NAME $PROJECT_VERSION $JOB_ARCH"
    else
    	BUILD_REF="$PROJECT_NAME $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER"
    	INSTALLER_NAME="$PROJECT_NAME $JOB_ARCH"
    fi
}

#-----------------------------------------------------------------------
function copy_release_notes {
# See if there are release notes to include
    if [[ $SYSTEM == "Win" ]]; then
        #/cygdrive/c/cygwin64/bin/find.exe . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"$PROJECT_NAME" \;
        /cygdrive/c/cygwin64/bin/find.exe . -name *ReleaseNotes.txt -exec cp {} "$WRAPPED_JENKINS_BUILDS" \;
    else
        #/usr/bin/find . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"$PROJECT_NAME" \;
        /usr/bin/find . -name *ReleaseNotes.txt -exec cp {} "$WRAPPED_JENKINS_BUILDS" \;
    fi
}

#-----------------------------------------------------------------------
function zip_plugins {
    copy_release_notes

    # set up values before the copy starts
    if [[ "$JOB_PROJECT_VERSION" == "Demo" ]]; then
    	BUILD_REF="$JOB_PROJECT Plugins $JOB_PROJECT_VERSION $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER Commit$GIT_COMMIT"
    	LAST_ARCHIVE="$JOB_PROJECT Plugins $JOB_PROJECT_VERSION $JOB_ARCH"
    else
    	BUILD_REF="$JOB_PROJECT Plugins $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER Commit$GIT_COMMIT"
    	LAST_ARCHIVE="$JOB_PROJECT Plugins $JOB_ARCH"
    fi

    create_build_details
    echo_and_run cd "$WRAPPED_JENKINS_BUILDS"
    echo_and_run "$ZIP_COMMAND" $ZIP_OPTIONS "$BUILD_REF.zip" *.aaxplugin *.dll *.vst *.vst3 *.component *.txt

}

#-----------------------------------------------------------------------
# Create build_details.txt
function create_build_details {
    printf "Build date:"                           >  "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    date                                           >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nJenkins build job: $JOB_BASE_NAME"   >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nJenkins build number: $BUILD_NUMBER" >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit commit: $GIT_COMMIT"             >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit branch: $GIT_BRANCH"             >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
    printf "\nGit tag: $REPO_TAG"                  >> "$WRAPPED_JENKINS_BUILDS"/build_details.txt
}

#-----------------------------------------------------------------------
# copy plugins to latest folder
function copy_plugins_to_latest {
    mkdir -p "$DROPBOX"/Development/"$PROJECT_DIR"/"Jenkins Builds"/"$LATEST_DIR"
    #copy plugins to latest
    rm -rf "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/$LATEST_DIR/$LAST_ARCHIVE"*.zip
    echo_and_run cp -rf "$WRAPPED_JENKINS_BUILDS/$BUILD_REF.zip"  "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/$LATEST_DIR"
    echo "Created $DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip"
}

#-----------------------------------------------------------------------
# copy plugins to the archive
    function copy_plugins_to_archive {
    #cd "$DROPBOX"/Development/"$PROJECT_DIR"/"Jenkins Builds"
    mkdir -p "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/Archive/$SYSTEM/$REPO_BRANCH"
    echo_and_run cp -rf "$WRAPPED_JENKINS_BUILDS/$BUILD_REF.zip"  "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/Archive/$SYSTEM/$REPO_BRANCH"
    echo "Created $DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/Archive/$SYSTEM/$REPO_BRANCH/$BUILD_REF.zip"
}

#-----------------------------------------------------------------------
# remove the zip file from wrapped dir
function cleanup {
    rm -rf "$WRAPPED_JENKINS_BUILDS"/"$BUILD_REF".zip
}
#-----------------------------------------------------------------------
#
function checkup {
    
    if [[ -e  "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip" ]] && \
       [[ -e  "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/Archive/$SYSTEM/$REPO_BRANCH/$BUILD_REF.zip" ]]; then
        echo "Everything is okay."
    else
        echo "Error: cannot confirm plugins were safely copied to dropbox at:"
        echo "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/$LATEST_DIR/$BUILD_REF.zip"
        echo "$DROPBOX/Development/$PROJECT_DIR/Jenkins Builds/Archive/$SYSTEM/$REPO_BRANCH/$BUILD_REF.zip"
        return 1
    fi
}

#-----------------------------------------------------------------------
setup_variables
zip_plugins
copy_plugins_to_latest
copy_plugins_to_archive
cleanup
checkup