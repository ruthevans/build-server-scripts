#!/bin/bash
#-----------------------------------------------------------------------
#
# This script updates the version header file
#
#-----------------------------------------------------------------------
set -e #exit script on error

function usage {
    echo "Usage: update_version_header.sh -f <filepath>"
    echo "       -f   File: Path to version file(optional for older project, igniter and prev.)"
    echo "       e.g.:  update_version_header.sh -f \"C:\\Dev\\Projects\\Project\\VersionHeader.h\" "
    exit 1
}

while getopts ":f:" opt; do
    case ${opt} in
    f ) # binary to wrap
        HEADER_FILE="$OPTARG"
        ;;
    \? ) echo "Invalid option."
        usage
        ;;
    : )
        echo "Parameter required for -$OPTARG"
        usage
        ;;
    * ) echo "no"
        usage
    esac
done
shift $((OPTIND -1))

source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh

#----Get values---------------------------------------------

function set_buildDate {
    # Date and time that we are running this build
    buildDate=`date "+%F %H:%M:%S"`
}

function set_currentBranchTemp {
    # Current branch in use
    # GIT_BRANCH is set by a jenkins plugin.
    currentBranchTemp=`echo $GIT_BRANCH | cut -d / -f 2`
    if [ -n "$currentBranchTemp" ];then
        currentBranch=$currentBranchTemp
    else
        currentBranch=""
        echo "Warning: Cannot find data for GIT_CURRENT_BRANCH"
    fi
}

function set_lastCommitHashTemp {
    # Last hash from the current branch
    lastCommitHashTemp=`git log --pretty=format:"%h" -1`
    if [ -n "$lastCommitHashTemp" ];then
        lastCommitHash=$lastCommitHashTemp
    else
        lastCommitHash=""
        echo "Warning: Cannot find data for GIT_LAST_COMMIT_HASH"
    fi
}   

function set_lastCommitDateTemp {
    # Date and time of the last commit on this branch
    lastCommitDateTemp=`git log --pretty=format:"%ad" --date=short -1`
    if [ -n "$" ];then
        lastCommitDate=$lastCommitDateTemp
    else
        lastCommitDate=""
        echo "Warning: Cannot find data for GIT_LAST_COMMIT_DATE"
    fi
}

function set_lastCommitCommentTemp {
    # Comment from the last commit on this branch
    lastCommitCommentTemp=`git log --pretty=format:"%s" -1`
    # Make the comment compiler-friendly by removing quotes and anything else weird
    lastCommitCommentTemp=`echo $lastCommitCommentTemp | sed "s/[^[:alnum:][:space:].,_?@£$%^&*-=_+;:~]//g"`
    if [ -n "$" ];then
        lastCommitComment=$lastCommitCommentTemp
    else
        lastCommitComment=""
        echo "Warning: Cannot find data for GIT_LAST_COMMIT_COMMENT"
    fi
}

function set_lastRepoTagTemp {
    # Last tag applied to this branch
    lastRepoTagTemp=`git describe --abbrev=0 --tags`
    if [ -n "$lastRepoTagTemp" ];then
        lastRepoTag=$lastRepoTagTemp
    else
        echo "Error: Cannot find data for GIT_LAST_REPO_TAG"
        exit 2
    fi
}

#the sed command here removes everything before any '/'s
function set_currentRepoTag {
    currentRepoTag=`git tag -l --points-at HEAD | sed 's:.*/::'`
    if [ -z "$currentRepoTag" ];then
        currentRepoTag="no_tag"
    fi
}

function set_lastDescribeTemp {
    # Git describe string
    lastDescribeTemp=`git describe`
    if [ -z "$lastDescribeTemp" ];then
        lastDescribe=$lastDescribeTemp
    else
        lastDescribe="0.0.0"
        echo "Warning: Cannot find data for $lastDescribe"
    fi
}


function set_tagCommitVersion {
    # Git describe string
    tagCommitVersion="$currentRepoTag"-"$lastCommitHash"
    if [ -z "$tagCommitVersion" ];then
        tagCommitVersion="0.0.0"
        echo "Warning: Cannot find data for $tagCommitVersion"
    fi
}


function fix_file_ownership {
#So we can own and remove the file afterwards
#only required for windows where jenkins runs as admin
if [[ $SYSTEM == "Win" ]]; then
    chown Krotos:Krotos "$gitDataFile"
fi
}
#----Create the version header ------------------------------------------

function create_version_header {


    echo "Creating $gitDataFile"
    
    if [ -f "$gitDataFile" ];then
        rm "$gitDataFile"
    fi

    set_buildDate
    set_currentBranchTemp
    set_lastCommitHashTemp
    set_lastCommitDateTemp
    set_lastCommitCommentTemp
    set_currentRepoTag
    set_tagCommitVersion
    
    # Build the file with all the information in it
    echo "/*" >> "$gitDataFile"
    echo "  ==============================================================================" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo `basename "$gitDataFile"` >> "$gitDataFile"
    echo "Auto generated on: $buildDate" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "  ==============================================================================" >> "$gitDataFile"
    echo "*/" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "#define BUILD_DATE              \"$buildDate\"" >> "$gitDataFile"
    echo "#define GIT_CURRENT_BRANCH      \"$currentBranch\"" >> "$gitDataFile"
    echo "#define GIT_LAST_COMMIT_HASH    \"$lastCommitHash\"" >> "$gitDataFile"
    echo "#define GIT_LAST_COMMIT_DATE    \"$lastCommitDate\"" >> "$gitDataFile"
    echo "#define GIT_LAST_COMMIT_COMMENT \"$lastCommitComment\"" >> "$gitDataFile"
    echo "// GIT_LAST_REPO_TAG is now actually the repo tag on the commit or \"no_tag\" if there isn't any."
    echo "#define GIT_LAST_REPO_TAG       \"$currentRepoTag\"" >> "$gitDataFile"
    if [ ! -z "$GIT_LAST_REFORMER_TAG" ]; then
        echo "#define GIT_LAST_REFORMER_TAG   \"$GIT_LAST_REFORMER_TAG\"" >> "$gitDataFile" 
    fi
    echo "#define AUTOVERSION             $currentRepoTag" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "#define $versionString       \"$tagCommitVersion\"" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    echo "" >> "$gitDataFile"
    
}

#----------Weaponiser--------
function create_header_weaponiser_Mac {
    gitDataFile="/Users/krotos/Dev/Projects/Weaponiser/Common Source/WeaponiserVersion.h"
    versionString="WEAPONISER_VERSION_STRING"
    create_version_header
}

function create_header_weaponiser_Win {
    gitDataFile="/cygdrive/c/Dev/Projects/Weaponiser/Common Source/WeaponiserVersion.h"
    versionString="WEAPONISER_VERSION_STRING"
    create_version_header
}

#----------DH/SM--------
function create_header_dh_Mac {
    gitDataFile="/Users/krotos/Dev/Projects/Simple Monsters/Source/DehumaniserVersion.h"
    versionString="DH_VERSION_STRING"
    create_version_header
}
function create_header_dh_Win {
    gitDataFile="/cygdrive/c/Dev/Projects/Simple Monsters/Source/DehumaniserVersion.h"
    versionString="DH_VERSION_STRING"
    create_version_header
}

#--------Reformer---------
function create_header_reformer_Mac {
    gitDataFile="/Users/krotos/Dev/Projects/Reformer/Reformer/Source/ConductorVersion.h"
    versionString="KROTOS_VERSION_STRING"
    create_version_header
}
function create_header_reformer_Win {
    gitDataFile="/cygdrive/c/Dev/Projects/Reformer/Reformer/Source/ConductorVersion.h"
    versionString="KROTOS_VERSION_STRING"
    create_version_header
}

#--------Reformer Pro ---------
function create_header_reformer_pro_Mac {
    gitDataFile="/Users/krotos/Dev/Projects/Reformer/Reformer Pro/Source/ConductorVersion.h"
    versionString="KROTOS_VERSION_STRING"
	GIT_LAST_REFORMER_TAG="use"
    create_version_header
}
function create_header_reformer_pro_Win {
    gitDataFile="/cygdrive/c/Dev/Projects/Reformer/Reformer Pro/Source/ConductorVersion.h"
    versionString="KROTOS_VERSION_STRING"
	GIT_LAST_REFORMER_TAG="use"
    create_version_header
}

#--------Igniter ---------
function create_header_igniter_Mac {
    gitDataFile="/Users/krotos/Dev/Projects/Igniter/Source/IgniterVersion.h"
    versionString="KROTOS_VERSION_STRING"
    create_version_header
}
function create_header_igniter_Win {
    gitDataFile="/cygdrive/c/Dev/Projects/Igniter/Source/IgniterVersion.h"
    versionString="KROTOS_VERSION_STRING"
    create_version_header
}

#-------- Default ----------

function create_header_default {
	if [ -z $HEADER_FILE ]; then
		echo "Error: No header file give"
		usage
		exit 1
	fi
    gitDataFile="$HEADER_FILE"
    versionString="KROTOS_VERSION_STRING"
    create_version_header
}

#----'Main' part of the script---------------------------------------------
set_system_var
set_job_project_var
echo "fetching project var:"
echo $JOB_PROJECT
case $JOB_PROJECT in
    Weaponiser) create_header_weaponiser_$SYSTEM
        ;;
    "Simple Monsters") create_header_dh_$SYSTEM
        ;;
    "Reformer Pro") create_header_reformer_pro_$SYSTEM
    	;;
    Reformer) create_header_reformer_$SYSTEM
        ;;
    Igniter) create_header_igniter_$SYSTEM
        ;;
    *)
        #Default
        create_header_default
esac

# we're removing the workspace at the end of builds now, so hopefully this is not required.
#fix_file_ownership

# Display the new file.
cat "$gitDataFile"

