#!/bin/bash
#-----------------------------------------------------------------------
# 
#  Removes files installed by the dehumaniser installer that would stop
#  the build server building again.
#
#-----------------------------------------------------------------------
set -e #exit on error
source $(dirname "$BASH_SOURCE")/../build_server_utils.sh 
#-----------------------------------------------------------------------

echo_and_run sudo rm -rf "/Users/krotos/Library/Audio/Plug-Ins/VST/Dehumaniser.vst"
echo_and_run sudo rm -rf "/Users/krotos/Library/Audio/Plug-Ins/Components/Dehumaniser.component"
echo_and_run sudo rm -rf "/Library/Application Support/Avid/Audio/Plug-Ins/Dehumaniser.aaxplugin"
echo_and_run sudo rm -rf "/Applications/ProTools_3PDev/Plug-Ins/Dehumaniser.aaxplugin"
echo_and_run sudo chmod -R 777 "/Library/Application Support/Krotos/Dehumaniser/Library/Presets/main"
