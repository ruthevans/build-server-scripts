#!/usr/bin/env python
########################################################################################
#
#  generate_solutions.py
#
#
########################################################################################

import subprocess
import argparse
import platform
import os
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("jucerpath", help="Path to the jucer file for the project")
parser.add_argument("jucerVersion", help="Jucer major version. Supported: 5, 5.4.3, 6.0.8")
args = parser.parse_args()

jucer_path = args.jucerpath
jucer_version = args.jucerVersion

print("running generate_solutions script")
settings_dat_file = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
def setup_windows_paths():
    global settings_dat_file
    global settings_dat_dest
    global juce_path
    settings_dat_file=os.path.join(settings_dat_file,"win_setup_files\\Projucer5\\Projucer.settings")
    settings_dat_dest ="C:\\Users\\Jenkins\\AppData\\Roaming\\Projucer"
    if (jucer_version=="5"):
        juce_path="C:\\Program Files\\Jucer\\Projucer 5.3.exe"
    elif(jucer_version=="5.4.3"):
        juce_path="C:\\Program Files\\Jucer\\Projucer 5.4.3.exe"
    elif(jucer_version=="6.0.8"):
        juce_path="C:\\Program Files\\Jucer\\Projucer 6.0.8.exe"
    else:
        print("Error: Unknown jucer_version")
        quit(1)

def setup_osx_paths():
    global settings_dat_file
    global settings_dat_dest
    global juce_path
    settings_dat_file =os.path.join(settings_dat_file,"osx_setup_files/Projucer5/Projucer.settings")
    settings_dat_dest ="/Users/krotos/Library/Application Support/Projucer"
    if (jucer_version=="5"):
        juce_path="/Applications/Projucer5/Projucer.app/Contents/MacOS/Projucer"
    elif(jucer_version=="5.4.3"):
        juce_path="/Applications/Projucer5/Projucer5.4.3.app/Contents/MacOS/Projucer"
    elif(jucer_version=="6.0.8"):
        juce_path="/Applications/Projucer6/Projucer8.0.8.app/Contents/MacOS/Projucer"
    else:
        print("Error: Unknown jucer_version")
        quit(1)

def replace_settings_file():
    print ("replacing settings file...")
    # Replace the settings.dat. Removing and recreating the destination is overkill but apparently required to satisfy JUCE
    if (os.path.isdir(settings_dat_dest)):
        shutil.rmtree(settings_dat_dest)
    os.makedirs(settings_dat_dest)
    shutil.copyfile(settings_dat_file,os.path.join(settings_dat_dest,"Projucer.settings"))
    if (os.path.isfile(os.path.join(settings_dat_dest,"Projucer.settings"))):
        print("file copied.")

# Main part of the script starts here

if (not os.path.exists(jucer_path)):
    print("Error: " + jucer_path + " does not exist.")
    quit(1)

if (platform.system()=="Windows"):
    setup_windows_paths()
    replace_settings_file()
elif(platform.system()=="Darwin"):
    setup_osx_paths()
    replace_settings_file()


# Remove the post build command
# this script will also turns off enablePluginBinaryCopyStep :)
print ("removing postbuild commands...")
postbuildcommand_remover=os.path.join(os.path.dirname(os.path.realpath(__file__)), "deletePostbuildCommands.py")
subprocess.call(["python", postbuildcommand_remover, jucer_path])

# Create the solutions
print("resave solution")
subprocess.call([juce_path, "--resave", jucer_path])
