#!/bin/bash
#-----------------------------------------------------------------------
#
#   This script will check and wrap plugins
#
#
#-----------------------------------------------------------------------
#set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh

#-------------Check script input ---------------------------------------
function usage {
    echo "Usage: wrap_project_plugins.sh -n [-i -o -v]"
    echo "       -n   Name: Name of the plugin to be wrapped. No file extensions, the script will "
    echo "                  wrap all .dll .vst .au and .aax where possible. This is case sensitive!"
    echo "       -i   ID  : GUID to use for wrapping (optional because script has a list of GUIDs)"
    echo "       -o   Output Dir: Output directory for the wrapped plugin. (optional)"
    echo "       -v   Visual Studio version (Optional. Windows only) vs2015 or vs2019 supported."
    echo "       e.g.:  wrap_project_plugins.sh -n Weaponiser -i 1234-5678 -o /tmp/newdir"
    exit 1
}

while getopts ":n:i:o:v:" opt; do
    case ${opt} in
    n ) # Plugin name
        PLUGIN_NAME=$OPTARG
        ;;
    i ) # Plugin ID
        OVERRIDE_GUID=$OPTARG
        ;;
    o ) # Output dir
    	OVERRIDE_OUTPUTDIR=$OPTARG
    	;;
    v ) # Visual Studio version
        VS_VERSION=$OPTARG
        ;;
    \? ) echo "Invalid option."
        usage
        ;;
    : )
        echo "Parameter required for -$OPTARG"
        usage
        ;;
    * ) echo "no"
        usage
    esac
done
shift $((OPTIND -1))

if [[ -z $PLUGIN_NAME ]]; then
    echo "No plugin name given."
    usage
fi
#-----------------------------------------------------------------------
#  Function: set_guid
#  Purpose:  sets WRAPTOOL_GUID to OVERRIDE_GUID if set
#            otherwise GUID is selected using the JOB_PROJECT variable
function set_guid {
    if [[ $OVERRIDE_GUID ]]; then
        WRAPTOOL_GUID=$OVERRIDE_GUID
    else
        case "$JOB_PROJECT" in
        "Dehumaniser")
            if [[ $JOB_PROJECT_VERSION == "Demo" ]]; then
                WRAPTOOL_GUID="23EB64A0-11DE-11E6-8802-005056875CC3"
            else
                WRAPTOOL_GUID="E1A73960-11E3-11E6-9BAE-005056A204F3"
            fi ;;
        "Simple Monsters") 
            if [[ $JOB_PROJECT_VERSION == "Demo" ]]; then
                WRAPTOOL_GUID="C29C1160-9C55-11E6-A442-005056A204F3"
            else
                WRAPTOOL_GUID="9B3232A0-6F94-11E6-A726-005056875CC3"
            fi ;;
        "Reformer Pro")    WRAPTOOL_GUID="58B4E230-8BCF-11E7-90EF-005056875CC3";;
        "Reformer")        WRAPTOOL_GUID="9E5AE6B0-DD6D-11E6-8076-005056875CC3";;
        "Weaponiser")      WRAPTOOL_GUID="4A8E12D0-4219-11E7-8D5B-005056875CC3";;
        "Igniter")         WRAPTOOL_GUID="26A51F00-B4C6-11E8-8B50-005056A204F3";;
        "Concept")         WRAPTOOL_GUID="721BDC00-BDA6-11E9-A234-00505692C25A";;
        *)
        echo "error: unknown project: $JOB_PROJECT"
            exit 2
        esac
    fi
    echo "Using WCGUID:$WRAPTOOL_GUID"
}
#-----------------------------------------------------------------------
function wrap_windows_plugin {
    INPUT_PLUGIN="$1"
    OUTPUT_PLUGIN="$2"
    if [[ -z $WRAPTOOL ]]; then set_wraptool_var; fi
    echo "$WRAPTOOL"
        
    $WRAPTOOL wrap --verbose --account krotosltd --password 80439777Krotosilok \
    --keyfile "C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12" \
    --keypassword "KrotosHighConcept2019!" \
    --wcguid $WRAPTOOL_GUID \
    --in "$INPUT_PLUGIN" \
    --out "$OUTPUT_PLUGIN"
    
    #echo "wraptool wrap --verbose --account krotosltd --password 80439777Krotosilok --keyfile 'C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12' --keypassword 'KrotosHighConcept2019!' --wcguid $WRAPTOOL_GUID --in $INPUT_PLUGIN --out $OUTPUT_PLUGIN"
    
    $WRAPTOOL verify --in "$OUTPUT_PLUGIN"
}
function wrap_mac_plugin {
    INPUT_PLUGIN="$1"
    OUTPUT_PLUGIN="$2"
    if [[ -z $WRAPTOOL ]]; then set_wraptool_var; fi
    $WRAPTOOL wrap --verbose --account krotosltd --signid "Krotos LTD" \
    --wcguid $WRAPTOOL_GUID \
    --removeunsupportedarchs \
    --in "$INPUT_PLUGIN" \
    --out "$OUTPUT_PLUGIN"
    $WRAPTOOL verify --in "$OUTPUT_PLUGIN"
}
#-----------------------------------------------------------------------
function wrap_dll_Win {
    # Remember, windows wraptool will want windows style paths
    echo "Copying plugin to the unwrapped folder $UNWRAPPED_DIR"
    mkdir -p "$UNWRAPPED_DIR"
    cp -pR "$VST_PLUGINS/$PLUGIN_NAME.dll" "$UNWRAPPED_DIR"
	
	if [ -e "$VST_PLUGINS/$PLUGIN_NAME.aaxdll" ]; then
		cp -pR "$VST_PLUGINS/$PLUGIN_NAME.aaxdll" "$UNWRAPPED_DIR"
	fi
	
    for i in 1 2 3
    do
        if [ ! -e "$WRAPPED_DIR/$PLUGIN_NAME.dll" ]; then
            sleep 3
            wrap_windows_plugin \
            "C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\unwrapped$JOB_PROJECT_VERSION$JOB_ARCH\\$PLUGIN_NAME.dll" \
            "$WRAPPED_DIR\\$PLUGIN_NAME.dll"
        fi
    done
}

function wrap_vst3_Win {
    # Remember, windows wraptool will want windows style paths
    mkdir -p "$UNWRAPPED_DIR"
    echo "Copying plugin to the unwrapped folder $UNWRAPPED_DIR"
    cp -pR "$VST3_PLUGINS/$PLUGIN_NAME.VST3" "$UNWRAPPED_DIR"
    for i in 1 2 3
    do
        if [ ! -e "$WRAPPED_DIR/$PLUGIN_NAME.VST3" ]; then
            sleep 3
                wrap_windows_plugin \
                "C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\unwrapped$JOB_PROJECT_VERSION$JOB_ARCH\\$PLUGIN_NAME.VST3" \
                "$WRAPPED_DIR\\$PLUGIN_NAME.VST3"
         fi
    done
}

function wrap_aax_Win {
     mkdir -p "$UNWRAPPED_DIR"
        #If that failed then I imagine the file doesn't exist so use the .dll to make one
    if [[ $JOB_ARCH == "32" ]]; then
        AAX_STRUCTURE="$PLUGIN_NAME.aaxplugin\contents\Win32"
    else
        AAX_STRUCTURE="$PLUGIN_NAME.aaxplugin\contents\x64"
    fi
    mkdir -p "$WRAPPED_DIR/$AAX_STRUCTURE"
    echo "making dir $WRAPPED_DIR/$AAX_STRUCTURE"
     for i in 1 2 3 4 5 6
    do
        if [ ! -e "$WRAPPED_DIR/$PLUGIN_NAME.aaxdll" ]; then
            sleep 3
            if [[ -e "$UNWRAPPED_DIR\\$PLUGIN_NAME.aaxdll" ]]; then
                echo "Creating aax from unwrapped aaxdll."
                wrap_windows_plugin \
                "C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\unwrapped$JOB_PROJECT_VERSION$JOB_ARCH\\$PLUGIN_NAME.aaxdll" \
                "$WRAPPED_DIR\\$AAX_STRUCTURE\\$PLUGIN_NAME.aaxplugin"
            elif [[ -e "$UNWRAPPED_DIR\\$PLUGIN_NAME.dll" ]]; then
                echo "Creating aax from unwrapped dll."
                wrap_windows_plugin \
                "C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\unwrapped$JOB_PROJECT_VERSION$JOB_ARCH\\$PLUGIN_NAME.dll" \
                "$WRAPPED_DIR\\$AAX_STRUCTURE\\$PLUGIN_NAME.aaxplugin"
            fi
        fi
    done

}
function wrap_vst_Mac {
    mkdir -p "$UNWRAPPED_DIR"
    cp -pR "$VST_PLUGINS/$PLUGIN_NAME.vst" "$UNWRAPPED_DIR"
    echo "============================= VST ==========================================="
    wrap_mac_plugin \
    "$UNWRAPPED_DIR/$PLUGIN_NAME.vst" \
    "$WRAPPED_DIR/$PLUGIN_NAME.vst"
     echo "============================================================================="
}

function wrap_vst3_Mac {
    mkdir -p "$UNWRAPPED_DIR"
    cp -pR "$VST3_PLUGINS/$PLUGIN_NAME.vst3" "$UNWRAPPED_DIR"
    echo "============================= VST3 ==========================================="
    wrap_mac_plugin \
    "$UNWRAPPED_DIR/$PLUGIN_NAME.vst3" \
    "$WRAPPED_DIR/$PLUGIN_NAME.vst3"
    echo "============================================================================="
}

function wrap_aax_Mac {
    mkdir -p "$UNWRAPPED_DIR"
    cp -pR "$AVID_PLUGINS/$PLUGIN_NAME.aaxplugin" "$UNWRAPPED_DIR"
    echo "============================= AAX ==========================================="
    wrap_mac_plugin \
    "$UNWRAPPED_DIR/$PLUGIN_NAME.aaxplugin" \
    "$WRAPPED_DIR/$PLUGIN_NAME.aaxplugin"
    echo "============================================================================="
}
function wrap_au_Mac {
    mkdir -p "$UNWRAPPED_DIR"
    cp -pR "$COMPONENT_PLUGINS/$PLUGIN_NAME.component" "$UNWRAPPED_DIR"
    echo "============================== AU ==========================================="
    wrap_mac_plugin \
    "$UNWRAPPED_DIR/$PLUGIN_NAME.component" \
    "$WRAPPED_DIR/$PLUGIN_NAME.component"
    echo "============================================================================="
}
#-----------------------------------------------------------------------
function wrap_plugins_Win {
    if [[ $JOB_ARCH == "32" ]]; then
        PROGRAM_FILES="Program Files (x86)"
    else
        PROGRAM_FILES="Program Files"
    fi
    
    if [[ $VS_VERSION == "vs2019" ]]; then
        echo "Using visual studio 2019 paths"
        VST_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2019/x64/Release/VST"
        VST3_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2019/x64/Release/VST3"
        AVID_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2019/x64/Release/AAX"
    elif [[ $VS_VERSION == "vs2015" ]]; then
        echo "Using visual studio 2015 paths"
        VST_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2015/x64/Release"
        VST3_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2015/x64/Release"
        AVID_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2015/x64/Release"
    else
        echo "Using default paths"
        VST_PLUGINS="/cygdrive/c/$PROGRAM_FILES/Steinberg/VstPlugins"
        VST3_PLUGINS="/cygdrive/c/Dev/Projects/$JOB_PROJECT/Builds/VisualStudio2015/x64/Release/VST3"
        AVID_PLUGINS="/cygdrive/c/$PROGRAM_FILES/Common Files/Avid/Audio/Plug-Ins"
    fi
        
    UNWRAPPED_DIR="C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"
	

    mkdir -p "$UNWRAPPED_DIR"
    if [[ "$OVERRIDE_OUTPUTDIR" ]]; then 
    	WRAPPED_DIR="$OVERRIDE_OUTPUTDIR"
    else
        WRAPPED_DIR="C:\\Users\\Jenkins\\Desktop\\Jenkins Builds\\$JOB_PROJECT\\wrapped$JOB_PROJECT_VERSION$JOB_ARCH"
    fi
	
	#make sure unwrapped/wrapped dirs are empty!
	echo "Cleaning un/wrapped directories"
	rm -rf "$UNWRAPPED_DIR"/*
	rm -rf "$WRAPPED_DIR"/*
	
    echo "============================= VST ==========================================="
    echo "looking for $VST_PLUGINS/$PLUGIN_NAME.dll"
    if [[ -e "$VST_PLUGINS/$PLUGIN_NAME.dll" ]]; then
        wrap_dll_Win
    else
        echo "vst not found."
    fi
    echo "=============================================================================="
    
    echo "============================= VST3 ==========================================="
    echo "looking for $VST3_PLUGINS/$PLUGIN_NAME.VST3"
    if [[ -e "$VST3_PLUGINS/$PLUGIN_NAME.VST3"  ]]; then
         wrap_vst3_Win #This depends on wrap_dll_Win being run first
    else
        echo "vst3 not found."
    fi
    echo "============================================================================="

    echo "============================= AAX ==========================================="
    echo "looking for $AVID_PLUGINS/$PLUGIN_NAME.aaxdll"
    #No need to check for the .aax file, if it doesn't exist we will MAKE it exist
    if [[ -e "$AVID_PLUGINS/$PLUGIN_NAME.aaxdll" ]]; then
        cp -pR "$AVID_PLUGINS/$PLUGIN_NAME.aaxdll" "$UNWRAPPED_DIR"
    fi
    wrap_aax_Win
    
    echo "============================================================================="
}
function wrap_plugins_Mac {
    VST_PLUGINS="/Users/krotos/Library/Audio/Plug-Ins/VST"
    VST3_PLUGINS="/Users/krotos/Library/Audio/Plug-Ins/VST3" #mac only, windows makes a .dll to fits both
    AVID_PLUGINS="/Library/Application Support/Avid/Audio/Plug-Ins"
    COMPONENT_PLUGINS="/Users/krotos/Library/Audio/Plug-Ins/Components"
    UNWRAPPED_DIR="/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/unwrapped$JOB_PROJECT_VERSION$JOB_ARCH"
    if [[ "$OVERRIDE_OUTPUTDIR" ]]; then 
    	WRAPPED_DIR="$OVERRIDE_OUTPUTDIR"
    else
    	WRAPPED_DIR="/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"
    fi
    
    if [[ -e "$VST_PLUGINS/$PLUGIN_NAME.vst" ]]; then
        wrap_vst_Mac
    else
        echo "Error: Cannot find plugin at:$VST_PLUGINS/$PLUGIN_NAME.vst"
        MISSING_PLUGIN="true"
    fi
    
    if [[ -e "$VST3_PLUGINS/$PLUGIN_NAME.vst3" ]]; then
        wrap_vst3_Mac
    else
        echo "Error: Cannot find plugin at:$VST3_PLUGINS/$PLUGIN_NAME.vst3"
        MISSING_PLUGIN="true"
    fi

    if [[ -e "$AVID_PLUGINS/$PLUGIN_NAME.aaxplugin" ]]; then
        wrap_aax_Mac
    else
        echo "Error: Cannot find plugin at:$VST_PLUGINS/$PLUGIN_NAME.vst"
        MISSING_PLUGIN="true"
    fi
    
    if [[ -e "$COMPONENT_PLUGINS/$PLUGIN_NAME.component" ]]; then
        wrap_au_Mac
    else
        echo "Error: Cannot find plugin at:$COMPONENT_PLUGINS/$PLUGIN_NAME.component"
        MISSING_PLUGIN="true"
    fi
    
    #if [[ $MISSING_PLUGIN ]]; then exit 3; fi
    
}

#----'Main' part of the script-------------------------------------------
set_system_var
set_job_project_version_var
set_job_project_var
set_job_arch_var
set_guid
wrap_plugins_$SYSTEM


