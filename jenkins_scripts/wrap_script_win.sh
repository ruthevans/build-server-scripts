#!/bin/bash
#-----------------------------------------------------------------------
#
# This script sets up the correct repos for Jenkins
#
#-----------------------------------------------------------------------

WRAPTOOL="/cygdrive/c/PROGRA~2/PACEAntiPiracy/Eden/Fusion/Versions/4/wraptool.exe"
UNWRAPPED_FOLDER_PATH="/cygdrive/c/Users/Krotos/Desktop/Jenkin~2/$PROJECT/unwrapped$VERSION$ARCH"
WRAPPED_FOLDER_PATH="/cygdrive/c/Users/Krotos/Desktop/Jenkin~2/$PROJECT/wrapped$VERSION$ARCH"

echo "unwrapped folder is: $UNWRAPPED_FOLDER_PATH"
echo "wrapped folder is: $WRAPPED_FOLDER_PATH"
echo
echo "input is $UNWRAPPED_FOLDER_PATH/$PLUGIN_NAME.dll"

$WRAPTOOL wrap --verbose --account krotosltd --password 80439777Krotosilok --keyfile "/cygdrive/c/Users/Krotos/Documents/Window~1/2017SignignCert.pfx" --keypassword "80439777Krotos" --wcguid $WCGUID --in $UNWRAPPED_FOLDER_PATH/$PLUGIN_NAME.dll --out "$WRAPPED_FOLDER_PATH/$PLUGIN_NAME.dll"
#    rem Create a directory for wrapped release defined by AAX folder structure
#    rmdir "%PROJECT_NAME%.aaxplugin" /S /Q
#    md %PROJECT_NAME%.aaxplugin
#    md %PROJECT_NAME%.aaxplugin\contents
#    md %AAX_FOLDER_STRUCTURE%

#    "C:\Program Files (x86)\PACEAntiPiracy\Eden\Fusion\Versions\3\wraptool" wrap --verbose --account krotosltd --password 80439777Krotosilok --keyfile "C:\Users\Krotos\Documents\Windows Self-Signed Certificates\2017SignignCert.pfx" --keypassword "80439777Krotos" --wcguid %WCGUID% --in "%UNWRAPPED_FOLDER_PATH%\%PROJECT_NAME%.dll" --out "%WRAPPED_FOLDER_PATH%\%AAX_FOLDER_STRUCTURE%\%PROJECT_NAME%.aaxplugin"
#    call :AssertSuccess %errorlevel% "PACE Wraptool"

#    rem Copy the wrapped file .aaxplugin file beside .aaxplugin folder and change its extension to dll
#    copy "%WRAPPED_FOLDER_PATH%\%AAX_FOLDER_STRUCTURE%\%PROJECT_NAME%.aaxplugin" "%WRAPPED_FOLDER_PATH%\%PROJECT_NAME%.dll" /y 
#    call :AssertSuccess %errorlevel% "Copy of %PROJECT_NAME%.aaxplugin file to %WRAPPED_FOLDER_PATH% folder"