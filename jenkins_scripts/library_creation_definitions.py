#!/usr/bin/env python
###############################################################
#
#   Functions to be used for generating reformer library installers on jenkins
#
###############################################################
import os
import argparse
import subprocess
from sys import platform

# Remove the dropbox part of the path
def get_samples_path_suffix(dropbox_path):
    if platform == "darwin":
        return dropbox_path.split('Reformer Libraries/', 1)[-1]
    elif platform == "win32":
        return dropbox_path.split('Reformer Libraries', 1)[-1]

# Remove the dropbox part of the path, if any,  and add the dropbox prefix for this computer
def get_samples_path(dropbox_path):
    if platform == "darwin":
        my_dropbox_path = "/Volumes/Transcend/Dropbox (Krotos)/Development/Reformer/Reformer Libraries/"
        my_dropbox_path = my_dropbox_path + dropbox_path.split('Reformer Libraries/', 1)[-1]
    elif platform == "win32":
        my_dropbox_path = "B:\\Dropbox (Krotos)\\Development\\Reformer\\Reformer Libraries"
        my_dropbox_path = os.path.join(my_dropbox_path, dropbox_path.split('Reformer Libraries', 1)[-1])
    return my_dropbox_path


def get_output_path(output_env):
    if platform == "darwin":
        output_path = os.path.join("/Volumes/Transcend/Dropbox (Krotos)", output_env)
    elif platform == "win32":
        output_path = os.path.join("B:\\Dropbox (Krotos)", output_env)
    return output_path

# returns true is the given directory is empty
def folder_is_empty(path_to_folder, library_name):
    if (len(os.listdir(path_to_folder)) == 0):
        print ">>" + library_name + " is empty"
        return True

# returns true if the given directory includes another directory
def subfolder_exists(path_to_folder, library_name):
    subfolderFound=False
    for root, dirs, files in os.walk(path_to_folder):
        for dir in dirs:
            print ">>" +  library_name + " contains a subfolder."
            subfolderFound=True
    return subfolderFound

# returns true if the given directory includes a file that is not .wav or .aif (not case sensitive)
def includes_incompatible_file(path_to_folder, library_name):
    incompatibleFileFound=False
    for root, dirs, files in os.walk(path_to_folder):
        for file in files:
            testfilelower=file.lower()
            #print testfilelower
            if (not testfilelower.endswith((".wav", ".aif")) and  not testfilelower.startswith((".ds", "._"))):
                print ">>" + library_name + " contains file that is not a sound type:" + file
                incompatibleFileFound=True
    return incompatibleFileFound

def samples_file_is_good(sample_path):
    sample_path = os.path.join(sample_path,"Samples")
    result = True
    # Check that the path ends with a directory
    if not os.path.isdir(sample_path):
        result = False
    return result
    

# Runs some checks to see if a directory appears to be a library
def folder_is_a_library(folder):
    isALib=True
    possibleLibraryName=os.path.basename(folder)
    #Libraries include a "Samples" subfolder
    if not samples_file_is_good(folder):
        isALib = False
    
    #if (subfolder_exists(folder, possibleLibraryName)):
    #    isALib=False
    #if (includes_incompatible_file(folder, possibleLibraryName)):
    #    isALib=False
    #if (folder_is_empty(folder, possibleLibraryName)):
    #    isALib=False
    return isALib

# Returns a list of suitable reformer libraries in the given directory (recursive)
def get_library_list(folder):
    returnlist = list()
    for root, dirs, files in os.walk(folder):
        for dir in dirs:
            if folder_is_a_library(os.path.join(root,dir)):
                returnlist.append(os.path.join(root,dir))
    return returnlist


