#!/bin/bash
#-----------------------------------------------------------------------
# Copies plugins to dropbox
# Plugins should be pre-wrapped before this script is called.
# PROJECT, PROJECT_VERSION and ARCH should be set. BUILD_NUMBER and BUILD_TAG will be set by jenkins
#
# e.g.: export PROJECT=Reformer   (should match dropbox dir)
#       export PROJECT_VERSION=Release
#       export ARCH=64
#       ./copy_plugins_to_dropbox.sh
#-----------------------------------------------------------------------
set -e

UNWRAPPED_DIR="/Users/krotos/Desktop/PACE/unwrapped/$PROJECT/$ARCH$PROJECT_VERSION/$BUILD_NUMBER"
WRAPPED_DIR="/Users/krotos/Desktop/PACE/wrapped/$PROJECT/$ARCH$PROJECT_VERSION/$BUILD_NUMBER"


function usage {
echo "Usage: ./wrap_plugin_mac.sh <wcguid> <plugin>"
echo "e.g.:  ./wrap_plugin_mac.sh 4A8E12D0-4219-11E7-8D5B-005056875CC3 Weaponiser.vst"
exit
}

WCGUID=$1
PLUGIN_NAME=$2

echo "system: $unamestr"
if [[ "$WCGUID" == "" ]]; then
    usage
fi
if [[ "$PLUGIN_NAME" == "" ]]; then
    usage
fi

echo mkdir -p "$UNWRAPPED_DIR"
echo mkdir -p "$WRAPPED_DIR"
mkdir -p "$UNWRAPPED_DIR"
mkdir -p "$WRAPPED_DIR"

cp -r "/Users/krotos/Library/Audio/Plug-Ins/VST/$PROJECT"*         "$UNWRAPPED_DIR"
cp -r "/Users/krotos/Library/Audio/Plug-Ins/Components/$PROJECT"*  "$UNWRAPPED_DIR"
cp -r "/Library/Application Support/Avid/Audio/Plug-Ins/$PROJECT"* "$UNWRAPPED_DIR"


# Wrap given plugin
if [ -d /Users/krotos/Desktop/PACE/unwrapped/"$PROJECT"/$ARCH$PROJECT_VERSION/$BUILD_NUMBER/$PLUGIN_NAME ]; then
	/Applications/PACEAntiPiracy/Eden/Fusion/Current/bin/wraptool wrap --verbose --account krotosltd --wcguid $WCGUID --signid "Krotos LTD" --in "$UNWRAPPED_DIR"/$PLUGIN_NAME   --out "$WRAPPED_DIR"/$PLUGIN_NAME
	echo "Plugin wrapped at $WRAPPED_DIR/$PLUGIN_NAME"
else
	echo "Cannot find plugin $UNWRAPPED_DIR/$PLUGIN_NAME"
fi


