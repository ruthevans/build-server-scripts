#!/usr/bin/bash
#get git tag version number and save it to ConductorVersion.h file on project folder

if [ -z "$1" ]; then
    echo "Need path to ConductorVersion.h"
    exit
fi

echo "Building file"

cd "$1"
if [ -f ConductorVersion.h ]
then
echo "Removing previous file"
rm ConductorVersion.h
fi


gitDataFile="ConductorVersion.h"


echo "Get Information from system"

# Date and time that we are running this build
buildDate=`date "+%F %H:%M:%S"`

# Current branch in use
# GIT_BRANCH is set by jenkins
currentBranchTemp=`echo $GIT_BRANCH | cut -d / -f 2`
if [ -n "$currentBranchTemp" ]
then
currentBranch=$currentBranchTemp
else
currentBranch=""
fi

# Last hash from the current branch
lastCommitHashTemp=`git log --pretty=format:"%h" -1`
if [ -n "$lastCommitHashTemp" ]
then
lastCommitHash=$lastCommitHashTemp
else
lastCommitHash=""
fi

# Date and time of the last commit on this branch
lastCommitDateTemp=`git log --pretty=format:"%ad" --date=short -1`
if [ -n "$" ]
then
lastCommitDate=$lastCommitDateTemp
else
lastCommitDate=""
fi

# Comment from the last commit on this branch
lastCommitCommentTemp=`git log --pretty=format:"%s" -1`
# make the comment c++ friendy. Mainly by removing any quotes
lastCommitCommentTemp=`echo $lastCommitCommentTemp | sed "s/[^[:alnum:][:space:].,_?@£$%^&*-=_+;:~]//g"`
if [ -n "$" ]
then
lastCommitComment=$lastCommitCommentTemp
else
lastCommitComment=""
fi


# Last tag applied to this branch
lastRepoTagTemp=`git describe --abbrev=0 --tags`
if [ -n "$lastRepoTagTemp" ]
then
lastRepoTag=$lastRepoTagTemp
else
lastRepoTag="0.0.0"
fi

# Git describe string
lastDescribeTemp=`git describe`
if [ -n "$lastDescribeTemp" ]
then
lastDescribe=$lastDescribeTemp
else
lastDescribe="0.0.0"
fi




# Build the file with all the information in it
echo "Create header file"
echo "/*" >> $gitDataFile
echo "  ==============================================================================" >> $gitDataFile
echo "" >> $gitDataFile
echo " ConductorVersion.h" >> $gitDataFile
echo " Auto generated on: $buildDate" >> $gitDataFile
echo "" >> $gitDataFile
echo "  ==============================================================================" >> $gitDataFile
echo "*/" >> $gitDataFile
echo "" >> $gitDataFile
echo "" >> $gitDataFile
echo "" >> $gitDataFile
echo "#define BUILD_DATE              \"$buildDate\"" >> $gitDataFile
echo "#define GIT_CURRENT_BRANCH      \"$currentBranch\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_HASH    \"$lastCommitHash\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_DATE    \"$lastCommitDate\"" >> $gitDataFile
echo "#define GIT_LAST_COMMIT_COMMENT \"$lastCommitComment\"" >> $gitDataFile
echo "#define GIT_LAST_REPO_TAG       \"$lastRepoTag\"" >> $gitDataFile
echo "#define AUTOVERSION             $lastRepoTag" >> $gitDataFile

echo "" >> $gitDataFile
echo "" >> $gitDataFile

echo "#define KROTOS_VERSION_STRING       \"$lastDescribeTemp\"" >> $gitDataFile

echo "" >> $gitDataFile
echo "" >> $gitDataFile
