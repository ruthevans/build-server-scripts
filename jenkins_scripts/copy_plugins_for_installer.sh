#!/bin/bash
#-----------------------------------------------------------------------
#
# This script copies plugins from Jenkins Builds dir to somewhere the
# installer scripts will find them.
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh
set_system_var
set_job_arch_var
set_job_project_var
set_job_project_version_var


if [[ $SYSTEM == "Mac" ]]; then
    BUILDS_DIR=`convert_to_upper_case "$JOB_PROJECT"`
    mkdir -p "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH"
    echo_and_run rm -rf "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH"/*
    echo_and_run cp -pR "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.aaxplugin "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH"
    echo_and_run cp -pR "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.component "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH"
    echo_and_run cp -pR "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.vst       "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH"
    echo_and_run cp -pR "/Users/krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.vst3      "/Users/krotos/Documents/$BUILDS_DIR BUILDS/Plugins$JOB_ARCH" || true
else
    echo_and_run rm -rf "/cygdrive/c/Dev/Documents/$JOB_PROJECT Installers/Installer $JOB_PROJECT_VERSION $JOB_ARCH/"*.aaxplugin
    echo_and_run rm -rf "/cygdrive/c/Dev/Documents/$JOB_PROJECT Installers/Installer $JOB_PROJECT_VERSION $JOB_ARCH/"*.dll
    echo_and_run cp -R "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.aaxplugin "/cygdrive/c/Dev/Documents/$JOB_PROJECT Installers/Installer $JOB_PROJECT_VERSION $JOB_ARCH/"
    echo_and_run cp -R "/cygdrive/c/Users/Krotos/Desktop/Jenkins Builds/$JOB_PROJECT/wrapped$JOB_PROJECT_VERSION$JOB_ARCH"/*.dll       "/cygdrive/c/Dev/Documents/$JOB_PROJECT Installers/Installer $JOB_PROJECT_VERSION $JOB_ARCH/"
fi
