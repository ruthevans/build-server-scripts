rem ::: usage: sign_exe.bat /path/to/installer.exe
Set EXE_NAME=%~1

rem Sign executable
"C:\Program Files (x86)\Windows Kits\10\bin\x64\signtool.exe" sign /f "C:\Dev\SDKs and Libraries\Windows Self-Signed Certificates\krotos_cert.p12" /t http://timestamp.comodoca.com/authenticode /p KrotosHighConcept2019! "%EXE_NAME%"
call :AssertSuccess %errorlevel% "Installer signing"



exit /B 0
rem End of script

:AssertSuccess
:: %1 is the error level
:: %2 is the message to display
set SUCCEED_CODE=0
if not %1 == %SUCCEED_CODE% (
echo %2 failed
exit %1
)
echo %2 succeeded
echo.
exit /B 0