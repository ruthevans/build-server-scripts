#!/bin/bash
#-----------------------------------------------------------------------
# This is a Windows specific script
# This script creates and signs an inno installer
#
#-----------------------------------------------------------------------
set -e #exit script on error
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh
set_job_arch_var
set_job_project_var
set_job_project_version_var

# Create the installer
INNO_SCRIPT="C:\\Dev\\Documents\\$JOB_PROJECT Installers\\Installer $JOB_PROJECT_VERSION $JOB_ARCH\\MakeInstallerScript.iss"
echo_and_run "/cygdrive/c/Program Files (x86)/Inno Setup 5/ISCC.exe" "$INNO_SCRIPT"

# Sign the installer
EXE_NAME="C:\\Users\\Krotos\\Desktop\\Jenkins Installers\\$JOB_PROJECT\\$JOB_PROJECT $JOB_ARCH.exe"
if [[ $JOB_PROJECT == "Dehumaniser" ]];then
    EXE_NAME="C:\\Users\\Krotos\\Desktop\\Jenkins Installers\\$JOB_PROJECT\\$JOB_PROJECT II $JOB_ARCH.exe"
fi
echo_and_run "/cygdrive/c/Program Files (x86)/Windows Kits/10/bin/x64/signtool.exe" sign \
/f "C:\\Dev\\SDKs and Libraries\\Windows Self-Signed Certificates\\krotos_cert.p12" \
/t http://timestamp.comodoca.com/authenticode \
/p KrotosHighConcept2019! \
"$EXE_NAME"
