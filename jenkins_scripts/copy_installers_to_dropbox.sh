#!/bin/bash
#-----------------------------------------------------------------------
# Copies installers to dropbox
# All the variables should be set using the jenkins job name
#-----------------------------------------------------------------------
set -e #exit script on error
pwd
source "$(dirname "$BASH_SOURCE")"/../build_server_utils.sh

#-----------------------------------------------------------------------
function setup_variables {
    set_system_var
    set_job_arch_var
    set_job_project_var
    set_job_project_version_var
    set_repo_tag
    set_installer_name
    if [[ $SYSTEM == "Win" ]]; then
        LATEST_DIR="LatestWindowsInstaller"
        DROPBOX="/cygdrive/c/Dropbox (Krotos)"
        JENKINS_INSTALLERS="/cygdrive/c/Users/Krotos/Desktop/Jenkins Installers"
        ZIP_COMMAND="/cygdrive/c/PROGRA~1/7-Zip/7z.exe"
        ZIP_OPTIONS="a -tzip"
    elif [[ $SYSTEM == "Mac" ]]; then
        LATEST_DIR="LatestMacInstaller"
        DROPBOX="/Users/krotos/Dropbox (Krotos)"
        JENKINS_INSTALLERS="/Users/krotos/Desktop/Jenkins Installers"
        ZIP_COMMAND="/usr/bin/zip"
        ZIP_OPTIONS="-r"
    else
        echo "Unknown system detected."
        exit 1
    fi
}

#-----------------------------------------------------------------------
function set_repo_tag {
    REPO_TAG=`git tag -l --points-at HEAD | sed 's:.*/::'`
    if  [[ $? != 0 ]] || [[ "$REPO_TAG" == "" ]];then
        echo "Error: Cannot find git tag"
        return  2
    fi
}

#-----------------------------------------------------------------------
function set_installer_name {
# Dehumaniser and Simple monsters are special cases
    if [[ "$JOB_PROJECT" == "Simple Monsters" ]]; then
    	PROJECT_DIR="Dehumaniser/Simple Monster VST"
    	PROJECT_NAME="Simple Monsters"
    elif [[ "$JOB_PROJECT" == "Dehumaniser" ]]; then
        PROJECT_DIR="Dehumaniser/Dehumaniser VST"
        PROJECT_NAME="Dehumaniser II"
    elif [[ "$JOB_PROJECT" == "Reformer Pro" ]]; then
    	PROJECT_DIR="Reformer/Reformer Pro"
    	PROJECT_NAME="Reformer Pro"
    elif [[ "$JOB_PROJECT" == "Igniter" ]]; then
    	PROJECT_DIR="Engines"
        PROJECT_NAME="Igniter"
    elif [[ "$JOB_PROJECT" == "Simple Concept Create" ]]; then
        PROJECT_DIR="Synth"
        PROJECT_NAME="Simple Concept Create"
    elif [[ "$JOB_PROJECT" == "Simple Concept" ]]; then
        PROJECT_DIR="Synth"
        PROJECT_NAME="Simple Concept"
    elif [[ "$JOB_PROJECT" == "Concept" ]]; then
        PROJECT_DIR="Synth"
        PROJECT_NAME="Concept"
    else
    	PROJECT_DIR="$JOB_PROJECT"
    	PROJECT_NAME="$JOB_PROJECT"
    fi

    # set up values before the copy starts
    if [[ "$JOB_PROJECT_VERSION" == "Demo" ]]; then
    	BUILD_REF="$PROJECT_NAME $JOB_PROJECT_VERSION $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER"
    	INSTALLER_NAME="$PROJECT_NAME $JOB_PROJECT_VERSION $JOB_ARCH"
    else
        if [[ -z $JOB_ARCH ]]; then
            BUILD_REF="$PROJECT_NAME $REPO_TAG Build$BUILD_NUMBER"
        else
            BUILD_REF="$PROJECT_NAME $JOB_ARCH $REPO_TAG Build$BUILD_NUMBER"
        fi
    	INSTALLER_NAME="$PROJECT_NAME $JOB_ARCH"
    fi
    
    # Strip away any trailing spaces
    INSTALLER_NAME="$(echo -e "${INSTALLER_NAME}" | sed -e 's/[[:space:]]*$//')"

     
}

#-----------------------------------------------------------------------
function copy_release_notes {
# See if there are release notes to include
    if [[ $SYSTEM == "Win" ]]; then
        /cygdrive/c/cygwin64/bin/find.exe . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"$PROJECT_NAME" \;
    else
        /usr/bin/find . -name *ReleaseNotes.txt -exec cp {} "$JENKINS_INSTALLERS"/"$PROJECT_NAME" \;
    fi
}

#-----------------------------------------------------------------------
function create_zip {
# Create zip
    cd "$JENKINS_INSTALLERS"/"$JOB_PROJECT"
    pwd
    if [ -f *.txt ]; then
        echo "text file found"
        echo_and_run $ZIP_COMMAND $ZIP_OPTIONS "$BUILD_REF.zip" "$INSTALLER_NAME"*.* *.txt
    else
        echo "no text file found"
        echo_and_run $ZIP_COMMAND $ZIP_OPTIONS "$BUILD_REF.zip" "$INSTALLER_NAME"*.*
    fi
}

#-----------------------------------------------------------------------
function copy_zip_to_latest {
# copy zip to latest folder
    if [[ ! -d "$DROPBOX/Development/$PROJECT_DIR/Installers" ]]; then
        mkdir -p "$DROPBOX/Development/$PROJECT_DIR/Installers"
    fi
    cd "$DROPBOX/Development/$PROJECT_DIR/Installers"
    mkdir -p "$LATEST_DIR"
    rm -rf "$LATEST_DIR"/"$INSTALLER_NAME"*
    cp -r "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/"$BUILD_REF".zip "$LATEST_DIR"/"$BUILD_REF".zip
    echo "Created $DROPBOX/Development/$PROJECT_DIR/Installers/$LATEST_DIR/$BUILD_REF.zip"
}

#-----------------------------------------------------------------------
function copy_zip_to_archive {
# copy zip to archive
    if [[ ! -d "$DROPBOX/Development/$PROJECT_DIR/Installers" ]]; then
         "$DROPBOX/Development/$PROJECT_DIR/Installers"
    fi
    cd "$DROPBOX/Development/$PROJECT_DIR/Installers"
    mkdir -p Archive/"$SYSTEM"
    cp -r "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/"$BUILD_REF".zip Archive/"$SYSTEM"/"$BUILD_REF".zip
    echo Created "$DROPBOX/Development/$PROJECT_DIR/Installers"/Archive/"$SYSTEM"/"$BUILD_REF".zip
}

#-----------------------------------------------------------------------
function cleanup {
# Cleanup files no longer required
    if [[ -f "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/"$BUILD_REF".zip ]]; then
        rm "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/"$BUILD_REF".zip
    fi

    if [[ -f "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/*.txt ]]; then
        rm "$JENKINS_INSTALLERS"/"$JOB_PROJECT"/*.txt
    fi
}
#-----------------------------------------------------------------------
function checkup {
    # Check latest folder
    if [[ ! -e "$DROPBOX/Development/$PROJECT_DIR/Installers"/"$LATEST_DIR"/"$BUILD_REF".zip ]]; then
        echo "Error: Installer was not copied to dropbox."
        echo "It should have been at:$DROPBOX/Development/$PROJECT_DIR/Installers/$LATEST_DIR/$BUILD_REF.zip"
        exit 3
    fi
    # Check archive folder
        if [[ ! -e "$DROPBOX/Development/$PROJECT_DIR/Installers"/Archive/"$SYSTEM"/"$BUILD_REF".zip ]]; then
        echo "Error: Installer was not copied to dropbox."
        echo "It should have been at:$DROPBOX/Development/$PROJECT_DIR/Installers/Archive/$SYSTEM/$BUILD_REF.zip"
        exit 3
    fi
    echo "Everything is okay."
}
#-----------------------------------------------------------------------
setup_variables
copy_release_notes
create_zip
copy_zip_to_latest
copy_zip_to_archive
cleanup
checkup





