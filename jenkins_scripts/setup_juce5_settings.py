#!/usr/bin/env python
import os
from shutil import copyfile

if os.name=="nt": #windows
    savedSettingsPath="C:\\Dev\\Documents\\Build Server Scripts\\win_setup_files\\Projucer5\\Projucer.settings"
    settingsDir="C:\\Windows\\System32\\config\\systemprofile\\AppData\\Roaming\\Projucer"
else: #mac
    savedSettingsPath="/Users/krotos/Library/Application Support/Projucer/Projucer.settings"
    settingsDir="/Users/krotos/Documents/Build_Server_Scripts/osx_setup_files/Projucer5"

settingsFilePath=os.path.join(settingsDir, "Projucer.settings")



if os.path.isfile(settingsFilePath):
    print "removing file " + settingsFilePath
    os.remove(settingsFilePath)


if not os.path.exists(settingsDir):
    print "making dir " + settingsDir
    os.makedirs(settingsDir)


print "copying " + savedSettingsPath
print "to " + settingsFilePath
copyfile(savedSettingsPath, settingsFilePath)