#!/usr/bin/python
###############################################################
#
#   This script contains functions for creating installers
#
################################################################
from xml.dom import minidom      # For edit_distribution
from xml.dom.minidom import Text # For edit_distribution
from fnmatch import fnmatchcase  # For edit_sound_rate_options
import xml.etree.ElementTree as elementTree # For add_sound_rate_options which needed more than what minidom could give
#import xml.dom.minidom
import os 
from random import randint
from shutil import rmtree
from shutil import copytree
from shutil import copy

# Warning! Do not change install_root to an external directory. 
#    Chmod commands will not work on ntfs and exfat systems and
#    the package permissions will be screwed up
installer_id  = str(randint(0, 10000))
packages_dir  = "/tmp/Packages" + installer_id
install_root  = "/tmp/geninstallroot" +  installer_id
scripts_dir   = "/tmp/scripts" + installer_id

# Source input paths
background_installer_path = "/Users/krotos/Documents/REFORMER BUILDS/Images/Reformer Installer.png"
license_installer_path    = "/Users/krotos/Documents/REFORMER LIBRARY BUILDS/License/Krotos Sound Library EULA.rtf"
license_path              = "/Users/krotos/Documents/REFORMER LIBRARY BUILDS/License/Krotos Sound Library EULA.pdf"
# Target installer paths
target_license_dir   = install_root + "/Applications/Krotos/Reformer/Documentation/EULA"
target_soundlib_dir  = install_root + "/Reformer Sample Libraries"
target_thumbnail_dir = install_root + "/Library/Application Support/Krotos/Reformer/Library/Thumbnails"
database_dir = "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"
samples_dir = "/Applications/Krotos/Reformer/Reformer Sample Libraries"
additional_samples = "/Applications/Krotos/Reformer/Additional Samples to Install"
target_additional_samples_path = install_root + "/Additional Samples to Install"
target_database_dir  = install_root + "/" + database_dir
example_database_path_location="/Applications/Krotos/Reformer"
new_thumbnails_dir="/Users/krotos/Documents/REFORMER LIBRARY BUILDS/New Thumbnails"

###############################################################
#   Removes install directory for when the package is built
#
def clean_installroot_dir():
    if os.path.isdir(install_root): rmtree(install_root)  

###############################################################
#   Removes install directory for when the package is built
#
def clean_package_dir():
    if os.path.isdir(packages_dir): rmtree(packages_dir)

###############################################################
#   Removes install directory for when the package is built
#
def clean_all_dirs():
    if os.path.isdir(install_root): rmtree(install_root)   
    if os.path.isdir(packages_dir): rmtree(packages_dir)
    if os.path.isdir(scripts_dir):  rmtree(scripts_dir)


###############################################################
#   copy_thumbnail_package copies a package containing extra thumbnails
#   
def copy_thumbnail_package(version):
    if not os.path.exists(packages_dir): os.makedirs(packages_dir)
    underscore_version=version.replace(".", "_")
    thumbnail_package="/Users/krotos/Documents/REFORMER LIBRARY BUILDS/Thumbnails_" + version + "/com.Krotos.reformer.thumbnails." + underscore_version + ".pkg"
    if not os.path.exists(thumbnail_package):
        print "installer_defs.copy_thumbnail_package: cannot find " + thumbnail_package
    else:
        print "including " + thumbnail_package
        copy(thumbnail_package, packages_dir)
        



###############################################################
#   run_package_command creates the pkgbuild command
#
def run_package_command( package_id, package_version):
    if not os.path.isdir(install_root): os.makedirs(install_root)
    if not os.path.isdir(packages_dir): os.makedirs(packages_dir)
    # Create package
    os.system("pkgbuild --root \"" + install_root + "\"" +
              " --identifier " + package_id + 
              " --version " + package_version + 
              " \"" + packages_dir + "/" + package_id + ".pkg\"")
    clean_installroot_dir()

###############################################################
#   run_package_command creates the pkgbuild command
#
def run_package_command_with_scripts( package_id, package_version):
    if not os.path.isdir(install_root): os.makedirs(install_root)
    if not os.path.isdir(packages_dir): os.makedirs(packages_dir)
    
    if not os.path.isfile(os.path.join(scripts_dir,"postinstall")): print "run_package_command_with_scripts: cannot find postinstall script"
    # Create package
    os.system("pkgbuild --root \"" + install_root + "\"" +
              " --scripts " + scripts_dir +
              " --identifier " + package_id + 
              " --version " + package_version + 
                      " \"" + packages_dir + "/" + package_id + ".pkg\"")
    clean_installroot_dir()

def analyse_package():
    os.system("pkgbuild --analyze --root \"" + install_root + "\" /tmp/test.plist")

###############################################################
#   run_productbuild_command creates the productbuild command
#   all .pkg files in inst.packages_dir will be added.
def run_productbuild_command( lib_name, output_dir, version_number ):
    # generate distribution.dist file
    distribution_file= packages_dir + "/distribution.dist"
    package_list=get_package_list()
    
    os.system("productbuild --synthesize" +   package_list + 
                            " \"" + distribution_file + "\"" )
    edit_distribution(distribution_file, lib_name)
    # Create installer
    if not os.path.isdir(output_dir): os.makedirs(output_dir)
    os.system("productbuild --distribution \"" + distribution_file + "\"" +
                          " --package-path \"" + packages_dir + "\" \"" + output_dir + "/" + lib_name + " Sound Library Installer.pkg\" " +
                          " --sign \"Developer ID Installer: Krotos LTD (3HX6KKGDLH)\"")



###############################################################
#   copy_thumbnail_package copies a package containing extra thumbnails
# 
def create_and_copy_thumbnail_package(version_number):
    if not os.path.isdir(install_root): os.makedirs(install_root)
    copytree(new_thumbnails_dir, target_thumbnail_dir)
    os.system("chmod 777 \"" + target_thumbnail_dir + "\"")
    run_package_command( "com.krotos.reformer.thumbnails.new" , version_number )


###############################################################
#   create_sound_rate_single_package creates a single
#     sample rate package
def create_sound_rate_single_package(name, sample_rate):
    database_sample_path=os.path.join(database_dir, name, "Clustering", sample_rate)
    target_database_sample_path=os.path.join(target_database_dir, name, "Clustering", sample_rate)
    copytree(database_sample_path, target_database_sample_path)
    os.system("chmod -R 777 \"" + target_database_dir + "\"")
    # This assigns a number to each sample rate package so they appear in order
    if  "44100" == sample_rate:
        order="0"
    elif "48000" == sample_rate:
        order="1"
    elif "88200" == sample_rate:
        order="2"
    elif "96000" == sample_rate:
        order="3"
    elif "176400" == sample_rate:
        order="4"
    elif "192000" == sample_rate:
        order="5"
    else:
        order="x"
    
    # Create package and cleanup install root
    run_package_command(create_package_name(name + ".rate." + order + "." + sample_rate), "1.0")


###############################################################
#   create_sound_rate_packages creates the sound rate pkg files
#
def create_sound_rate_packages(name):
    create_sound_rate_single_package(name, "44100")
    create_sound_rate_single_package(name, "48000")
    create_sound_rate_single_package(name, "88200")
    create_sound_rate_single_package(name, "96000")
    create_sound_rate_single_package(name, "176400")    
    create_sound_rate_single_package(name, "192000") 

###############################################################
#   get_package_list generates a list of packages to use
#   with the productbuild command
def get_package_list():
    package_list=""
    if not os.path.isdir(packages_dir):
        print "get_package_list error: " + packages_dir + " does not exist."
        return
    for filename in os.listdir(packages_dir):
        if filename.endswith(".pkg"):
            package_list  += " --package \"" + packages_dir + "/" + filename + "\""
        else:
            print "Warning " + filename + " in " + packages_dir + " will not be included in package."
    return package_list

###############################################################
#   get_provider gets the provider string from the bundle_name
#   e.g. Krotos form Krotos/Fruit and Veg
#
#   but also returns the subdir path if given the whole path
#   e.g.Krotos/Fruit and Veg/ from Krotos/Fruit and Veg/Fruit and Veg Crunch
def get_provider(string):
    if string.find('/')!=-1:provider = string.rsplit('/',1)[0]
    else:provider=string
    return provider

###############################################################
#   get_provider gets the provider string from the bundle_name
#   e.g. Krotos form Krotos/Fruit and Veg
def get_lib_name(string):
    if string.find('/')!=-1:lib_name = string.rsplit('/',1)[1]
    else:lib_name=string
    return lib_name

###############################################################
#   create_package_name generates a package name using
#     the library filename
def create_package_name(name):
	name=name.replace(" ", "")
	name=name.lower()
	return "com.krotos.reformer.soundlibs." + name

###############################################################
#   edit_distribution edits the xml file which is in charge  
#     of the installer details
def edit_distribution(filename, title):
    add_sound_lib_options(filename, title)
    add_distribution_cosmetics(filename, title)


###############################################################
#   add_sound_rate_options edits the distribution file to
#     include a page to select which sound rates to install
def add_sound_lib_options(filename, name):
    mainpkg=create_package_name( name )
    soundlibpkg=create_package_name( name + ".soundlib" )
    thumbnailpkg="com.krotos.reformer.thumbnails.new" 
    tree = elementTree.parse(filename)
    root = tree.getroot()
    # make <options customize="always">
    # this enables the choice to select packages
    for options in root.iter("options"):
        options.set('customize', 'always')

    # make the sample rates packages visible
    for choice in root.iter("choice"):
        # fnmatchcase lets us use globs in the search string
        if fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "0.44100"):
            choice.set('description', 'Install 44100 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '44100')
        elif fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "1.48000"):
            choice.set('description',  'Install 48000 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '48000')
        elif fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "2.88200"):
            choice.set('description', 'Install 88200 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '88200')
        elif fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "3.96000"):
            choice.set('description', 'Install 96000 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '96000')
        elif fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "4.176400"):
            choice.set('description', 'Install 176400 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '176400')
        elif fnmatchcase(choice.get('id'), "com.krotos.reformer.soundlibs.*.rate." + "5.192000"):
            choice.set('description', 'Install 192000 sample rate files')
            choice.set('visible', 'true')
            choice.set('title', '192000')
    
    # make the default package choice invisible
    for choice in root.iter("choice"):
        if choice.get('id') == "default":
            choice.set('visible', 'false')
    
    # change default to sample_rates because it's easier hijack and remake default
    # then it would be to make sample_rates from scratch
    path=".//choices-outline/line"
    for node in tree.findall(path):
         node.set('choice', 'sample_rates')
    # Create sample_rates choice tag as well.
    sampleChoice = elementTree.Element('choice')
    sampleChoice.set('id', 'sample_rates')
    sampleChoice.set('title', 'Sample Rates')
    sampleChoice.set('description', 'Choose which sample rates to install Reformers Sample Libraries at.')
    root.insert(1,sampleChoice)

   # Remove basic pkg from sample_rates
    for parent in root.findall('.//choices-outline/line'):
         for element in parent.findall('line'):
            if element.get('choice') in (mainpkg, soundlibpkg, thumbnailpkg):
                parent.remove(element)


    #Create new <line> element
    defaultline = elementTree.Element('line')
    defaultline.set('choice', 'default')
    #Create the inner <line> subelement
    defaultlinevalue = elementTree.SubElement(defaultline, 'line')
    defaultlinevalue.set('choice', mainpkg)
    defaultlinevalue = elementTree.SubElement(defaultline, 'line')
    defaultlinevalue.set('choice', thumbnailpkg)
    
    soundlibline = elementTree.Element('line')
    soundlibline.set('choice', 'soundlib')
    soundliblinevalues = elementTree.SubElement(soundlibline, 'line')
    soundliblinevalues.set('choice', soundlibpkg)

    # this line actually works, jesus!
    root.find('choices-outline').insert(0, defaultline)
    root.find('choices-outline').insert(0, soundlibline)
    
    # Create sample_rates choice tag as well.
    soundlibchoice = elementTree.Element('choice')
    soundlibchoice.set('id', 'soundlib')
    soundlibchoice.set('title', 'Sound sample library')
    soundlibchoice.set('description', 'Choose the location of the audio files for this Reformer Library, including on an external drive. Reformer requires certain preferences files to be installed to your local disk only. These cannot be installed to a custom location.')
    soundlibchoice.set('customLocation', example_database_path_location)
    soundlibchoice.set('customLocationAllowAlternateVolumes', 'yes')
    soundlibchoice.set('enabled', 'false')
    root.insert(1,soundlibchoice)
        
    tree.write(filename)
    

###############################################################
#   add_distribution_cosics edits the part of the xml file  
#    in charge of the installer details such as the background 
#    and title
def add_distribution_cosmetics(filename, title):
	dist_file = minidom.parse(filename)
	# set title
	title_xml = dist_file.createElement("title")
	nodetxt = dist_file.createTextNode(title)
	title_xml.appendChild(nodetxt)
	dist_file.childNodes[0].appendChild(title_xml)
	# set licence
	license_xml = dist_file.createElement("license")
	license_xml.setAttribute("file", license_installer_path)
	dist_file.childNodes[0].appendChild(license_xml)
	# set background
	background_xml = dist_file.createElement("background")
	background_xml.setAttribute("alignment", "centre")
	background_xml.setAttribute("file", background_installer_path)
	background_xml.setAttribute("scaling", "none")
	dist_file.childNodes[0].appendChild(background_xml)
	# set options
	options_xml = dist_file.createElement("options")
	options_xml.setAttribute("rootVolumeOnly", "true")
	options_xml.setAttribute("customize", "always")
	dist_file.childNodes[0].appendChild(options_xml)
	# set domain
	domain_xml = dist_file.createElement("domains")
	domain_xml.setAttribute("enable_localSystem", "true")
	domain_xml.setAttribute("enable_anywhere", "false")
	domain_xml.setAttribute("enable_currentUserHome", "false")
	dist_file.childNodes[0].appendChild(domain_xml)
	# update distribution.dist
	with open(filename, "wb") as f:
		dist_file.writexml(f)