#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.1.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path
thumbnails_version_number = "v1.1.0"
 
# Clothes Bundle
bundle_library_list=["Krotos/Leather",
                     "Krotos/Clothes Foley/Canvas Bag Foley",
                     "Krotos/Clothes Foley/Jacket Foley",
                     "Krotos/Clothes Foley/Jeans Foley",
                     "Krotos/Clothes Foley/Waterproofs Foley"]
bun_inst.create_bundle("Clothes Foley Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Clothes Foley", thumbnails_version_number)


# Rocks Bundle
bundle_library_list=["Krotos/Rocks/Bricks",
                     "Krotos/Rocks/Rocks",
                     "Krotos/Rocks/Stones and Gravel"]
bun_inst.create_bundle("Rocks Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Rocks", thumbnails_version_number)