#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.1.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path

thumbnails_version_number = "v1.1.4"
bundle_library_list=["Krotos/Surfaces/Concrete Foot Slides",
                     "Krotos/Surfaces/Concrete Hand Rubs",
                     "Krotos/Surfaces/Concrete On Concrete",
                     "Krotos/Surfaces/Concrete Slab On Gravel",
                     "Krotos/Surfaces/Grass Rustling",
                     "Krotos/Surfaces/Gravel Rolling On Wood",
                     "Krotos/Surfaces/Gravel Sliding",
                     "Krotos/Surfaces/Leaves and Twigs Slide",
                     "Krotos/Surfaces/Plywood On Concrete",
                     "Krotos/Surfaces/Shoes On Gravel",
                     "Krotos/Surfaces/Shoes On Leaves And Twigs",
                     "Krotos/Surfaces/Wood On Concrete",
                     "Krotos/Surfaces/Wood On Wood",
                     "Krotos/Surfaces/Wooden Plank On Gravel"]
#bun_inst.create_bundle("Surfaces Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Surfaces", thumbnails_version_number)


bundle_library_list=["Sounddogs/Animals/Bison",
                     "Sounddogs/Animals/Brown Bears",
                     "Sounddogs/Animals/Buffalo",
                     "Sounddogs/Animals/Horse Miniature",
                     "Sounddogs/Animals/Horse Neighs",
                     "Sounddogs/Animals/Horses",
                     "Sounddogs/Animals/Snake Hisses",
                     "Sounddogs/Animals/Snake Rattle",
                     "Sounddogs/Animals/Snakes"]
#bun_inst.create_bundle("Animals Bundle", bundle_library_list, output_root + "/Animals", thumbnails_version_number)


bundle_library_list=["Computer Music/CM_Drum Glitches",
                     "Computer Music/CM_Mother Madness",
                     "Computer Music/CM_Sci-fi Glitches"]
bun_inst.create_bundle("Computer Music Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/ComputerMusic", thumbnails_version_number)


