#!/usr/bin/env python
###############################################################
#
#   This script contains functions for creating installers
#
################################################################
from xml.dom import minidom      # For edit_distribution
from xml.dom.minidom import Text # For edit_distribution
import os 

packages_dir  = "/Volumes/KROTOS BACK/test/tmp/Packages"
install_root  = "/Volumes/KROTOS BACK/test/tmp/geninstallroot"
#packages_dir  = "/tmp/newSoundLibraryInstallers/Packages"
#install_root  = "/tmp/geninstallroot"
# Source input paths
background_installer_path = "/Users/krotos/Documents/REFORMER BUILDS/Images/Reformer Installer.png"
license_installer_path    = "/Users/krotos/Documents/REFORMER LIBRARY BUILDS/License/Krotos Sound Library EULA.rtf"
license_path              = "/Users/krotos/Documents/REFORMER LIBRARY BUILDS/License/Krotos Sound Library EULA.pdf"
# Target installer paths
target_license_dir   = install_root + "/Applications/Krotos/Reformer/Documentation/EULA"
target_soundlib_dir  = install_root + "/Applications/Krotos/Reformer/Reformer Sample Libraries"
database_dir = "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"
target_database_dir  = install_root + "/" +  database_dir


###############################################################
#   create_package_name generates a package name using
#     the library filename
def create_package_name(name):
	name=name.replace(" ", "")
	name=name.lower()
	return "com.Krotos.reformer.soundlibs." + name


###############################################################
#   edit_distribution edits the xml file which is in charge  
#     of the installer details
def edit_distribution(filename, title):
	dist_file = minidom.parse(filename)
	# set title
	title_xml = dist_file.createElement("title")
	nodetxt = dist_file.createTextNode(title)
	title_xml.appendChild(nodetxt)
	dist_file.childNodes[0].appendChild(title_xml)
	# set licence
	license_xml = dist_file.createElement("license")
	license_xml.setAttribute("file", license_installer_path)
	dist_file.childNodes[0].appendChild(license_xml)
	# set background
	background_xml = dist_file.createElement("background")
	background_xml.setAttribute("alignment", "centre")
	background_xml.setAttribute("file", background_installer_path)
	background_xml.setAttribute("scaling", "none")
	dist_file.childNodes[0].appendChild(background_xml)
	# set options
	options_xml = dist_file.createElement("options")
	options_xml.setAttribute("rootVolumeOnly", "true")
	dist_file.childNodes[0].appendChild(options_xml)
	# set domain
	domain_xml = dist_file.createElement("domain")
	domain_xml.setAttribute("enable_localSystem", "true")
	dist_file.childNodes[0].appendChild(domain_xml)
	# update distribution.dist
	with open(filename, "wb") as f:
		dist_file.writexml(f)