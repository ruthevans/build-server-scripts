#!/usr/bin/env python
#-----------------------------------------------------------------------
#
# This script creates a package of additional files for Computer FX
#
#   Usage: python create_additional_packages.py <bundle provider/name>
#          e.g.: python Create_AdditionalPkg_ComputerFX.py "Soundbits/Computer FX"
#-----------------------------------------------------------------------
import os
import shutil
import argparse
import installer_definitions as inst

parser = argparse.ArgumentParser()
parser.add_argument("bundle_name", help="Name of bundle e.g. krotos")
args = parser.parse_args()

additional_files_dir="/Applications/Krotos/Reformer/Additional Samples to Install/" + args.bundle_name


###############################################################
#   run_package_command creates the pkgbuild command
#
lib_name=os.path.basename(args.bundle_name)
print "Creating package for " + lib_name
package_id=inst.create_package_name(lib_name + ".additional");
distribution_file= inst.packages_dir + "/distribution.dist"
if not os.path.isdir(inst.install_root): os.makedirs(inst.install_root)
if not os.path.isdir(inst.packages_dir): os.makedirs(inst.packages_dir)
if not os.path.isdir(inst.target_soundlib_dir): os.makedirs(inst.target_soundlib_dir)

# Set up files
if not os.path.isdir(additional_files_dir):
    print("Cannot find " + additional_files_dir)
    quit(1)
else:
    shutil.copytree(additional_files_dir,inst.target_soundlib_dir + "/" + args.bundle_name)

# Create package
os.system("pkgbuild --root \"" + inst.install_root + "\"" + 
                  " --identifier " + package_id + 
                  " --version " + "1.0.0" + 
                  " \"" + inst.packages_dir + "/" + package_id + ".pkg\"")

if os.path.isdir(inst.install_root): shutil.rmtree(inst.install_root) 






