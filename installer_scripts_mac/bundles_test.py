#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.1.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path

thumbnails_version_number = "v1.1.5"
bundle_library_list=["Krotos/test"]
bun_inst.create_bundle("Test", bundle_library_list, output_root + "/testinstaller", "")
