#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.1.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path

thumbnails_version_number = "v1.1.2"
bundle_library_list=["Krotos/Electric Lasers",
                     "Krotos/Electric Razor",
                     "Krotos/Gritty Electronics",
                     "Krotos/Pulsing Electronics"]
bun_inst.create_bundle("Electronics Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Electronics", thumbnails_version_number)


thumbnails_version_number = "v1.1.3"
bundle_library_list=["Krotos/Water Bubbles",
                     "Krotos/Water Dropping",
                     "Krotos/Water Hydrophone",
                     "Krotos/Water Pouring",
                     "Krotos/Water Splashing"]
#bun_inst.create_bundle("Water Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Water", thumbnails_version_number)



