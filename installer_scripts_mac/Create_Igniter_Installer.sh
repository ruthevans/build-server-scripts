#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates an igniter plugin installer
#
#  Usage: ./Create_Igniter_Installer [optional] --demo
#
#-----------------------------------------------------------------------

if [[ $1 == --demo ]]; then
	DEMO=true
	echo "Building Demo installer..."
fi

#Exit on error
set -e
VERSION_NUMBER=1.0
if [ $DEMO ]; then
	INSTALLER_TITLE="Krotos Igniter Demo"
else
	INSTALLER_TITLE="Krotos Igniter"
fi

echo $INSTALLER_TITLE

# Create the example root folder for this product
INSTALL_ROOT="/tmp/ignrootbb$RANDOM"
PACKAGE_PATH="/tmp/ignpackagesbb$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Igniter"
BUNDLE_OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Igniter"
INPUT_PATH="/Users/$USER/Documents/IGNITER BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Engines"

# Package install destination paths
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Igniter/Presets/main"
ASSETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Igniter/Library/Factory Assets"
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Igniter/Documentation"
DATABASE_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Igniter/Databases/"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Igniter"

mkdir -p "$PACKAGE_PATH"

# Create vst package
echo "Creating vst package..."
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Igniter.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/VST_package" --identifier com.Krotos.Igniter.vst --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.vst.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create au package 
echo "Creating au package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Igniter.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AU_package" --identifier com.Krotos.Igniter.au --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.au.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create aax package 
echo "Creating aax package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Igniter.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/AAX_package" --identifier com.Krotos.Igniter.aax --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.aax.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH/Manual"
mkdir -p "$DOCUMENT_PATH/EULA"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Manual/Manual Beta/Igniter Manual.pdf" "$DOCUMENT_PATH/Manual/Igniter Manual.pdf"
cp -pR "$DROPBOX_INPUT_PATH/Documents/EULA/Igniter License.pdf" "$DOCUMENT_PATH/EULA/Igniter License.pdf"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Documentation --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

if [ -z $DEMO ]; then
# Create presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/Release/Mac/"*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

if [ $DEMO ]; then
# Create presets package
echo "Creating Demo Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/Demo/Mac/"*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.DemoPresets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create database package
echo "Creating Database package..."
mkdir -p "$DATABASE_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Database/"*.db "$DATABASE_PATH"
chmod -R 777 "$DATABASE_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Databases --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.Databases.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

if [ -z $DEMO ]; then
# Create assets package
echo "Creating Assets package..."
mkdir -p "$ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Assets/Release Assets/"*   "$ASSETS_PATH"
chmod -R 777 "$ASSETS_PATH"/..
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Assets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.Assets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

if [ $DEMO ]; then
# Create demo assets package
echo "Creating Demo Assets package..."
mkdir -p "$ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Assets/Demo Assets/"*   "$ASSETS_PATH"
chmod -R 777 "$ASSETS_PATH"/..
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Igniter.Assets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.DemoAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create analytics package
echo "Creating Analytics package..."
pkgbuild  --nopayload --scripts "$INPUT_PATH/Scripts/analytics_package" --identifier com.Krotos.Igniter.Analytics --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Igniter.Analytics.pkg"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Uninstall Igniter.app" "$UNINSTALLER_PATH/"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/igniter_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/igniter_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/igniter_uninstaller.plist --identifier com.Krotos.Igniter.Uninstaller --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Igniter.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
#echo ""
########################################################
# Build the installer
########################################################
if [ $DEMO ]; then
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.vst.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.au.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.aax.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Databases.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Analytics.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.DemoPresets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.DemoAssets.pkg \
    "$PACKAGE_PATH"/distribution.dist
else
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.vst.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.au.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.aax.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Databases.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Analytics.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Igniter.Assets.pkg \
     "$PACKAGE_PATH"/distribution.dist
fi


sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>'"$INSTALLER_TITLE"'</title>    \
\ \   <license file="'"$DROPBOX_INPUT_PATH"'/Documents/EULA/Igniter License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$INPUT_PATH"'/Images/Igniter Installer for OS X 623x349_2.png" scaling="none"/>\
	' "$PACKAGE_PATH"/distribution.dist

# Finish up
if [ $DEMO ]; then
	INSTALLER_NAME="Igniter Demo Installer.pkg"
else
	INSTALLER_NAME="Igniter Installer.pkg"
fi

mkdir -p "$BUNDLE_OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --plugins "$INPUT_PATH/InstallerPanePlugins" --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/$INSTALLER_NAME"  --version "$VERSION_NUMBER" --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/$INSTALLER_NAME"

# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "$INPUT_PATH/Plugins/*"
