#!/bin/bash

installer_path=$1

tmpfile="/tmp/notarize-app-"

RED="\033[1;31m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
MAGENTA="\033[1;35m"
CYAN="\033[1;36m"
WHITE="\033[1;37m"
RESET="\033[0m"

echo "Running altool..."
xcrun altool --notarize-app -f "$installer_path" -t osx -u orfeas@krotosaudio.com -p "@keychain:AC_PASSWORD" --primary-bundle-id "krotos.concept.pkg"  >& "$tmpfile"
altool_status=$?
echo "altool status is: $altool_status"

if [[ "$altool_status" == "0" ]]; then
	echo "Installer submitted successfully"
    uuid=$(grep "RequestUUID = " "$tmpfile" | sed -e 's/.* = //')
else
	echo "that didn't work sorry."
fi

echo "uuid: $uuid" 

#xcrun altool --notarization-info "$uuid" -u krotosltd@gmail.com -p "@keychain:AC_PASSWORD" #  >> "$tmpfile" 2>&1 || continue;

#xcrun altool --notarization-history 0 -u krotosltd@gmail.com -p "@keychain:AC_PASSWORD"

numtry=0
ok=0
if [ "$uuid" != "" ]; then
     while true; do
        numtry=$((numtry+1))
        if (( $numtry > 1 )); then
             sleep 30;
        fi
        if (( $numtry > 50 )); then
            die "two many attempts, notarization seem to take foreever..."
        fi
        echo "Fetching updates..."
        xcrun altool --notarization-info "$uuid" -u krotosltd@gmail.com -p "@keychain:AC_PASSWORD" >> "$tmpfile" 2>&1 || continue;
        status=$(tail -3 "$tmpfile" | grep "Status Message" | sed -e 's/Status Message.*: *//')
        #sec=$(($(date +%s) - $(stat -t %s -f %m -- "$uuidfile"))); # number of seconds since the creation of the file
        echo -e "$tmpfile attempt #$numtry: $YELLOW$status$RESET";
        if [[ "$status" == *"Package Approved"* ]]; then
            ok=1
            break;
        fi
        if [ "$status" != "" ]; then
            ok=0;
            tail -5 "$tmpfile";
            break;
        fi
    done
fi

if (( $ok )); then
    xcrun stapler staple "$installer_path"
    echo -e "Application $GREEN Concept Installer.pkg$RESET stapled!!"
else
	echo -e "Application Concept Installer.pkg $REDfailed!!$RESET"
    exit 42
fi