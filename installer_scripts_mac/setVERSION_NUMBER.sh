#!/bin/bash
# Stolen and adapted from https://apple.stackexchange.com/questions/127561/script-to-check-software-version
app_path="$1"

#Get the line, regardless of whether it is correct
version_line=$(plutil -p "${app_path}/Contents/Info.plist" | grep "CFBundleShortVersionString")
if [[ $? -ne 0 ]]; then
    version_line=$(plutil -p "${app_path}/Contents/Info.plist" | grep "CFBundleVersion")
    if [[ $? -ne 0 ]]; then #if it failed again, the app is not installed at that path
        echo "$app_path not installed at all"
        exit 123456
    fi
fi
# hahahahaha this is so long I'm sorry. sed1 removes most of the extra text, sed2 removed the quotes around the left over 
# text and sed3 replaced the full stops with underscores so this data can be used in the installers
real_version=$(echo "$version_line" | sed 's/^.*=> //g' | sed 's/"//g')
export VERSION_NUMBER="$real_version"
