#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Reformer Pro installer for OS X 
#
#  Usage: ./Create_Reformer_Pro_Installer.sh <arch>
#   e.g.: ./Create_Reformer_Pro_Installer.sh 64
#-----------------------------------------------------------------------
#Exit on error
set -e

ARCH=$1
#VERSION_NUMBER=$2

if [[ "$ARCH" == "32" ]]; then
	HOSTARCH="i386"
elif [[ "$ARCH" == "64" ]]; then
	HOSTARCH="x86_64"
else 
	echo "Arch not valid."
	echo "Usage: ./Create_Reformer_Pro_Installer.sh <arch> <version number>"
	echo "e.g.   ./Create_Reformer_Pro_Installer.sh 64 1.1"
	exit 1
fi

# Update distribution.dist if changing this number
VERSION_NUMBER=1.1


# Create the example root folder for this product
INSTALL_ROOT="/tmp/refproroot"
PACKAGE_PATH="/tmp/refpropackages"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Reformer Pro"
INPUT_PATH="/Users/$USER/Documents/REFORMER PRO BUILDS"
# Package install destination paths
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer Pro/Documentation"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer Pro/Library/Presets/main"
REFORMER_LIBRARY_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
CONVERTED_SAMPLES_PATH="$INSTALL_ROOT/Reformer Sample Libraries/Krotos"
THUMBNAILS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Thumbnails"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer Pro"
ERROR_LOG_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Logs"
SETTINGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer Pro"
OFFLINE_TOOL_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer Pro"
DROPBOX_PATH="/Users/krotos/Dropbox (Krotos)/Development/Reformer/Reformer Pro"

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH"
cp -pR "$DROPBOX_PATH/Documentation/" "$DOCUMENT_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Documentation --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_PATH/Presets/OS X"/* "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Presets --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create offline tool package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Offline Tool package..."
mkdir -p "$OFFLINE_TOOL_PATH"
cp -pR "$INPUT_PATH/Binaries/AnalysisTool.app" "$OFFLINE_TOOL_PATH"
chmod -R 777 "$OFFLINE_TOOL_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/offlinetool.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/offlinetool.plist
plutil -replace BundleOverwriteAction -string upgrade "$PACKAGE_PATH"/offlinetool.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/offlinetool.plist --identifier com.Krotos.Reformer.Pro.AnalysisTool --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.AnalysisTool.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

################ Reformer Libraries Options #####################################
# Create Reformer Libraries package 
echo "Creating Reformer Libraries package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/versionNumber.txt" "$REFORMER_LIBRARY_PATH/Black Leopard Pro"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/versionNumber.txt"    "$REFORMER_LIBRARY_PATH/Electronic Pro"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/versionNumber.txt"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/versionNumber.txt" "$REFORMER_LIBRARY_PATH/Leather Foley Pro"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/libraries_package" --identifier com.Krotos.Reformer.Pro.Libraries --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 44100
echo "Creating Reformer Libraries 44100 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/44100
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/44100" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/44100"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/44100"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/44100" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.0.44100 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.0.44100.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 48000
echo "Creating Reformer Libraries 48000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/48000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/48000" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/48000"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/48000"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/48000" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.1.48000 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.1.48000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 88200
echo "Creating Reformer Libraries 88200 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/88200
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/88200" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/88200"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/88200"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/88200" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.2.88200 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.2.88200.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 96000
echo "Creating Reformer Libraries 96000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/96000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/96000" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/96000"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/96000"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/96000" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.3.96000 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.3.96000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 176400
echo "Creating Reformer Libraries 176400 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/176400
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/176400" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/176400"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/176400"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/176400" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.4.176400 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.4.176400.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 192000
echo "Creating Reformer Libraries 192000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Black Leopard Pro"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Electronic Pro"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit Squash Pro"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Leather Foley Pro"/Clustering/192000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard Pro/Clustering/192000" "$REFORMER_LIBRARY_PATH/Black Leopard Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Electronic Pro/Clustering/192000"    "$REFORMER_LIBRARY_PATH/Electronic Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit Squash Pro/Clustering/192000"  "$REFORMER_LIBRARY_PATH/Fruit Squash Pro/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Leather Foley Pro/Clustering/192000" "$REFORMER_LIBRARY_PATH/Leather Foley Pro/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Libraries.5.192000 --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Libraries.5.192000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

#################################################################################


# Create Converted Samples packge
echo "Creating Converted Samples package..."
mkdir -p "$CONVERTED_SAMPLES_PATH/Fruit and Veg"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Black Leopard" "$CONVERTED_SAMPLES_PATH/Black Leopard"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Electronic"    "$CONVERTED_SAMPLES_PATH/Electronic"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Leather"       "$CONVERTED_SAMPLES_PATH/Leather"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Fruit and Veg/Fruit and Veg Squash" "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Squash"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Black Leopard"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Electronic"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Squash"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Leather"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/samples_package" --identifier com.Krotos.Reformer.Pro.ConvertedSamples --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.ConvertedSamples.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create VST package
echo "Creating VST package..."
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins"$ARCH"/Reformer Pro.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/VST_package"  --identifier com.Krotos.Reformer.Pro.VstPlugin --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.VstPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AAX package
echo "Creating AAX package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins"$ARCH"/Reformer Pro.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AAX_package" --identifier com.Krotos.Reformer.Pro.AaxPlugin --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.AaxPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AU package 
echo "Creating AU package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins"$ARCH"/Reformer Pro.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AU_package" --identifier com.Krotos.Reformer.Pro.AuPlugin --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.AuPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Thumbnails package
echo "Creating Thumbails package..."
mkdir -p "$THUMBNAILS_PATH"
cp -pR "/Users/krotos/Documents/REFORMER BUILDS/Images/Thumbnails/" "$THUMBNAILS_PATH"
chmod -R 777 "$THUMBNAILS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Thumbnails --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Thumbnails.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Settings package
echo "Creating Settings package..."
mkdir -p "$SETTINGS_PATH"
cp -pR "$INPUT_PATH/settings.dat" "$SETTINGS_PATH"
chmod -R 777 "$SETTINGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/settings_package" --identifier com.Krotos.Reformer.Pro.Settings --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Settings.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create analytics package
echo "Creating Analytics package..."
pkgbuild  --nopayload --scripts "$INPUT_PATH/Scripts/analytics_package" --identifier com.Krotos.Reformer.Pro.Analytics --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Analytics.pkg"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Reformer Pro Uninstaller.app" "$UNINSTALLER_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/reformer_pro_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/reformer_pro_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/reformer_pro_uninstaller.plist --identifier com.Krotos.Reformer.Pro.UninstallReformer --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Reformer.Pro.UninstallReformer.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# If for whatever reason distribution.dist needs updating, re-enable this command.
# you will have to rewrite the modifications but these are detailed on the wiki at 'Create an OSX software installer'
# Using filemerge on the old and new dist can be super helpful
#productbuild --synthesize \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.VstPlugin.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.AuPlugin.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.AaxPlugin.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.0.44100.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.1.48000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.2.88200.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.3.96000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.4.176400.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Libraries.5.192000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.ConvertedSamples.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Presets.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Thumbnails.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Documentation.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Settings.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.UninstallReformer.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.AnalysisTool.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Analytics.pkg \
#    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
#sed -i '' '3i\
#\ \   <!-- Custom xml -->\
#\ \   <title>Reformer Pro Beta('$ARCH'-bit)</title>    \
#\ \   <license file="/Users/'$USER'/Documents/REFORMER PRO BUILDS/Documentation/EULA/Reformer Pro License.rtf" mime-type="" uti=""/>\
#\ \   <background alignment="center" file="/Users/'$USER'/Documents/REFORMER PRO BUILDS/Images/Reformer Pro Installer.png" scaling="none"/>\
#\ \   <domains enable_localSystem="true"/>\
#\ \   <options rootVolumeOnly="true"/>\
#	' "$PACKAGE_PATH"/distribution.dist

sed s/"<title>Reformer Pro"/"<title>Reformer Pro ($ARCH-bit)"/ < "$INPUT_PATH"/distribution.dist > "$PACKAGE_PATH"/distribution.dist

# Finish up
# Use the following command to test signature
#spctl --assess --verbose --type install Reformer.pkg

mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/Reformer Pro "$ARCH".pkg" --plugins "$INPUT_PATH/InstallerPanePlugins" --version "$VERSION_NUMBER" --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/Reformer Pro "$ARCH".pkg"

# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "INSTALL_ROOT"
