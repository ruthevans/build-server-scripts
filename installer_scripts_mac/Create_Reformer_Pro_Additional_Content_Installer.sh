#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Reformer Pro additional content installer for OS X 
#
#-----------------------------------------------------------------------
#Exit on error
set -e
VERSION_NUMBER=1.0
# Create the example root folder for this product
INSTALL_ROOT="/tmp/refproacroot"
PACKAGE_PATH="/tmp/refproacpackages"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Reformer Pro Additional Content"
INPUT_PATH="/Users/$USER/Documents/REFORMER PRO BUILDS"
# Package install destination paths
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer Pro/Library/Presets/main/Additional Content"
REFORMER_LIBRARY_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"
CONVERTED_SAMPLES_PATH="$INSTALL_ROOT/Reformer Sample Libraries/Krotos"
METADATA_PATH="$INSTALL_ROOT/Reformer Sample Libraries/Krotos/Metadata"
THUMBNAILS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Thumbnails"
OFFLINE_TOOL_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer Pro"
DROPBOX_PATH="/Users/krotos/Dropbox (Krotos)/Development/Reformer/Reformer Pro"


mkdir -p "$PACKAGE_PATH"

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_PATH/Presets For Additional Content/OS X"/* "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

################ Reformer Libraries Options #####################################
# Create Reformer Libraries package 
echo "Creating Reformer Libraries package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/versionNumber.txt"         "$REFORMER_LIBRARY_PATH/Bengal Tiger"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/versionNumber.txt"          "$REFORMER_LIBRARY_PATH/Polystyrene"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/versionNumber.txt"               "$REFORMER_LIBRARY_PATH/Sizzle"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/versionNumber.txt"            "$REFORMER_LIBRARY_PATH/Gun Foley"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/versionNumber.txt"        "$REFORMER_LIBRARY_PATH/Fruit and Veg"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/versionNumber.txt" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/versionNumber.txt"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"

chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/libraries_package" --identifier com.Krotos.Reformer.Pro.Additional.Libraries --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 44100
echo "Creating Reformer Libraries 44100 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/44100
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/44100
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/44100"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/44100"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/44100"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/44100"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/44100"        "$REFORMER_LIBRARY_PATH/Fruit and Veg/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/44100" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/44100"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.0.44100 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.0.44100.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 48000
echo "Creating Reformer Libraries 48000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/48000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/48000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/48000"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/48000"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/48000"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/48000"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/48000"        "$REFORMER_LIBRARY_PATH/Fruit and Veg/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/48000" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/48000"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.1.48000 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.1.48000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 88200
echo "Creating Reformer Libraries 88200 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/88200
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/88200
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/88200"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/88200"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/88200"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/88200"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/88200"        "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/88200" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/88200"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.2.88200 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.2.88200.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 96000
echo "Creating Reformer Libraries 96000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/96000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/96000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/96000"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/96000"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/96000"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/96000"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/96000"        "$REFORMER_LIBRARY_PATH/Fruit and Veg/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/96000" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/96000"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.3.96000 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.3.96000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 176400
echo "Creating Reformer Libraries 176400 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/176400
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/176400
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/176400"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/176400"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/176400"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/176400"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/176400"        "$REFORMER_LIBRARY_PATH/Fruit and Veg/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/176400" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/176400"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.4.176400 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.4.176400.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package 192000
echo "Creating Reformer Libraries 192000 package..."
mkdir -p "$REFORMER_LIBRARY_PATH/Bengal Tiger"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Polystyrene"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Sizzle"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Gun Foley"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch"/Clustering/192000
mkdir -p "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs"/Clustering/192000
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Bengal Tiger/Clustering/192000"         "$REFORMER_LIBRARY_PATH/Bengal Tiger/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Polystyrene/Clustering/192000"          "$REFORMER_LIBRARY_PATH/Polystyrene/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Sizzle/Clustering/192000"               "$REFORMER_LIBRARY_PATH/Sizzle/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Gun Foley/Clustering/192000"            "$REFORMER_LIBRARY_PATH/Gun Foley/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg/Clustering/192000"        "$REFORMER_LIBRARY_PATH/Fruit and Veg/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Crunch/Clustering/192000" "$REFORMER_LIBRARY_PATH/Fruit and Veg Crunch/Clustering"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Fruit and Veg Stabs/Clustering/192000"  "$REFORMER_LIBRARY_PATH/Fruit and Veg Stabs/Clustering"
chmod -R 0777 "$REFORMER_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Pro.Additional.Libraries.5.192000 --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.Libraries.5.192000.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

#################################################################################

# Create Converted Samples packge
echo "Creating Converted Samples package..."
mkdir -p "$CONVERTED_SAMPLES_PATH/Fruit and Veg"
mkdir -p "$CONVERTED_SAMPLES_PATH/Metadata"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Bengal Tiger"  "$CONVERTED_SAMPLES_PATH/Bengal Tiger"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Polystyrene"   "$CONVERTED_SAMPLES_PATH/Polystyrene"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Sizzle"        "$CONVERTED_SAMPLES_PATH/Sizzle"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Gun Foley"     "$CONVERTED_SAMPLES_PATH/Gun Foley"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Fruit and Veg/Fruit and Veg"        "$CONVERTED_SAMPLES_PATH/Fruit and Veg"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Fruit and Veg/Fruit and Veg Crunch" "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Crunch"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Fruit and Veg/Fruit and Veg Stabs"  "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Stabs"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Bengal Tiger"*         "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Black Leopard"*        "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Electronic"*                  "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Leather Foley"*               "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Polystyrene"*          "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Sizzle"*               "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Gun Foley"*            "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Fruit and Veg Full"*   "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Fruit and Veg Crunch"* "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Fruit and Veg Stabs"*  "$METADATA_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Metadata/Krotos Fruit and Veg Squash"* "$METADATA_PATH"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Bengal Tiger"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Polystyrene"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Sizzle"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Gun Foley"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Crunch"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Fruit and Veg/Fruit and Veg Stabs"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Metadata"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/AdditionalContent/Scripts/samples_package" --identifier com.Krotos.Reformer.Pro.Additional.ConvertedSamples --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Reformer.Pro.Additional.ConvertedSamples.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# If for whatever reason distribution.dist needs updating, re-enable this command.
# you will have to rewrite the modifications but these are detailed on the wiki at 'Create an OSX software installer'
#productbuild --synthesize \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.0.44100.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.1.48000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.2.88200.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.3.96000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.4.176400.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Libraries.5.192000.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.ConvertedSamples.pkg \
#    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Pro.Additional.Presets.pkg \
#    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
#sed -i '' '3i\
#\ \   <!-- Custom xml -->\
#\ \   <title>Reformer Pro Beta('$ARCH'-bit)</title>    \
#\ \   <license file="/Users/'$USER'/Documents/REFORMER PRO BUILDS/Documentation/EULA/Reformer Pro License.rtf" mime-type="" uti=""/>\
#\ \   <background alignment="center" file="/Users/'$USER'/Documents/REFORMER PRO BUILDS/Images/Reformer Pro Installer.png" scaling="none"/>\
#\ \   <domains enable_localSystem="true"/>\
#\ \   <options rootVolumeOnly="true"/>\
#	' "$PACKAGE_PATH"/distribution.dist


# Finish up
# Use the following command to test signature
#spctl --assess --verbose --type install Reformer.pkg

mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$INPUT_PATH"/AdditionalContent/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/Reformer Pro Additional Subscriber Content.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/Reformer Pro Additional Subscriber Content.pkg"

# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "INSTALL_ROOT"
