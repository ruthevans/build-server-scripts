#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library installers
#
#   Usage: python generate_library_installers.py /path/to/sound/libs /path/to/output/dir provider
#          e.g. python generate_library_installers.py "/Users/krotos/Dropbox (Krotos)/Development/reformer/reformer Libraries/Krotos Reformer Libraries" /tmp/krotos_sound_libs Krotos
###############################################################
import os
import shutil
import argparse
import installer_definitions as inst

parser = argparse.ArgumentParser()
parser.add_argument("lib_path", help="Path to the sound libraries")
parser.add_argument("installer_out_path", help="Output path for the generated installers")
parser.add_argument("bundle_name", help="Name of bundle e.g. krotos")
args = parser.parse_args()

bundle_dir    = args.bundle_name
soundlib_path = args.lib_path
finished_installer_dir = args.installer_out_path


###############################################################
#   prepare_package copies files to root_install
#
def prepare_package(name, bundle_subdir):
    if args.bundle_name.find('/')!=-1:provider=args.bundle_name.rsplit('/',1)[0]
    else:provider=args.bundle_name
    samples_path=soundlib_path + "/" + name
    database_path=os.path.join(inst.database_dir, name)
    print "provider is: " + provider
    # License
    if not os.path.exists(inst.target_license_dir):os.makedirs(inst.target_license_dir)
    shutil.copyfile(inst.license_path, inst.target_license_dir + "/Krotos Sound Library EULA.pdf")
    # Databases
    if not os.path.exists(inst.target_database_dir):os.makedirs(inst.target_database_dir )
    if not os.path.exists(database_path + "/Clustering"):
    	#note If this doesn't exists when we're looking at a bundle's subdir and should skip.
    	return False
    else:
    	shutil.copytree(database_path, os.path.join(inst.target_database_dir, name))
    	os.system("chmod -R 777 \"" + os.path.join(inst.target_database_dir, name, "\""))
    # Sound libraries
    if not os.path.exists(os.path.join(inst.target_soundlib_dir, bundle_subdir)):os.makedirs(os.path.join(inst.target_soundlib_dir, bundle_subdir))
    shutil.copytree(samples_path, os.path.join(inst.target_soundlib_dir, bundle_subdir, name))
    os.system("chmod -R 777 \"" + os.path.join(inst.target_soundlib_dir, bundle_subdir, name, "\"")) 
    
    # Metadata
    metadata_path=os.path.join( "/Applications/Krotos/Reformer/Reformer Sample Libraries",provider, "Metadata")
    target_metadata_path=os.path.join(inst.target_soundlib_dir, provider, "Metadata")
    
    library_metadata_found=False
    #this is the case for krotos metadata
    print "looking for metadata at "+metadata_path
    if os.path.exists(metadata_path):
        print "I should find the meta data here at " + metadata_path + "/" + provider + " " + name
        for filename in os.listdir(metadata_path):
            if filename.startswith(provider + " " + name):
    	        library_metadata_found=True
                if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
        #this is the case for soundbits and boom metadata
        if not library_metadata_found:
            if args.bundle_name.find('/') !=-1:just_bundle_name=args.bundle_name.rsplit('/',1)[1]
            else: just_bundle_name=""           
            for filename in os.listdir(metadata_path):
                 if filename.startswith(provider + " " + just_bundle_name):
                    if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                    shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
    else:
    	print "Warning: No metadata found for " + name
    return True


###############################################################
#   run_package_command creates the pkgbuild command
#
def run_package_command( lib_name, package_version):
    package_id=inst.create_package_name(lib_name);
    distribution_file= inst.packages_dir + "/distribution.dist"
    if not os.path.isdir(inst.install_root): os.makedirs(inst.install_root)
    if not os.path.isdir(inst.packages_dir): os.makedirs(inst.packages_dir)
    # Create package
    os.system("pkgbuild --root \"" + inst.install_root + "\"" +
                      " --identifier " + package_id + 
                      " --version " + package_version + 
                      " \"" + inst.packages_dir + "/" + package_id + ".pkg\"")
    # generate distribution.dist file
    os.system("productbuild --synthesize" +
                          " --package \"" + inst.packages_dir + "/" + package_id + ".pkg\" " + 
                            "\"" + distribution_file + "\"" )
    inst.edit_distribution(distribution_file, lib_name)
    # Create installer
    if not os.path.isdir(finished_installer_dir): os.makedirs(finished_installer_dir)
    os.system("productbuild --distribution \"" + distribution_file + "\"" +
                          " --package-path \"" + inst.packages_dir + "\" \"" + finished_installer_dir + "/" + lib_name + " Sound Library Installer.pkg\"" + 
                          " --sign \"Developer ID Installer: Krotos LTD (3HX6KKGDLH)\"")
    os.remove(distribution_file)
    
###############################################################
#   Removes install directory for when the package is built
#
def clean_package_dir():
    if os.path.isdir(inst.install_root): shutil.rmtree(inst.install_root)   
    if os.path.isdir(inst.packages_dir): shutil.rmtree(inst.packages_dir) 

###############################################################
#   Checks the existence of the directories given
#
def check_arguments():
	if not os.path.isdir(args.lib_path):
		print "Error: Could not find sound library directory " + args.lib_path 
		quit(1) 
	if not os.path.isdir(args.installer_out_path):
		os.makedirs(args.installer_out_path)
	print("about to make " + args.lib_path)
	if not os.path.isdir(inst.packages_dir):
		os.makedirs(inst.packages_dir)
	   	

###############################################################
#   Main starts here
#
check_arguments()
# Create an installer for every folder in soundlib_path
clean_package_dir()
for filename in os.listdir(soundlib_path):
    if os.path.isdir(soundlib_path + "/" + filename):     
		if prepare_package(filename, bundle_dir):
			print "Building installer for " + filename   
			run_package_command(filename, "1.0")
    clean_package_dir()


