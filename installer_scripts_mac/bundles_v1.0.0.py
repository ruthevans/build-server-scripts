#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.0.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path
thumbnails_version_number = "none"

##############################################################
# Fruit and Veg Bundle
##############################################################
bundle_library_list=["Krotos/Fruit and Veg/Fruit and Veg",
                     "Krotos/Fruit and Veg/Fruit and Veg Crunch",
                     "Krotos/Fruit and Veg/Fruit and Veg Squash",
                     "Krotos/Fruit and Veg/Fruit and Veg Stabs"]
#bun_inst.create_bundle("Fruit and Veg Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac/Fruit and Veg", thumbnails_version_number)

##############################################################
# Krotos Bundle
##############################################################
bundle_library_list=["Krotos/Bengal Tiger",
                     "Krotos/Electronic",
                     "Krotos/Fruit and Veg/Fruit and Veg",
                     "Krotos/Fruit and Veg/Fruit and Veg Crunch",
                     "Krotos/Fruit and Veg/Fruit and Veg Squash",
                     "Krotos/Fruit and Veg/Fruit and Veg Stabs",
                     "Krotos/Gun Foley",
                     "Krotos/Leather",
                     "Krotos/Polystyrene",
                     "Krotos/Sizzle"]
#bun_inst.create_bundle("Krotos Bundle", bundle_library_list, output_root + "/Krotos Reformer Libraries/Mac", thumbnails_version_number)



##############################################################
# Computer FX bundle
##############################################################
bundle_library_list=["SoundBits/Computer FX/Alarms",
                     "SoundBits/Computer FX/Computer One Shots",
                     "SoundBits/Computer FX/Glitch Malfunction",
                     "SoundBits/Computer FX/Loading Processing",
                     "SoundBits/Computer FX/Mechanical",
                     "SoundBits/Computer FX/Retro 8bit"]
#bun_inst.create_bundle("Computer FX Bundle", bundle_library_list, output_root + "/Third Party Libraries/SoundBits/Mac/Computer FX", thumbnails_version_number)

##############################################################
# Drag and Slide bundle
##############################################################
bundle_library_list=["SoundBits/Drag and Slide/Crate on Concrete",
                     "SoundBits/Drag and Slide/Heavy Object Forest",
                     "SoundBits/Drag and Slide/Iron on Carpet",
                     "SoundBits/Drag and Slide/Metal Bucket on Concrete",
                     "SoundBits/Drag and Slide/Metal Can on Concrete",
                     "SoundBits/Drag and Slide/Metal on Stone",
                     "SoundBits/Drag and Slide/Object on Gravel",
                     "SoundBits/Drag and Slide/Paper Bag on Table",
                     "SoundBits/Drag and Slide/Plastic Stool on Tile",
                     "SoundBits/Drag and Slide/Shoe on Carpet",
                     "SoundBits/Drag and Slide/Stone on Path",
                     "SoundBits/Drag and Slide/Wood on Carpet",
                     "SoundBits/Drag and Slide/Wood on Gravel",
                     "SoundBits/Drag and Slide/Wood on Parquet"]
bun_inst.create_bundle("Drag and Slide Bundle", bundle_library_list, output_root + "/Third Party Libraries/SoundBits/Mac/Drag and Slide", thumbnails_version_number)

##############################################################
# Electric Typewriters bundle
##############################################################
bundle_library_list=["SoundBits/Electric Typewriters/Erika 3006",
                     "SoundBits/Electric Typewriters/Olivetti Lettera E501",
                     "SoundBits/Electric Typewriters/Panasonic R300",
                     "SoundBits/Electric Typewriters/Sharp PA4000",
                     "SoundBits/Electric Typewriters/Soundbits Typewriters",
                     "SoundBits/Electric Typewriters/TriumphAdler Gabriele100",]
#bun_inst.create_bundle("Electric Typewriters Bundle", bundle_library_list, output_root + "/Third Party Libraries/SoundBits/Mac/Electric Typewriters", thumbnails_version_number)


##############################################################
# Electro Mechanics bundle
##############################################################
bundle_library_list=["SoundBits/Electro Mechanics/Buzzsaw",
                     "SoundBits/Electro Mechanics/CD Drive",
                     "SoundBits/Electro Mechanics/Document Shredder",
                     "SoundBits/Electro Mechanics/Electric Drill",
                     "SoundBits/Electro Mechanics/Electric Screwdriver",
                     "SoundBits/Electro Mechanics/Electric Toy Car Motor",
                     "SoundBits/Electro Mechanics/Hand Milling Machine",
                     "SoundBits/Electro Mechanics/Large Angle Grinder",
                     "SoundBits/Electro Mechanics/Lifting Cart",
                     "SoundBits/Electro Mechanics/Office Hydraulics",
                     "SoundBits/Electro Mechanics/Old Electric Wood Plane",
                     "SoundBits/Electro Mechanics/Power Mirror",
                     "SoundBits/Electro Mechanics/Printer",
                     "SoundBits/Electro Mechanics/Sewing Machine",
                     "SoundBits/Electro Mechanics/Wind Up Toy"]
#bun_inst.create_bundle("Electro Mechanics Bundle", bundle_library_list, output_root + "/Third Party Libraries/SoundBits/Mac/Electro Mechanics", thumbnails_version_number)


##############################################################
# Handwriting bundle
##############################################################
bundle_library_list=["SoundBits/Handwriting/Ballpoint Pen",
                     "SoundBits/Handwriting/Chalk",
                     "SoundBits/Handwriting/Felt Pen",
                     "SoundBits/Handwriting/Fountain Pen",
                     "SoundBits/Handwriting/Markers",
                     "SoundBits/Handwriting/Pencil",
                     "SoundBits/Handwriting/Quill",
                     "SoundBits/Handwriting/Sand",
                     "SoundBits/Handwriting/Spray Can",
                     "SoundBits/Handwriting/Stone",
                     "SoundBits/Handwriting/Text Marker",
                     "SoundBits/Handwriting/Watercolour on Canvas",
                     "SoundBits/Handwriting/Watercolour on Paper",
                     "SoundBits/Handwriting/Wax Crayon",
                     "SoundBits/Handwriting/Whiteboard"]
#bun_inst.create_bundle("Handwriting Bundle", bundle_library_list, output_root + "/Third Party Libraries/SoundBits/Mac/Handwriting", thumbnails_version_number)

##############################################################
# Cinematic Metal bundle
##############################################################
bundle_library_list=["BOOM/Cinematic Metal/Bars",
                     "BOOM/Cinematic Metal/Bell",
                     "BOOM/Cinematic Metal/Bicycle",
                     "BOOM/Cinematic Metal/Container",
                     "BOOM/Cinematic Metal/Crate",
                     "BOOM/Cinematic Metal/Door",
                     "BOOM/Cinematic Metal/Dumpster",
                     "BOOM/Cinematic Metal/Hatch",
                     "BOOM/Cinematic Metal/Jetty",
                     "BOOM/Cinematic Metal/Plate",
                     "BOOM/Cinematic Metal/Scratch",
                     "BOOM/Cinematic Metal/Sheet Crunch",
                     "BOOM/Cinematic Metal/Sheet Hollow",
                     "BOOM/Cinematic Metal/Trash Box",
                     "BOOM/Cinematic Metal/Trash Massive"]
#bun_inst.create_bundle("Cinematic Metal Bundle", bundle_library_list, output_root + "/Third Party Libraries/BOOM/Mac/Cinematic Metal", thumbnails_version_number)

##############################################################
# Destruction bundle
##############################################################
bundle_library_list=["BOOM/Destruction/Glass Smash",
                     "BOOM/Destruction/Glass Stress",
                     "BOOM/Destruction/Metal",
                     "BOOM/Destruction/Metal Creak",
                     "BOOM/Destruction/Metal Impact",
                     "BOOM/Destruction/Plastic",
                     "BOOM/Destruction/Plastic Crack",
                     "BOOM/Destruction/Plastic Creak",
                     "BOOM/Destruction/Rock",
                     "BOOM/Destruction/Rock Creak",
                     "BOOM/Destruction/Rock Impact",
                     "BOOM/Destruction/Wood Break",
                     "BOOM/Destruction/Wood Creak",
                     "BOOM/Destruction/Wood Impact"]
#bun_inst.create_bundle("Destruction Bundle", bundle_library_list, output_root + "/Third Party Libraries/BOOM/Mac/Destruction", thumbnails_version_number)


##############################################################
# Dogs bundle
##############################################################
bundle_library_list=["Coll Anderson/Dog/Pit Bull",
                     "Coll Anderson/Dog/Terrier Female Growl",
                     "Coll Anderson/Dog/Terrier Female Snarl",
                     "Coll Anderson/Dog/Texas Big Face Growl",
                     "Coll Anderson/Dog/Texas Big Face Whimper",
                     "Coll Anderson/Dog/Wolford Growls"]
#bun_inst.create_bundle("Dogs Bundle", bundle_library_list, output_root + "/Third Party Libraries/Coll Anderson/Mac/Dog", thumbnails_version_number)


