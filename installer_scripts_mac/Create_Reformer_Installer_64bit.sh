#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Reformer installer for OS X 
#
#-----------------------------------------------------------------------

#Exit on error
set -e

# Create the example root folder for this product
INSTALL_ROOT="/tmp/root"
PACKAGE_PATH="/tmp/packages"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Reformer"
INPUT_PATH="/Users/$USER/Documents/REFORMER BUILDS"
# Package install destination paths
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer/Documentation"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Presets/main"
REFORMER_LIBRARY_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries"
VOICESAMPLES_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer/VoiceSamples"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
CONVERTED_SAMPLES_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos"
THUMBNAILS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Library/Thumbnails"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Reformer"
ERROR_LOG_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer/Logs"
SETTINGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Reformer"

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH"
cp -pR "$INPUT_PATH/Documentation/" "$DOCUMENT_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Documentation --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$INPUT_PATH/Presets/main/" "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Presets --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Reformer Libraries package
echo "Creating Reformer Libraries package..."
mkdir -p "$REFORMER_LIBRARY_PATH"
cp -pR "/Library/Application Support/Krotos/Reformer/Library/Reformer Libraries/Black Leopard" "$REFORMER_LIBRARY_PATH/Black Leopard"
chmod -R 777 "$REFORMER_LIBRARY_PATH/Black Leopard"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Libraries --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.Libraries.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Voice Samples package
#echo "Creating Voice Samples package..."
#mkdir -p "$VOICESAMPLES_PATH"
#cp -pR "/Users/$USER/Documents/DEHUMANISER BUILDS/PLUGIN/VoiceSamples/" "$VOICESAMPLES_PATH"
#pkgbuild --root "$INSTALL_ROOT" --identifier  com.Krotos.Reformer.VoiceSamples --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.VoiceSamples.pkg"
#rm -rf "$INSTALL_ROOT"
#echo ""

# Create VST package
echo "Creating VST package..."
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Reformer.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/VST_package"  --identifier com.Krotos.Reformer.VstPlugin --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.VstPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AAX package
echo "Creating AAX package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Reformer.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AAX_package" --identifier com.Krotos.Reformer.AaxPlugin --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.AaxPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AU package 
echo "Creating AU package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Reformer.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AU_package" --identifier com.Krotos.Reformer.AuPlugin --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.AuPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Converted Samples packge
echo "Creating Converted Samples package..."
mkdir -p "$CONVERTED_SAMPLES_PATH"
cp -pR "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Black Leopard" "$CONVERTED_SAMPLES_PATH/Black Leopard"
chmod -R 777 "$CONVERTED_SAMPLES_PATH/Black Leopard"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.ConvertedSamples --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.ConvertedSamples.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Thumbnails package
echo "Creating Thumbails package..."
mkdir -p "$THUMBNAILS_PATH"
cp -pR "$INPUT_PATH/Images/Thumbnails/" "$THUMBNAILS_PATH"
chmod -R 777 "$THUMBNAILS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.Thumbnails --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.Thumbnails.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Settings package
echo "Creating Settings package..."
mkdir -p "$SETTINGS_PATH"
cp -pR "$INPUT_PATH/settings.dat" "$SETTINGS_PATH"
chmod -R 777 "$SETTINGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/settings_package" --identifier com.Krotos.Reformer.Settings --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.Settings.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Reformer Uninstaller.app" "$UNINSTALLER_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/reformer_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/reformer_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/reformer_uninstaller.plist --identifier com.Krotos.Reformer.UninstallReformer --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.UninstallReformer.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Error Log package
#echo "Creating Error Log package..."
#mkdir -p "$ERROR_LOG_PATH"
#cp -pR "/Users/$USER/Documents/REFORMER BUILDS/Error Log/CP_ErrorLog.log" "$ERROR_LOG_PATH"
#pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Reformer.ErrorLog --version 1.0 "$PACKAGE_PATH/com.Krotos.Reformer.ErrorLog.pkg"
#rm -rf "$INSTALL_ROOT"
#echo ""

# Put the packages together
# todo: Add the following line when the error log functionality is ready
# --package com.Krotos.Reformer.ErrorLog.pkg
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.VstPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.AaxPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.AuPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Libraries.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.ConvertedSamples.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Thumbnails.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.Settings.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Reformer.UninstallReformer.pkg \
    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>Reformer</title>    \
\ \   <license file="/Users/'$USER'/Documents/REFORMER BUILDS/Documentation/EULA/Reformer License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="/Users/'$USER'/Documents/REFORMER BUILDS/Images/Reformer Installer.png" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" hostArchitectures="x86_64"/>\
	' "$PACKAGE_PATH"/distribution.dist

# Finish up
# Use the following command to test signature
#spctl --assess --verbose --type install Reformer.pkg
mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/Reformer 64.pkg" --version 1.0 --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/Reformer 64.pkg"

# Clean up
rm -rf "$PACKAGE_PATH"/com.Krotos.reformer*.pkg
rm -rf "$PACKAGE_PATH"/*.plist
rm -rf "$PACKAGE_PATH"/distribution.dist
