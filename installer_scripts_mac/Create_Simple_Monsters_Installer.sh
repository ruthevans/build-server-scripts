#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Simple Monsters installer for OS X 
#
#   Usage: ./Create_Simple_Monsters_Installer.sh <arch> <optional:type>
#     e.g. ./Create_Simple_Monsters_Installer.sh 64
#     e.g. ./Create_Simple_Monsters_Installer.sh 64 Demo
#-----------------------------------------------------------------------

#Exit on error
set -e
VERSION_NUMBER="1.1.2"
ARCH=$1
TYPE=`echo "$2" | awk '{print toupper($0)}'` #Make $2 uppercase

if [[ "$ARCH" == "32" ]]; then
	HOSTARCH="i386"
elif [[ "$ARCH" == "64" ]]; then
	HOSTARCH="x86_64"
else 
	echo "Arch not valid."
	echo "Usage: ./Create_Simple_Monsters_Installer.sh <arch>  <optional:type>"
	echo "e.g.   ./Create_Simple_Monsters_Installer.sh 64"
	echo "e.g.   ./Create_Simple_Monsters_Installer.sh 32 demo"
	exit 1
fi

# Create the example root folder for this product
INSTALL_ROOT="/tmp/smroot$RANDOM"
PACKAGE_PATH="/tmp/smpackages$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Simple Monsters"
INPUT_PATH="/Users/$USER/Documents/SIMPLE MONSTERS BUILDS"
# Package install destination paths
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser Simple Monsters/Documentation"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser Simple Monsters/Library"
SOUND_LIBRARY_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser Simple Monsters/Library"
TOUCH_OSC_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser Simple Monsters/Touch OSC"
VOICE_SAMPLES_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser Simple Monsters"
LOGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser Simple Monsters"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
SETTINGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser Simple Monsters"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser Simple Monsters"

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH/EULA"
mkdir -p "$DOCUMENT_PATH/Manual"
if [[ "$TYPE" == "DEMO" ]]; then
  cp -pR "$INPUT_PATH/Documentation_Demo/EULA/Simple Monsters EULA."* "$DOCUMENT_PATH/EULA"
  cp -pR "$INPUT_PATH/Documentation_Demo/Manual/Simple Monsters User Manual."* "$DOCUMENT_PATH/Manual"
else
  cp -pR "$INPUT_PATH/Documentation/EULA/Simple Monsters EULA."* "$DOCUMENT_PATH/EULA"
  cp -pR "$INPUT_PATH/Documentation/Manual/Simple Monsters User Manual."* "$DOCUMENT_PATH/Manual"
fi
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleMonsters.Documentation --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$INPUT_PATH/Presets" "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleMonsters.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Sound Library package
echo "Creating Sound Library package..."
mkdir -p "$SOUND_LIBRARY_PATH"
cp -pR "$INPUT_PATH/Sound Library" "$SOUND_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleMonsters.SoundLibrary --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.SoundLibrary.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Touch OSC package
echo "Creating Touch OSC package..."
mkdir -p "$TOUCH_OSC_PATH"
cp -pR "$INPUT_PATH/Touch OSC/SimpleMonsters_TouchOSC.touchosc" "$TOUCH_OSC_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleMonsters.TouchOSC --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.TouchOSC.pkg"
rm -rf "$INSTALL_ROOT"
echo ""


# Create Voice Samples package
echo "Creating Voice Samples package..."
mkdir -p "$VOICE_SAMPLES_PATH"
cp -pR "$INPUT_PATH/VoiceSamples" "$VOICE_SAMPLES_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleMonsters.VoiceSamples --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.VoiceSamples.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create VST package
echo "making $VST_PATH" 
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/DehumaniserSimpleMonsters.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/VST_package"  --identifier com.Krotos.SimpleMonsters.VstPlugin --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.VstPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AAX package
echo "Creating AAX package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/DehumaniserSimpleMonsters.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AAX_package" --identifier com.Krotos.SimpleMonsters.AaxPlugin --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.AaxPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create AU package 
echo "Creating AU package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/DehumaniserSimpleMonsters.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/AU_package" --identifier com.Krotos.SimpleMonsters.AuPlugin --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.AuPlugin.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Log package
# Permissions and users set in postinstall script
echo "Creating Log package..."
mkdir -p "$LOGS_PATH"
cp -pR "$INPUT_PATH/Logs" "$LOGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/Logs_package" --identifier com.Krotos.SimpleMonsters.Log --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.Log.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Settings package
echo "Creating Settings package..."
mkdir -p "$SETTINGS_PATH"
cp -pR "$INPUT_PATH/settings.dat" "$SETTINGS_PATH"
chmod -R 777 "$SETTINGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/settings_package" --identifier com.Krotos.SimpleMonsters.Settings --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.Settings.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Uninstall Simple Monsters.app" "$UNINSTALLER_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH/Simple Monsters_uninstaller.plist"
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH/Simple Monsters_uninstaller.plist"
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH/Simple Monsters_uninstaller.plist" --identifier com.Krotos.SimpleMonsters.Uninstaller --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleMonsters.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Put the packages together
# todo: Add the following line when the error log functionality is ready
# --package com.Krotos.Simple Monsters.ErrorLog.pkg

productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.VstPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.AaxPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.AuPlugin.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.SoundLibrary.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.TouchOSC.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.VoiceSamples.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.Log.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleMonsters.Settings.pkg \
    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
if [[ "$TYPE" == "DEMO" ]]; then
	INSTALLER_TITLE="Simple Monsters Demo ($ARCH-bit)"
	OUTPUT_PKG="Simple Monsters Demo $ARCH.pkg"
	BACKGROUND_IMG="$INPUT_PATH/Images/Installer Window Simple Monsters - Demo.png"
else
	INSTALLER_TITLE="Simple Monsters ($ARCH-bit)"
	OUTPUT_PKG="Simple Monsters $ARCH.pkg"
	BACKGROUND_IMG="$INPUT_PATH/Images/Installer Window Simple Monsters.png"
fi

sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>'"$INSTALLER_TITLE"'</title>    \
\ \   <license file="/Users/'$USER'/Documents/SIMPLE MONSTERS BUILDS/Documentation/EULA/Simple Monsters License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$BACKGROUND_IMG"'" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" />\
	' "$PACKAGE_PATH"/distribution.dist

# Finish up
# Use the following command to test signature
#spctl --assess --verbose --type install Simple Monsters.pkg
mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/$OUTPUT_PKG" --version 1.0 --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/$OUTPUT_PKG"

# Clean up
rm -rf "$PACKAGE_PATH"
