#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library installers
#
#   Usage: python generate_library_installers.py /path/to/sound/libs /path/to/output/dir provider
#          e.g. python generate_library_installers.py "/Users/krotos/Dropbox (Krotos)/Development/reformer/reformer Libraries/Krotos Reformer Libraries" /tmp/krotos_sound_libs Krotos
###############################################################
import os
import shutil
import argparse
import installer_definitions_new as inst

parser = argparse.ArgumentParser()
parser.add_argument("lib_path", help="Path to the sound libraries")
parser.add_argument("installer_out_path", help="Output path for the generated installers")
parser.add_argument("bundle_name", help="Name of bundle e.g. krotos")
args = parser.parse_args()

bundle_dir    = args.bundle_name
soundlib_path = args.lib_path
finished_installer_dir = args.installer_out_path


###############################################################
#   check_library_is_valid copies files to root_install
#
def check_library_is_valid(name):
    if os.path.exists(os.path.join(inst.database_dir, name, "Clustering")):
        return True
    else:
        print "check_library_is_valid warning: " + name + " is not valid"
        return False


###############################################################
#   create_main_package copies files to root_install
#
def create_main_package(name, bundle_subdir):
    print "create_main_package called"
    if not check_library_is_valid(name): return False
    provider=inst.get_provider(bundle_subdir)
    database_path=os.path.join(inst.target_database_dir, name)    
    
    print "provider is: " + provider
    # License
    if not os.path.exists(inst.target_license_dir):os.makedirs(inst.target_license_dir)
    shutil.copyfile(inst.license_path, inst.target_license_dir + "/Krotos Sound Library EULA.pdf")

    # Database text file
    database_file=os.path.join(inst.database_dir, name, "versionNumber.txt")
    print "data file:" + database_file
    if not os.path.exists(database_path):os.makedirs(database_path )
    shutil.copyfile(database_file, os.path.join(database_path, "versionNumber.txt"))
    os.system("chmod -R 777 \"" + database_path + "\"")
    
    inst.run_package_command(inst.create_package_name(name), "1.0")
    return True

def generate_postinstall_script(name, bundle_subdir):
    script_path = os.path.join(inst.scripts_dir, "postinstall")
    if not os.path.exists (inst.scripts_dir):os.makedirs(inst.scripts_dir)
    path_suffix=os.path.join("Reformer Sample Libraries", bundle_subdir, name)
    new_script = open(script_path, "w")
    
    new_script.write("#!/bin/bash\n" +
    "lib_location=$2\n" +
    "mkdir -p \"" + os.path.join(inst.database_dir, name) + "\"\n" +
    "printf \"%s%s\" " + " \"$lib_location\" \"/" + path_suffix + "\" > \"" + os.path.join(inst.database_dir, name, "NewSamplesPath.txt") + "\"\n" +
    "chmod 666 \"" + os.path.join(inst.database_dir, name, "NewSamplesPath.txt") + "\"\n")
    
    new_script.close()
    
    os.system("chmod +x \"" + script_path + "\"")

    
    
def add_metadata(name, provider):
    # Metadata
    metadata_path=os.path.join( "/Applications/Krotos/Reformer/Reformer Sample Libraries",provider, "Metadata")
    target_metadata_path=os.path.join(inst.target_soundlib_dir, provider, "Metadata")
    
    library_metadata_found=False
    #this is the case for krotos metadata
    if os.path.exists(metadata_path):
        print "add_metadata: Checking for metadata at " + metadata_path + "/" + provider + " " + name
        for filename in os.listdir(metadata_path):
            if filename.startswith(provider + " " + name):
                library_metadata_found=True
                if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
        #this is the case for soundbits and boom metadata
        if not library_metadata_found:
            if args.bundle_name.find('/') !=-1:just_bundle_name=args.bundle_name.rsplit('/',1)[1]
            else: just_bundle_name=""
            print "add_metadata: Checking for metadata at " + metadata_path
            for filename in os.listdir(metadata_path):
                 if filename.startswith(provider + " " + just_bundle_name):
                    if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                    library_metadata_found=True
                    shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
    if not library_metadata_found:
        print "Warning: No metadata found for " + name
    else:
        print "Found metadata:" + os.path.join(target_metadata_path,filename)


###############################################################
#   create_sound_library_package create a package with sound samples
#
def create_sound_library_package(name, bundle_subdir):
# Sound libraries
    print "create_sound_lib_package called"
    samples_path=os.path.join(soundlib_path, name)
    if not os.path.exists(os.path.join(inst.target_soundlib_dir, bundle_subdir)):os.makedirs(os.path.join(inst.target_soundlib_dir, bundle_subdir))
    shutil.copytree(samples_path, os.path.join(inst.target_soundlib_dir, bundle_subdir, name))
    os.system("chmod -R 777 \"" + os.path.join(inst.target_soundlib_dir, bundle_subdir, name, "\""))
    
    add_metadata(name, inst.get_provider(bundle_subdir))
    
    generate_postinstall_script(name, bundle_subdir)
    inst.run_package_command_with_scripts(inst.create_package_name( name + ".soundlib" ), "1.0")


###############################################################
#   Removes install directory for when the package is built
#
def clean_package_dir():
    if os.path.isdir(inst.install_root): shutil.rmtree(inst.install_root)   
    if os.path.isdir(inst.packages_dir): shutil.rmtree(inst.packages_dir)
    if os.path.isdir(inst.scripts_dir):  shutil.rmtree(inst.scripts_dir)

###############################################################
#   Checks the existence of the directories given
#
def check_arguments():
	if not os.path.isdir(args.lib_path):
		print "Error: Could not find sound library directory " + args.lib_path 
	if not os.path.isdir(args.installer_out_path):
		os.makedirs(args.installer_out_path)
	print("about to make " + args.lib_path)
	if not os.path.isdir(inst.packages_dir):
		os.makedirs(inst.packages_dir)


###############################################################
#   Main starts here
#
check_arguments()
# Create an installer for every folder in soundlib_path
clean_package_dir()
for filename in os.listdir(soundlib_path):
    if os.path.isdir(soundlib_path + "/" + filename): 
        print "checking" +    soundlib_path + "/" + filename 
        if create_main_package(filename, bundle_dir):
            print "Building installer for " + filename 
            create_sound_library_package(filename, bundle_dir)
            inst.create_sound_rate_packages(filename)
            inst.copy_thumbnail_package("v1.2.0")
            inst.run_productbuild_command(filename, finished_installer_dir, "v1.2.0")
            clean_package_dir()


