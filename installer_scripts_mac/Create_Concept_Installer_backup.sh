#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a concept plugin installer
#
#  Usage: ./Create_Concept_Installer
#
#-----------------------------------------------------------------------

#Exit on error
set -e
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Create the example root folder for this product
INSTALL_ROOT="/tmp/synrootbb$RANDOM"
PACKAGE_PATH="/tmp/synpackagesbb$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Synth Installers/Synth"
BUNDLE_OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Concept"
INPUT_PATH="/Users/$USER/Documents/CONCEPT BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Synth"


# Package install destination paths
APP_SUPPORT_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Concept"
PRESETS_PATH="$APP_SUPPORT_PATH/Presets/main"
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Concept/Documentation"
DAW_PROJECTS_PATH="$INSTALL_ROOT/Applications/Krotos/Concept/Demo Projects"
IRS_PATH="$APP_SUPPORT_PATH/IRs"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
VST3_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST3"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Concept"

source $SCRIPT_DIR/setVERSION_NUMBER.sh "$INPUT_PATH/Plugins/Concept.vst"
echo "version number:$VERSION_NUMBER"

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH/Manual"
mkdir -p "$DOCUMENT_PATH/EULA"
mkdir -p "$IRS_PATH"
mkdir -p "$DAW_PROJECTS_PATH"
chmod -R 777 "$APP_SUPPORT_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Manual/Release/Concept Manual.pdf"  "$DOCUMENT_PATH/Manual/"
cp -pR "$DROPBOX_INPUT_PATH/Documents/EULA/Concept License.pdf"                  "$DOCUMENT_PATH/EULA/"
cp -pR "$DROPBOX_INPUT_PATH/IR Files/v1.0 IRs/"*                                          "$IRS_PATH/"
cp -pR "$DROPBOX_INPUT_PATH/Projects/v1.0 Projects/"* "$DAW_PROJECTS_PATH/"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Concept.Documentation --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Concept.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create presets and friendly names package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/v1.0 Presets/"*   "$PRESETS_PATH"
chmod 777 "$APP_SUPPORT_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/plugins_package" --identifier com.Krotos.Concept.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Concept.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create plugins package
echo "Creating vst package..."
mkdir -p "$VST_PATH"
mkdir -p "$AU_PATH"
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Concept.vst"       "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Concept.vst3"      "$VST3_PATH"
cp -pR "$INPUT_PATH/Plugins/Concept.component" "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Concept.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/plugins_package" --identifier com.Krotos.Concept.Plugins --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Concept.Plugins.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Uninstall Concept.app" "$UNINSTALLER_PATH/"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/concept_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/concept_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/concept_uninstaller.plist --identifier com.Krotos.Concept.Uninstaller --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Concept.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

########################################################
# Build the installer
########################################################
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Concept.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Concept.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Concept.Plugins.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Concept.Uninstaller.pkg \
     "$PACKAGE_PATH"/distribution.dist

sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>Concept Installer</title>    \
\ \   <license file="'"$DROPBOX_INPUT_PATH"'/Documents/EULA/Concept License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="topleft" file="'"$INPUT_PATH"'/Images/Concept Installer for OS X - 03.jpg" scaling="none"/>\
	' "$PACKAGE_PATH"/distribution.dist

# /Users/krotos/Documents/CONCEPT BUILDS/Images

mkdir -p "$BUNDLE_OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/Concept Installer.pkg"  --version "$VERSION_NUMBER" --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/Concept Installer.pkg"

# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "$INPUT_PATH/Plugins/*"
