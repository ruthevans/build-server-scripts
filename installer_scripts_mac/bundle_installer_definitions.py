#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: unknown
###############################################################
import os
import shutil
import installer_definitions_new as inst

###############################################################
#   check_library_is_valid copies files to root_install
#
def check_library_is_valid(name):
    if os.path.exists(os.path.join(inst.database_dir, name, "Clustering")):
        return True
    else:
        print "check_library_is_valid warning: " + name + " is not valid"
        return False

###############################################################
#   add_to_main_package copies files to root_install
#
#   This only contains the documents and databases text file
def add_to_main_package(name):
    if not check_library_is_valid(name): return 
    database_path=os.path.join(inst.target_database_dir, name)    

    # License
    if not os.path.exists(inst.target_license_dir):os.makedirs(inst.target_license_dir)
    shutil.copyfile(inst.license_path, inst.target_license_dir + "/Krotos Sound Library EULA.pdf")

    # Database text file
    database_file=os.path.join(inst.database_dir, name, "versionNumber.txt")
    if os.path.isfile(database_file):
        print "database file:" + database_file
        if not os.path.exists(database_path):os.makedirs(database_path )
        shutil.copyfile(database_file, os.path.join(database_path, "versionNumber.txt"))
        os.system("chmod -R 777 \"" + inst.target_database_dir + "\"")
  

##############################################################
#   generate_postinstall_script creates a script named
#   postinstall in script_dir that when run will copy the
#   users chosen install directory into NewSampelsPath.txt
def generate_postinstall_script(lib_list):
    postinstall_text = "#!/bin/bash\nlib_location=$2\n"
    
    for library_path in lib_list:
        name = inst.get_lib_name(library_path)
        bundle_subdir = inst.get_provider(library_path)
        path_suffix=os.path.join("Reformer Sample Libraries", bundle_subdir, name)
        postinstall_text += "mkdir -p \"" + os.path.join(inst.database_dir, name) + "\"\n"
        postinstall_text += "printf \"%s%s\" " + " \"$lib_location\" \"/" + path_suffix + "\" > \"" + os.path.join(inst.database_dir, name, "NewSamplesPath.txt") + "\"\n"
        postinstall_text += "chmod 666 \"" + os.path.join(inst.database_dir, name, "NewSamplesPath.txt") + "\"\n\n"

    
    script_path = os.path.join(inst.scripts_dir, "postinstall")
    if not os.path.exists (inst.scripts_dir):os.makedirs(inst.scripts_dir)
    
    new_script = open(script_path, "w")
    new_script.write(postinstall_text)
    new_script.close()
    
    os.system("chmod +x \"" + script_path + "\"")

    
###############################################################
#   add_to_sound_library_package create a package with sound samples
#
def add_to_sound_library_package(lib_path):
# Sound libraries
    bundle_subdir=inst.get_provider(lib_path)
    name = inst.get_lib_name(lib_path)    
    samples_path=os.path.join(inst.samples_dir, lib_path)
    if not os.path.exists(os.path.join(inst.target_soundlib_dir, bundle_subdir)):os.makedirs(os.path.join(inst.target_soundlib_dir, bundle_subdir))
    if not os.path.exists(samples_path):
        print "bundle_installer_defs.add_to_sound_library: Cannot find " + samples_path
        quit(1)
    else:
        shutil.copytree(samples_path, os.path.join(inst.target_soundlib_dir, bundle_subdir, name))
        os.system("chmod -R 777 \"" + os.path.join(inst.target_soundlib_dir, bundle_subdir, name, "\""))
    #add_metadata(name, bundle_subdir)
    add_additional_sample_files(bundle_subdir)

#This version of the above function is for the new jenkins form installers
def add_to_sound_library_package_auto(lib_path, lib_root):
# Sample files
    name = inst.get_lib_name(lib_path)
    samples_path=os.path.join(lib_path, "Samples")
    target_samples_path = os.path.join(inst.install_root, "Reformer Sample Libraries", lib_root)
    
    if not os.path.exists(samples_path):
        print "bundle_installer_defs.add_to_sound_library: Cannot find " + samples_path
        quit(1)
    else:
        if not os.path.exists(target_samples_path):os.makedirs(target_samples_path)
        shutil.copytree(samples_path, os.path.join(target_samples_path, name))
        os.system("chmod -R 777 \"" + os.path.join(target_samples_path, name, "\""))
    #add_metadata(name, bundle_subdir)
    #add_additional_sample_files(bundle_subdir)


def add_additional_sample_files(bundle_subdir):
    # don't reinstall the packages additional files everytime a library is added.
    additional_files_path=os.path.join(inst.additional_samples, bundle_subdir)
    if not os.path.exists(inst.target_additional_samples_path) and os.path.exists(additional_files_path):
        print "Adding additional samples from " + additional_files_path
        shutil.copytree (additional_files_path, inst.target_additional_samples_path)
        os.system("chmod -R 777 \"" + inst.target_additional_samples_path + "\"")

###############################################################
#   add_metadata finds and adds metadata files to the install root
#
#   Looks in the sound sample path/Metadata for files named after the
#   package or bundle e.g.:
#   Krotos Eletronic Metadata.xml
#   Or
#   Soundbits Computer FX List.pdf
def add_metadata(name, bundle_subdir):
    # Metadata
    print "Looking for metadata..."
    provider = inst.get_provider(bundle_subdir)
    metadata_path=os.path.join( "/Applications/Krotos/Reformer/Reformer Sample Libraries",provider, "Metadata")
    target_metadata_path=os.path.join(inst.target_soundlib_dir, provider, "Metadata")
    
    library_metadata_found=False
    #this is the case for krotos metadata
    if os.path.exists(metadata_path):
        for filename in os.listdir(metadata_path):
            if filename.lower().startswith(provider.lower() + " " + name.lower()):
                library_metadata_found=True
                if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                print "copying metadata from " + os.path.join(metadata_path,filename)
                shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
        #this is the case for soundbits and boom metadata
        if not library_metadata_found:
            if bundle_subdir.find('/') !=-1:just_bundle_name=bundle_subdir.rsplit('/',1)[1]
            else: just_bundle_name=""
            for filename in os.listdir(metadata_path):
                if filename.lower().startswith(provider.lower() + " " + just_bundle_name.lower()):
                    library_metadata_found=True
                    if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                    print "copying metadata from " + os.path.join(metadata_path,filename)
                    shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
    if not library_metadata_found:
         print "Warning: No metadata found for " + name

def add_metdata_simple(lib_path, lib_root):
    target_metadata_path=os.path.join(inst.install_root, "Reformer Sample Libraries", lib_root, "Metadata")
    metadata_path = os.path.join(lib_path, "../Metadata")
    print "Seeking metadata in:" + metadata_path
    if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
    for filename in os.listdir(metadata_path):
        shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
    

def add_to_sound_rate_package(lib_path, sample_rate):
    name=inst.get_lib_name(lib_path)
    database_sound_rate_path=os.path.join(inst.database_dir, name, "Clustering", sample_rate)
    target_database_sound_rate_path=os.path.join(inst.target_database_dir, name, "Clustering", sample_rate)
    shutil.copytree(database_sound_rate_path, target_database_sound_rate_path)
    os.system("chmod -R 777 \"" + inst.target_database_dir + "\"")

def add_to_sound_rate_package_auto(database_path, sample_rate):
    library_name=os.path.basename(database_path)
    database_path = os.path.join(database_path, "Databases")
    database_sound_rate_path=os.path.join(database_path, "Clustering", sample_rate)
    target_database_sound_rate_path=os.path.join(inst.target_database_dir, library_name, "Clustering", sample_rate)
    shutil.copytree(database_sound_rate_path, target_database_sound_rate_path)
    os.system("chmod -R 777 \"" + inst.target_database_dir + "\"")

def create_main_package(name, lib_list):
    for library_path in lib_list:
        add_to_main_package(inst.get_lib_name(library_path))
    inst.run_package_command(inst.create_package_name( name + ".main"), "1.0")

def create_main_package_auto(name):
    if not os.path.exists(inst.target_license_dir):os.makedirs(inst.target_license_dir)
    shutil.copyfile(inst.license_path, inst.target_license_dir + "/Krotos Sound Library EULA.pdf")
    inst.run_package_command(inst.create_package_name( name + ".main"), "1.0")

def create_sound_library_package(name, lib_list):
    for library_path in lib_list:
        add_to_sound_library_package(library_path)
    generate_postinstall_script(lib_list)
    inst.run_package_command_with_scripts(inst.create_package_name( name + ".soundlib" ), "1.0")
    

def create_sound_library_package_auto(name, lib_list, lib_root):
    for library_path in lib_list:
        library_name=os.path.basename(library_path)
        databases_path=os.path.join(library_path, "Databases")
        add_to_sound_library_package_auto( library_path, lib_root)
    
    add_metdata_simple(library_path, lib_root)
    generate_postinstall_script(lib_list)
    inst.run_package_command_with_scripts(inst.create_package_name( name + ".soundlib" ), "1.0")

def create_sound_rate_packages(name, lib_list):
    #44100
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "44100")
    inst.run_package_command(inst.create_package_name( name + ".rate.0.44100" ), "1.0")
    #48000
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "48000")
    inst.run_package_command(inst.create_package_name( name + ".rate.1.48000" ), "1.0")
    #88200
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "88200")
    inst.run_package_command(inst.create_package_name( name + ".rate.2.88200" ), "1.0")
    #96000
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "96000")
    inst.run_package_command(inst.create_package_name( name + ".rate.3.96000" ), "1.0")
    #176400
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "176400")
    inst.run_package_command(inst.create_package_name( name + ".rate.4.176400" ), "1.0")
    #192000
    for library_path in lib_list:
        add_to_sound_rate_package(library_path, "192000")
    inst.run_package_command(inst.create_package_name( name + ".rate.5.192000" ), "1.0")

def create_sound_rate_packages_auto(name, lib_list, lib_root):
    #44100
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "44100")
    inst.run_package_command(inst.create_package_name( name + ".rate.0.44100" ), "1.0")
    #48000
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "48000")
    inst.run_package_command(inst.create_package_name( name + ".rate.1.48000" ), "1.0")
    #88200
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "88200")
    inst.run_package_command(inst.create_package_name( name + ".rate.2.88200" ), "1.0")
    #96000
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "96000")
    inst.run_package_command(inst.create_package_name( name + ".rate.3.96000" ), "1.0")
    #176400
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "176400")
    inst.run_package_command(inst.create_package_name( name + ".rate.4.176400" ), "1.0")
    #192000
    for library_path in lib_list:
        add_to_sound_rate_package_auto(library_path, "192000")
    inst.run_package_command(inst.create_package_name( name + ".rate.5.192000" ), "1.0")

def create_bundle(name, lib_list, output_dir, version):
    # Create the main package
    create_main_package(name, lib_list)
    create_sound_library_package(name, lib_list)
    create_sound_rate_packages(name, lib_list)
    if not "none" == version:
        inst.copy_thumbnail_package(version)
    inst.run_productbuild_command(name, output_dir, version)
    inst.clean_all_dirs()
    
#bun_inst.create_bundle_auto(installer_name, library_root, libraries, output_path)
def create_bundle_auto(name, lib_root, lib_list, output_dir):
    # Create the main package
    create_main_package_auto(name)
    create_sound_library_package_auto(name, lib_list, lib_root)
    create_sound_rate_packages_auto(name, lib_list, lib_root)
    inst.run_productbuild_command(name, output_dir, "1.0")
    inst.clean_all_dirs()
 

