#!/usr/bin/env python
###############################################################
#
#   This script creates bundle installers
#
#   Usage:   generate_bundle_installer.py <mode(add/create)> <add: path to sound libraries. create: path for installers to be made> <bundle name>
#            e.g. python generate_bundle_installer.py add    "/Applications/Krotos/Reformer/Reformer Sample Libraries/Krotos/Fruit and Veg" "Krotos/Fruit and Veg"
#                 python generate_bundle_installer.py create "$OUTPUT_DIR/Krotos Reformer Libraries/Mac/Fruit and Veg"  "Fruit and Veg"
#
################################################################

import argparse
import os
import shutil
import installer_definitions as inst

parser = argparse.ArgumentParser()
parser.add_argument("mode", help="'add' or 'create'")
parser.add_argument("path", help="if add: Path to the sound libraries\nif create: Path to installer output directory")
parser.add_argument("bundle_name", help="Name of bundle e.g. krotos")
args = parser.parse_args()

distribution_file= inst.packages_dir + "/distribution.dist"

#path is either installer_out_path depending on mode
installer_out_path=args.path
soundlib_path=args.path

###############################################################
#   Checks the arguments passed in
#
def check_arguments():
	print ("mode is: " + args.mode)
	if (args.mode == "add"):
		if not os.path.isdir(soundlib_path):
			print "Error: Could not find sound library directory " + soundlib_path 
			quit(1) 
		if not os.path.isdir(inst.packages_dir):
			os.makedirs(inst.packages_dir)
	elif (args.mode == "create"):
	    if not os.path.isdir(installer_out_path): 
		    os.makedirs(installer_out_path)
	else:
	    print("mode is invalid")
	    quit(2)

###############################################################
#   Removes install directory for when the package is built
#
def clean_package_dir():
    if os.path.isdir(inst.install_root): shutil.rmtree(inst.install_root)   
    if os.path.isdir(inst.packages_dir): shutil.rmtree(inst.packages_dir) 

###############################################################
#   prepare_package copies files to root_install
#
def prepare_package(name, bundle_subdir):
    samples_path=soundlib_path + "/" + name
    database_path=inst.database_dir + "/" + name
    provider=args.bundle_name.rsplit('/',1)[0]
        
    # License
    if not os.path.exists(inst.target_license_dir):os.makedirs(inst.target_license_dir)
    shutil.copyfile(inst.license_path, inst.target_license_dir + "/Krotos Sound Library EULA.pdf")
    # Databases
    if not os.path.exists(inst.target_database_dir):os.makedirs(inst.target_database_dir )
    if not os.path.exists(database_path + "/Clustering"):
    	#note If this doesn't exists when we're looking at a bundle's subdir and should skip.
    	print("could not find " + database_path + "/Clustering")
    	return False
    else:
    	shutil.copytree(database_path,inst.target_database_dir + "/" + name )
    	os.system("chmod -R 777 \"" + inst.target_database_dir + "/" + name + "\"")
    # Sound libraries
    if not os.path.exists(inst.target_soundlib_dir + "/" + bundle_subdir):os.makedirs(inst.target_soundlib_dir + "/" + bundle_subdir )
    shutil.copytree(samples_path, inst.target_soundlib_dir + "/" + bundle_subdir + "/" + name)
    os.system("chmod -R 777 \"" + inst.target_soundlib_dir + "/" + bundle_subdir + "/" + name + "\"")  
    
    # Metadata
    metadata_path=os.path.join( "/Applications/Krotos/Reformer/Reformer Sample Libraries", provider, "Metadata")
    target_metadata_path=os.path.join(inst.target_soundlib_dir, provider, "Metadata")
    
    library_metadata_found=False
    #this is the case for krotos metadata
    for filename in os.listdir(metadata_path):
        if filename.startswith(provider + " " + name):
    	    library_metadata_found=True
            if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
            shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
    #this is the case for soundbits and boom metadata
    if not library_metadata_found:
        if args.bundle_name.find('/') !=-1:just_bundle_name=args.bundle_name.rsplit('/',1)[1]
        else: just_bundle_name=""           
        for filename in os.listdir(metadata_path):
             if filename.startswith(provider + " " + just_bundle_name):
                if not os.path.exists(target_metadata_path): os.makedirs(target_metadata_path)
                shutil.copyfile(os.path.join(metadata_path,filename), os.path.join(target_metadata_path,filename) )
     
    return True

###############################################################
#   run_package_command creates the pkgbuild command
#
def run_package_command( lib_name, package_version):
    package_id=inst.create_package_name(lib_name);
    distribution_file= inst.packages_dir + "/distribution.dist"
    if not os.path.isdir(inst.install_root): os.makedirs(inst.install_root)
    if not os.path.isdir(inst.packages_dir): os.makedirs(inst.packages_dir)
    # Create package
    os.system("pkgbuild --root \"" + inst.install_root + "\"" + 
                      " --identifier " + package_id + 
                      " --version " + package_version + 
                      " \"" + inst.packages_dir + "/" + package_id + ".pkg\"")


###############################################################
#   create_bundle_installer creates the bundle installer pkg
#
def create_bundle_installer(bundle_name):
    # generate distribution.dist file
    productbuild_command="productbuild --synthesize "
    for filename in os.listdir(inst.packages_dir):
        if filename.lower().endswith('.pkg'):productbuild_command = productbuild_command + " --package \"" + os.path.join(inst.packages_dir,filename) + "\""
    productbuild_command = productbuild_command + " \"" + distribution_file + "\""
    print(productbuild_command)
    os.system(productbuild_command)
    inst.edit_distribution(distribution_file, bundle_name)
    # Create installer
    if not os.path.isdir(installer_out_path): os.makedirs(installer_out_path)
    os.system("productbuild --distribution \"" + distribution_file + "\""
                          " --package-path \"" + inst.packages_dir + "\" \"" + installer_out_path + "/" + bundle_name + " Bundle Sound Library Installer.pkg\"" + 
                          " --sign \"Developer ID Installer: Krotos LTD (3HX6KKGDLH)\"")
    print "Created " + installer_out_path + "/" + bundle_name + " Bundle Sound Library Installer.pkg\""

###############################################################
#   Main starts here
#
check_arguments()
if (args.mode == "add"):
	# Create an installer for every folder in soundlib_path
	for filename in os.listdir(soundlib_path):
   		if os.path.isdir(soundlib_path + "/" + filename):     
			if prepare_package(filename, args.bundle_name):
				print "Generating package for " + filename   
				run_package_command(filename, "1.0")
				if os.path.isdir(inst.install_root): shutil.rmtree(inst.install_root)  
elif (args.mode == "create"):
	create_bundle_installer(args.bundle_name)
#	clean_package_dir()


