#!/usr/bin/env python
###############################################################
#
#   This script generates reformer sound library bundle installers
#
#   Usage: python bundles_v1.1.0.py <installer output dir>
#     e.g. python bundles_v1.1.0.py "/Volumes/TOSHIBA EXT/V1.1.0 Reformer libraries Mac"
###############################################################
import os
import shutil
import argparse
import bundle_installer_definitions as bun_inst

parser = argparse.ArgumentParser()
parser.add_argument("installer_out_path", help="Output path for the generated installers")
args = parser.parse_args()

output_root = args.installer_out_path

thumbnails_version_number = "v1.1.5"
bundle_library_list=["Krotos/Clothes Foley/Backpack Foley",
                     "Krotos/Clothes Foley/Bag Straps Foley",
                     "Krotos/Clothes Foley/Bag Velcro Foley",
                     "Krotos/Clothes Foley/Bag Zips Foley",
                     "Krotos/Clothes Foley/Canvas Bag Foley",
                     "Krotos/Clothes Foley/Jacket Foley",
                     "Krotos/Clothes Foley/Jeans Foley",
                     "Krotos/Clothes Foley/Laptop Bag Foley",
                     "Krotos/Clothes Foley/Large Fabric Bag Foley",
                     "Krotos/Clothes Foley/Leather Bag Foley",
                     "Krotos/Clothes Foley/Luggage Holdall Foley",
                     "Krotos/Clothes Foley/Paper Bag Foley",
                     "Krotos/Clothes Foley/Plastic Bag Foley",
                     "Krotos/Clothes Foley/Plate Armour",
                     "Krotos/Clothes Foley/Satchel Bag Foley",
                     "Krotos/Clothes Foley/Waterproofs Foley"]
bun_inst.create_bundle_dont_build("Clothes and Materials", bundle_library_list, output_root + "/clothesAndMat", thumbnails_version_number)
