#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Weaponiser Player installer for OS X 
#
#  Usage: Create_Weaponiser_Installer_or_Bundle.sh <version>(optional)
#         version could be basic, fullyloaded, all. No version creates a normal installer
#
# e.g.: ./Create_Weaponiser_Installer_or_Bundle.sh fullyloaded
#-----------------------------------------------------------------------

#Exit on error
set -e
VERSION_NUMBER=1.2.1
DATABASE_VERSION=1.2.1
BUNDLE="$1"

if [[ "basic" == "$BUNDLE" ]]; then
	echo "Creating basic Weaponiser installer."
	BUILD_BASIC="on"
elif [[ "fullyloaded" == "$BUNDLE" ]]; then
	echo "Creating fully loaded Weaponiser installer"
	BUILD_FULLYLOADED="on"
elif [[ "all" == "$BUNDLE" ]]; then
	echo "Creating normal/demo, basic and fully loaded installer"
	BUILD_DEMO="on"
	BUILD_BASIC="on"
	BUILD_FULLYLOADED="on"
else
	echo "Creating normal/demo Weaponiser installer"
	BUILD_DEMO="on"
fi


# Create the example root folder for this product
INSTALL_ROOT="/tmp/wpnroot$RANDOM"
PACKAGE_PATH="/tmp/wpnpackages$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Weaponiser"
BUNDLE_OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Weaponiser Bundles"
INPUT_PATH="/Users/$USER/Documents/WEAPONISER BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Weaponiser/WP Relaunch"
# Package install destination paths
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Weaponiser/Documentation"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Library/Presets/main"
F_ASSETS_PATH="$INSTALL_ROOT/Weaponiser Factory Assets" # rest of path is set in script
TOOLS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Tools"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
DATABASE_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Databases/"
SETTINGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Weaponiser"
ERROR_LOG_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Logs"
	

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH/Manual"
mkdir -p "$DOCUMENT_PATH/EULA"
cp -pR "$DROPBOX_INPUT_PATH/../Documents/Manual/Manual Release/Weaponiser Manual.pdf" "$DOCUMENT_PATH/Manual/Weaponiser Manual.pdf"
cp -pR "$DROPBOX_INPUT_PATH/../Documents/EULA/License/Weaponiser License.pdf" "$DOCUMENT_PATH/EULA/Weaponiser License.pdf"
if [[ "${BUILD_FULLYLOADED}" ]]; then
	cp -pR "$DROPBOX_INPUT_PATH/WP Fully Loaded/"*.pdf "$DOCUMENT_PATH"
elif [[ "${BUILD_BASIC}" ]]; then
	cp -pR "$DROPBOX_INPUT_PATH/WP Basic/"*.pdf "$DOCUMENT_PATH"
fi
#Include the CustomPluginDataTool here because it has to go somewhere, and it cannot be in the factory assets package.
mkdir -p "$TOOLS_PATH"
cp -pR "$INPUT_PATH/Tools/CustomPluginDataTool" "$TOOLS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.Documentation --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""


########################################################
# presets
########################################################
# Create Presets package ~Demo~
if [[ "${BUILD_DEMO}" ]]; then
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Demo/Presets/OSX"/*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/presets_package" --identifier com.Krotos.Weaponiser.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create Presets package ~Basic~
if [[ "${BUILD_BASIC}" ]]; then
echo "Creating Presets Basic package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Basic/Presets/OSX"/*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/presets_package" --identifier com.Krotos.Weaponiser.Basic.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Basic.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create Presets package ~Fully Loaded~
if [[ "${BUILD_FULLYLOADED}" ]]; then
# Create Presets package
echo "Creating Presets Fully Loaded package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Fully Loaded/Presets/OSX"/*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/presets_package" --identifier com.Krotos.Weaponiser.Total.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

########################################################
# demo/basic plugins
########################################################
# Create vst package
if [[ "${BUILD_DEMO}" ]] || [[ "$BUILD_BASIC" ]]; then
echo "Creating vst package..."
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins64/Weaponiser.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/OS_VST_package" --identifier com.Krotos.Weaponiser.vst --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.vst.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create au package 
echo "Creating au package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins64/Weaponiser.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/OS_AU_package" --identifier com.Krotos.Weaponiser.au --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.au.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create aax package 
echo "Creating aax package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins64/Weaponiser.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/OS_AAX_package" --identifier com.Krotos.Weaponiser.aax --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.aax.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi 

########################################################
# Fully Loaded plugins
########################################################
# Create vst package ~Fully Loaded~
if [[ "${BUILD_FULLYLOADED}" ]]; then
echo "Creating Fully Loaded vst package..."
mkdir -p "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins64_fullyloaded/Weaponiser.vst" "$VST_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/OS_VST_package" --identifier com.Krotos.Weaponiser.Total.vst --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.vst.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create au package ~Fully Loaded~
echo "Creating Fully Loaded au package..."
mkdir -p "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins64_fullyloaded/Weaponiser.component" "$AU_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/OS_AU_package" --identifier com.Krotos.Weaponiser.Total.au --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.au.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create aax package ~Fully Loaded~
echo "Creating Fully Loaded aax package..."
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins64_fullyloaded/Weaponiser.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT"  --scripts "$INPUT_PATH/Scripts/OS_AAX_package" --identifier com.Krotos.Weaponiser.Total.aax --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.aax.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi 

########################################################

# Create factory assets package ~Demo~
if [[ "${BUILD_DEMO}" ]]; then
echo "Creating Factory Assets package..."
mkdir -p "$F_ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Demo/Samples/"* "$F_ASSETS_PATH"
chmod -R 777 "$F_ASSETS_PATH"
find "$INSTALL_ROOT" -name *.repeaks\* -delete
pkgbuild --root "$INSTALL_ROOT"  --identifier com.Krotos.Weaponiser.FactoryAssets --scripts "$INPUT_PATH/Scripts/factory_assets_package/demo" --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.FactoryAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create factory assets package ~Basic~
if [[ "${BUILD_BASIC}" ]]; then
echo "Creating Factory Assets Basic package..."
mkdir -p "$F_ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Basic/Samples/"* "$F_ASSETS_PATH"
chmod -R 777 "$F_ASSETS_PATH"
find "$INSTALL_ROOT" -name *.repeaks\* -delete
pkgbuild --root "$INSTALL_ROOT"  --identifier com.Krotos.Weaponiser.Basic.FactoryAssets --scripts "$INPUT_PATH/Scripts/factory_assets_package/basic" --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Basic.FactoryAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create factory assets package ~Fully Loaded~
if [[ "${BUILD_FULLYLOADED}" ]]; then
echo "Creating Factory Assets Fully Loaded package..."
mkdir -p "$F_ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/WP Fully Loaded/Samples/"* "$F_ASSETS_PATH"
chmod -R 777 "$F_ASSETS_PATH"
find "$INSTALL_ROOT" -name *.repeaks\* -delete
pkgbuild --root "$INSTALL_ROOT"  --identifier com.Krotos.Weaponiser.Total.FactoryAssets --scripts "$INPUT_PATH/Scripts/factory_assets_package/fullyloaded" --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.FactoryAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create database package
echo "Creating Database package..."
mkdir -p "$DATABASE_PATH"
cp -pR "$DROPBOX_INPUT_PATH/../Databases/WP DB/filetags.db" "$DATABASE_PATH"
chmod -R 777 "$DATABASE_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.Databases --version $DATABASE_VERSION "$PACKAGE_PATH/com.Krotos.Weaponiser.Databases.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create settings 
echo "Creating settings package..."
mkdir -p "$SETTINGS_PATH"
cp "$INPUT_PATH/settings.dat" "$SETTINGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/settings_package" --identifier com.Krotos.Weaponiser.Settings --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Settings.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Uninstall Weaponiser.app" "$UNINSTALLER_PATH/"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/weaponiser_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/weaponiser_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/weaponiser_uninstaller.plist --identifier com.Krotos.Weaponiser.Uninstaller --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
#echo ""

# Create analytics package
echo "Creating Analytics package..."
pkgbuild --nopayload --scripts "$INPUT_PATH/Scripts/analytics_package" --identifier com.Krotos.Weaponiser.Analytics --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.Weaponiser.Analytics.pkg"
echo ""

function edit_dist_file {
# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>'"$INSTALLER_TITLE"'</title>\
\ \   <license file="/Users/krotos/Dropbox (Krotos)/Development/Weaponiser/Documents/EULA/License/Weaponiser License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$DROPBOX_INPUT_PATH"'/../Installer art/Weaponiser Installer for OS X 623x349_compatibility_export.png" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" hostArchitectures="x86_64"/>\
	' "$PACKAGE_PATH"/distribution.dist
	
# xml editing required for customisable factory assets path
if [[ "${BUILD_BASIC}" ]]; then
	PACKAGE_NAME=com.Krotos.Weaponiser.Basic.FactoryAssets
elif [[ "${BUILD_FULLYLOADED}" ]]; then
	PACKAGE_NAME=com.Krotos.Weaponiser.Total.FactoryAssets
else
	PACKAGE_NAME=com.Krotos.Weaponiser.FactoryAssets
fi
sed -i "" 's/<options customize="never"/<options customize="always"/g' "$PACKAGE_PATH"/distribution.dist
sed -i "" 's/<line choice="'$PACKAGE_NAME'"\/>//g' "$PACKAGE_PATH"/distribution.dist
sed -i "" 's/<\/choices-outline>/<line choice="factoryassets"><line choice="'$PACKAGE_NAME'"\/><\/line><\/choices-outline>/g' "$PACKAGE_PATH"/distribution.dist
sed -i "" 's/<choice id="default"\/>/<choice id="default" visible="false"\/><choice id="factoryassets" title="Factory Assets" description="Choose the location of the audio files for Weaponiser." customLocation="\/Library\/Application Support\/Krotos\/Weaponiser\/Library\/Factory Assets" customLocationAllowAlternateVolumes="yes"\/>/g' "$PACKAGE_PATH"/distribution.dist
sed -i "" 's/<pkg-ref id="'$PACKAGE_NAME'"\/>/<pkg-ref id="'$PACKAGE_NAME'" visible="false"\/>/g' "$PACKAGE_PATH"/distribution.dist
cp "$PACKAGE_PATH"/distribution.dist /tmp/distribution.dist.txt
}

########################################################
# Build the normal/demo installer
########################################################
if [[ "${BUILD_DEMO}" ]]; then
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.vst.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.au.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.aax.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.FactoryAssets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Databases.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Settings.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Analytics.pkg \
    "$PACKAGE_PATH"/distribution.dist
INSTALLER_TITLE="Weaponiser"
edit_dist_file
# Finish up
# Use the following command to test signaturef
#spctl --assess --verbose --type install Reformer.pkg
mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --plugins "$INPUT_PATH/InstallerPanePlugins" --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/Weaponiser Demo.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/Weaponiser Demo.pkg"
fi #end demo build#

########################################################
# Build the basic installer
########################################################
if [[ "${BUILD_BASIC}" ]]; then
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Basic.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Basic.FactoryAssets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.vst.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.au.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.aax.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Databases.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Settings.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Uninstaller.pkg \
    "$PACKAGE_PATH"/distribution.dist
INSTALLER_TITLE="Weaponiser Basic"
edit_dist_file
# Finish up
mkdir -p "$BUNDLE_OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --plugins "$INPUT_PATH/InstallerPanePlugins" --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/Weaponiser Basic.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/Weaponiser Basic.pkg"
fi #end basic build#

########################################################
# Build the fully loaded installer
########################################################
if [[ "${BUILD_FULLYLOADED}" ]]; then
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.FactoryAssets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.vst.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.au.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.aax.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Databases.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Settings.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Uninstaller.pkg \
    "$PACKAGE_PATH"/distribution.dist
INSTALLER_TITLE="Weaponiser Fully Loaded"
edit_dist_file

# Finish up
mkdir -p "$BUNDLE_OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --plugins "$INPUT_PATH/InstallerPanePlugins" --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/Weaponiser Fully Loaded.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/Weaponiser Fully Loaded.pkg"
fi #end fully loaded build#


# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "$INPUT_PATH/Plugins/*"
