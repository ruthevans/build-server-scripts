#!/bin/bash
#-----------------------------------------------------------------------
#
#    This script sets the offline tool icon at the given app path
#
#    Usage: SetAnalysisToolIcon.sh '~/Documents/REFORMER PRO BUILDS/Binaries64/Analysis Tool.app'"
#
#-----------------------------------------------------------------------

#   To generate the icon rsrc file do the following:
#   - Create a new folder
#   - Right click and select GetInfo
#   - Drag the icon image to the folder icon
#   - Run the following command in a terminal (where 'tmpfolder' is the folder made in the first step)
#     DeRez -only icns tmpfolder/$'Icon\r' > AnalysisToolIcon.rsrc

binary="$1"
iconPath="/Users/krotos/Documents/REFORMER PRO BUILDS/Images/AnalysisToolIcon.rsrc"

if [ -z "$binary" ]; then
    echo "nope. please provide the path to the program"
    echo "e.g.: SetAnalysisToolIcon.sh '~/Documents/REFORMER PRO BUILDS/Binaries64/Analysis Tool.app'"
    exit 1
fi

if [ ! -d "$binary" ]; then
    echo "Either $binary is a file or it does not exist"
    echo "Maybe you forgot the hidden .app?"
    exit 2
fi

Rez -append "$iconPath" -o "$binary"/$'Icon\r'
SetFile -a C "$binary"