#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Magical assets installer
#
#  Usage: Create_Weaponiser_Magic.sh
#
#-----------------------------------------------------------------------

#Exit on error
set -e
VERSION_NUMBER=1.0
INSTALLER_TITLE="Weaponiser Magic Bundle"
# No spaces in bundle ID please
BUNDLE_ID="Magic" 

# Create the example root folder for this product
INSTALL_ROOT="/tmp/wpnroot$RANDOM"
PACKAGE_PATH="/tmp/wpnpackages$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Weaponiser"
BUNDLE_OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Weaponiser Bundles"
INPUT_PATH="/Users/$USER/Documents/WEAPONISER BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Weaponiser"

# Package install destination paths
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Weaponiser/Documentation"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Library/Presets/main"
F_ASSETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Library/Factory Assets/"
DATABASE_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Databases/"

mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Metadata/Krotos Weaponiser Magic Metadata.pdf" "$DOCUMENT_PATH/Krotos Weaponiser Magic Metadata.pdf"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.$BUNDLE_ID.Documentation --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.$BUNDLE_ID.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Add-on Packs/Magic/Presets/OSX/"*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.$BUNDLE_ID.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.$BUNDLE_ID.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create factory assets package
echo "Creating Factory Assets package..."
mkdir -p "$F_ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Add-on Packs/Magic/Samples/"* "$F_ASSETS_PATH"
chmod -R 777 "$F_ASSETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.$BUNDLE_ID.FactoryAssets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.$BUNDLE_ID.FactoryAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create database package
echo "Creating Database package..."
mkdir -p "$DATABASE_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Databases/WP DB/filetags.db"* "$DATABASE_PATH"
chmod -R 777 "$DATABASE_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.$BUNDLE_ID.Databases --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.$BUNDLE_ID.Databases.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.$BUNDLE_ID.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.$BUNDLE_ID.FactoryAssets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.$BUNDLE_ID.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.$BUNDLE_ID.Databases.pkg \
    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>'"$INSTALLER_TITLE"'</title>\
\ \   <license file="/Users/krotos/Dropbox (Krotos)/Development/Weaponiser/Documents/EULA/License/Weaponiser License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$DROPBOX_INPUT_PATH"'/Installer art/Weaponiser Installer for OS X 623x349_compatibility_export.png" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" hostArchitectures="x86_64"/>\
	' "$PACKAGE_PATH"/distribution.dist
	
# Finish up
mkdir -p "$BUNDLE_OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/$INSTALLER_TITLE.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/$INSTALLER_TITLE.pkg"