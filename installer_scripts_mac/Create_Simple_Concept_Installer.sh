#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a concept plugin installer
#
#  Usage: ./Create_Simple_Concept_Installer.sh <version>(optional)
#	       version could be: simple, create
#          If no version is given then simple will be built by default.
#
# e.g. ./Create_Simple_Concept_Installer simple
#-----------------------------------------------------------------------


if [[ "create" == "$1" ]]; then
	echo "Creating simple concept create installer."
	BUILD_SCC="on"
else
	echo "Creating simple concept installer"
	BUILD_SC="on"
fi

#Exit on error
#set -e
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Create the example root folder for this product
INSTALL_ROOT="/tmp/cptrootbb$RANDOM"
PACKAGE_PATH="/tmp/cptpackagesbb$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Synth Installers/Synth"
BUNDLE_OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers"
INPUT_PATH="/Users/$USER/Documents/SIMPLE CONCEPT BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Synth"

# Package install destination paths
SC_APP_SUPPORT_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Simple Concept"
SCC_APP_SUPPORT_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Simple Concept Create"
SC_PRESETS_PATH="$SC_APP_SUPPORT_PATH/Presets/main"
SC_IRS_PATH="$SC_APP_SUPPORT_PATH/IRs"
SCC_PRESETS_PATH="$SCC_APP_SUPPORT_PATH/Presets/main"
SCC_IRS_PATH="$SCC_APP_SUPPORT_PATH/IRs"
SC_DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Simple Concept/Documentation"
SCC_DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Simple Concept Create/Documentation"
DAW_PROJECTS_PATH="$INSTALL_ROOT/Applications/Krotos/Concept/Demo Projects"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
VST3_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST3"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Simple Concept"

source $SCRIPT_DIR/setVERSION_NUMBER.sh "$INPUT_PATH/Plugins/Simple Concept.vst"
echo "version number:$VERSION_NUMBER"

mkdir -p "$PACKAGE_PATH"


# Create Document package
echo "Creating Document package..."
mkdir -p "$SC_IRS_PATH"
mkdir -p "$SC_DOCUMENT_PATH/Manual"
mkdir -p "$SC_DOCUMENT_PATH/EULA"
chmod -R 777 "$SC_APP_SUPPORT_PATH"
cp -pR "$DROPBOX_INPUT_PATH/IR Files/v1.0 IRs/"*    "$SC_IRS_PATH/"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Simple Concept Documentation/Manual/Simple Concept Manual.pdf" "$SC_DOCUMENT_PATH/Manual/"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Simple Concept Documentation/EULA/Simple Concept License.pdf" "$SC_DOCUMENT_PATH/EULA/"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleConcept.Documentation --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.SimpleConcept.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

if [[ "${BUILD_SCC}" ]]; then
echo "Creating SCC Document package..."
mkdir -p "$SCC_IRS_PATH"
mkdir -p "$SCC_DOCUMENT_PATH/Manual"
mkdir -p "$SCC_DOCUMENT_PATH/EULA"
chmod -R 777 "$SCC_APP_SUPPORT_PATH"
cp -pR "$DROPBOX_INPUT_PATH/IR Files/v1.0 IRs/"*    "$SCC_IRS_PATH/"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Simple Concept Documentation/Manual/Simple Concept Manual.pdf" "$SCC_DOCUMENT_PATH/Manual/"
cp -pR "$DROPBOX_INPUT_PATH/Documents/Simple Concept Documentation/EULA/Simple Concept License.pdf" "$SCC_DOCUMENT_PATH/EULA/"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.SimpleConceptCreate.Documentation --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.SimpleConceptCreate.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create plugins package
echo "Creating simple concept plugin package..."
mkdir -p "$VST_PATH"
mkdir -p "$VST3_PATH"
mkdir -p "$AU_PATH"
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept.vst"       "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept.vst3"      "$VST3_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept.component" "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/concept_plugins.plist
plutil -replace BundleIsVersionChecked -bool false "$PACKAGE_PATH"/concept_plugins.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/concept_plugins.plist --scripts "$INPUT_PATH/Scripts/simple_plugins_package" --identifier com.Krotos.SimpleConcept.Plugins --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleConcept.Plugins.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

if [[ "${BUILD_SCC}" ]]; then
echo "Creating simple concept create plugin package..."
mkdir -p "$VST_PATH"
mkdir -p "$VST3_PATH"
mkdir -p "$AU_PATH"
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept Create.vst"       "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept Create.vst3"      "$VST3_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept Create.component" "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Simple Concept Create.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/concept_plugins.plist
plutil -replace BundleIsVersionChecked -bool false "$PACKAGE_PATH"/concept_plugins.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/concept_plugins.plist --scripts "$INPUT_PATH/Scripts/create_plugins_package" --identifier com.Krotos.SimpleConceptCreate.Plugins --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleConceptCreate.Plugins.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi

# Create presets and friendly names package for SC
echo "Creating Presets package..."
mkdir -p "$SC_PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/Simple Concept Release/"*   "$SC_PRESETS_PATH"
chmod 777 "$SC_APP_SUPPORT_PATH"
chmod -R 777 "$SC_PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/presets_package" --identifier com.Krotos.SimpleConcept.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleConcept.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

if [[ "${BUILD_SCC}" ]]; then
# Create presets and friendly names package for SSC
echo "Creating SCC Presets package..."
mkdir -p "$SCC_PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/Simple Concept Beta/"*   "$SCC_PRESETS_PATH"
chmod 777 "$SCC_APP_SUPPORT_PATH"
chmod -R 777 "$SCC_PRESETS_PATH"
#--scripts "$INPUT_PATH/Scripts/presets_package"
pkgbuild --root "$INSTALL_ROOT"  --identifier com.Krotos.SimpleConceptCreate.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleConceptCreate.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""
fi


# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Uninstall Simple Concept.app" "$UNINSTALLER_PATH/"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/simpleconcept_uninstaller.plist
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH"/simpleconcept_uninstaller.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/simpleconcept_uninstaller.plist --identifier com.Krotos.SimpleConcept.Uninstaller --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.SimpleConcept.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Empty package for the analytics functionality.
pkgbuild  --nopayload --scripts "$INPUT_PATH/Scripts/analytics_package" --identifier com.Krotos.SimpleConcept.Analytics --version "$VERSION_NUMBER" "$PACKAGE_PATH/com.Krotos.SimpleConcept.Analytics.pkg"




if [[ "${BUILD_SCC}" ]]; then
########################################################
# Build the SCC installer
########################################################
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConceptCreate.Plugins.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConceptCreate.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConceptCreate.Documentation.pkg \
     "$PACKAGE_PATH"/distribution.dist

sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>Simple Concept Create Installer</title>    \
\ \   <license file="'"$DROPBOX_INPUT_PATH"'/Documents/Simple Concept Documentation/EULA/Simple Concept License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="topleft" file="'"$INPUT_PATH"'/Images/Simple Concept Installer for OS X.jpg" scaling="fill"/>\
	' "$PACKAGE_PATH"/distribution.dist


mkdir -p "$BUNDLE_OUTPUT_PATH/Simple Concept Create"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/Simple Concept Create/Simple Concept Create Installer.pkg"  --version "$VERSION_NUMBER" --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/Simple Concept Create/Simple Concept Create Installer.pkg"
fi

########################################################
# Build the SC installer
########################################################
if [[ "${BUILD_SC}" ]]; then
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConcept.Plugins.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConcept.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConcept.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConcept.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.SimpleConcept.Analytics.pkg \
     "$PACKAGE_PATH"/distribution.dist

sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>Simple Concept Installer</title>    \
\ \   <license file="'"$DROPBOX_INPUT_PATH"'/Documents/Simple Concept Documentation/EULA/Simple Concept License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="topleft" file="'"$INPUT_PATH"'/Images/Simple Concept Installer for OS X.jpg" scaling="fill"/>\
	' "$PACKAGE_PATH"/distribution.dist


mkdir -p "$BUNDLE_OUTPUT_PATH/Simple Concept"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --plugins "$INPUT_PATH/Installer Pane Plugin" --package-path "$PACKAGE_PATH" "$BUNDLE_OUTPUT_PATH/Simple Concept/Simple Concept Installer.pkg"  --version "$VERSION_NUMBER" --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $BUNDLE_OUTPUT_PATH/Simple Concept/Simple Concept Installer.pkg"
fi


# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "$INPUT_PATH/Plugins/*"

