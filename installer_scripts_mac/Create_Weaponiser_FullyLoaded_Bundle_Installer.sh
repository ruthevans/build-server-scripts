#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Total assets installer for Weaponiser
#
#-----------------------------------------------------------------------

#Exit on error
set -e
VERSION_NUMBER=0.2

# Create the example root folder for this product
INSTALL_ROOT="/tmp/wpnbasicroot$RANDOM"
PACKAGE_PATH="/tmp/wpnbasicpackages$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Weaponiser Assets"
INPUT_PATH="/Users/$USER/Documents/WEAPONISER BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Weaponiser"
F_ASSETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Library/Factory Assets"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Weaponiser/Library/Presets/main"

mkdir -p "$PACKAGE_PATH"

# Create factory assets package
echo "Creating Factory Assets package..."
mkdir -p "$F_ASSETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Factory Assets/Factory Assets Total Bundle 1.0.0/"* "$F_ASSETS_PATH"
chmod -R 777 "$F_ASSETS_PATH"
find "$INSTALL_ROOT" -name *.repeaks\* -delete
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.Total.FactoryAssets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.FactoryAssets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Presets/1.0.0 Presets/OS X/Total Bundle/"*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.Weaponiser.Total.Presets --version $VERSION_NUMBER "$PACKAGE_PATH/com.Krotos.Weaponiser.Total.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Put the packages together
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.FactoryAssets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.Weaponiser.Total.Presets.pkg \
    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>Weaponiser Fully Loaded Bundle</title>\
\ \   <license file="/Users/'"$USER"'/Dropbox (Krotos)/Development/Weaponiser/Documents/EULA/License/Weaponiser License.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$DROPBOX_INPUT_PATH"'/Installer art/Weaponiser Installer for OS X 623x349_compatibility_export.png" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" />\
	' "$PACKAGE_PATH"/distribution.dist

# Finish up
# Use the following command to test signaturef
#spctl --assess --verbose --type install Reformer.pkg
mkdir -p "$OUTPUT_PATH"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH/Weaponiser Fully Loaded Bundle.pkg" --version $VERSION_NUMBER --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/Weaponiser Fully Loaded Bundle.pkg"

# Clean up
rm -rf "$PACKAGE_PATH"
rm -rf "$INPUT_PATH/Plugins/*"



