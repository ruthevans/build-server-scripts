#!/bin/bash
#-----------------------------------------------------------------------
#
# This script creates a Dehumaniser II installer for OS X 
#
#   Usage: ./Create_Dehumaniser_Installer.sh <arch> <optional:type>
#     e.g. ./Create_Dehumaniser_Installer.sh 64
#     e.g. ./Create_Dehumaniser_Installer.sh 32 Demo
#-----------------------------------------------------------------------

#Exit on error
set -e

ARCH=$1
TYPE=`echo "$2" | awk '{print toupper($0)}'` #Make $2 uppercase


if [[ "$ARCH" == "32" ]]; then
	HOSTARCH="i386"
elif [[ "$ARCH" == "64" ]]; then
	HOSTARCH="x86_64"
else 
	echo "Arch not valid."
	echo "Usage: ./Create_Dehumaniser_Installer.sh <arch>"
	echo "e.g.   ./Create_Dehumaniser_Installer.sh 64"
	exit 1
fi



# Create the example root folder for this product
INSTALL_ROOT="/tmp/dhroot$RANDOM"
PACKAGE_PATH="/tmp/dhpackages$RANDOM"
OUTPUT_PATH="/Users/$USER/Desktop/Jenkins Installers/Dehumaniser"
INPUT_PATH="/Users/$USER/Documents/DEHUMANISER BUILDS"
DROPBOX_INPUT_PATH="/Users/$USER/Dropbox (Krotos)/Development/Dehumaniser/Dehumaniser VST"
# Package install destination paths
UNINSTALLER_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser/Uninstaller"
DOCUMENT_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser/Documentation"
SOUND_LIBRARY_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser/Library/Sound Library"
VST_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/VST"
AU_PATH="$INSTALL_ROOT/Library/Audio/Plug-Ins/Components"
AAX_PATH="$INSTALL_ROOT/Library/Application Support/Avid/Audio/Plug-Ins"
PRESETS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser/Library/Presets/main"
VOICE_SAMPLES_PATH="$INSTALL_ROOT/Applications/Krotos/Dehumaniser"
SETTINGS_PATH="$INSTALL_ROOT/Library/Application Support/Krotos/Dehumaniser"
mkdir -p "$PACKAGE_PATH"

# Create Document package
echo "Creating Document package..."
mkdir -p "$DOCUMENT_PATH/EULA" "$DOCUMENT_PATH/Manual"
cp -pR "$INPUT_PATH/Documentation/EULA/Dehumaniser II EULA.pdf" "$DOCUMENT_PATH/EULA"
cp -pR "$INPUT_PATH/Documentation/Manual/Dehumaniser II User Manual.pdf" "$DOCUMENT_PATH/Manual"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.DehumaniserII.Documentation --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.Documentation.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Presets package
echo "Creating Presets package..."
mkdir -p "$PRESETS_PATH"
cp -pR "$INPUT_PATH/Presets/main/Angry Beast.preset" "$PRESETS_PATH"
cp -pR "$DROPBOX_INPUT_PATH/PRESETS/1.3.1 Presets/OSX/"*   "$PRESETS_PATH"
chmod -R 777 "$PRESETS_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.DehumaniserII.Presets --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.Presets.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Sound Library package
echo "Creating Sound Library package..."
mkdir -p "$SOUND_LIBRARY_PATH"
cp -pR "$DROPBOX_INPUT_PATH/Sound Library/1.3.1/"* "$SOUND_LIBRARY_PATH"
chmod -R 777 "$SOUND_LIBRARY_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.DehumaniserII.SoundLibrary --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.SoundLibrary.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Voice Samples package
echo "Creating Voice Samples package..."
mkdir -p "$VOICE_SAMPLES_PATH"
cp -pR "$INPUT_PATH/VoiceSamples" "$VOICE_SAMPLES_PATH"
chmod -R 777 "$VOICE_SAMPLES_PATH"
pkgbuild --root "$INSTALL_ROOT" --identifier com.Krotos.DehumaniserII.VoiceSamples --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.VoiceSamples.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create plugins package
echo "Creating plugins package..."
mkdir -p "$VST_PATH"
mkdir -p "$AU_PATH"
mkdir -p "$AAX_PATH"
cp -pR "$INPUT_PATH/Plugins/Dehumaniser.vst"       "$VST_PATH"
cp -pR "$INPUT_PATH/Plugins/Dehumaniser.component" "$AU_PATH"
cp -pR "$INPUT_PATH/Plugins/Dehumaniser.aaxplugin" "$AAX_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH"/dhII_plugins.plist
plutil -replace BundleIsVersionChecked -bool false "$PACKAGE_PATH"/dhII_plugins.plist
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH"/dhII_plugins.plist --scripts "$INPUT_PATH/Scripts/plugins_package" --identifier com.Krotos.DehumaniserII.Plugins --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.Plugins.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create settings 
echo "Creating settings package..."
mkdir -p "$SETTINGS_PATH"
cp "$INPUT_PATH/settings.dat" "$SETTINGS_PATH"
pkgbuild --root "$INSTALL_ROOT" --scripts "$INPUT_PATH/Scripts/settings_package" --identifier com.Krotos.DehumaniserII.Settings --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.Settings.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Create Uninstaller package
# extra steps are needed for pkgbuild to deal with the .app bundle file
echo "Creating Uninstaller package..."
mkdir -p "$UNINSTALLER_PATH"
cp -pR "$INPUT_PATH/Uninstaller/Dehumaniser II uninstaller.app" "$UNINSTALLER_PATH"
pkgbuild --root "$INSTALL_ROOT" --analyze "$PACKAGE_PATH/Uninstaller.plist"
plutil -replace BundleIsRelocatable -bool false "$PACKAGE_PATH/Uninstaller.plist"
pkgbuild --root "$INSTALL_ROOT" --component-plist "$PACKAGE_PATH/Uninstaller.plist" --identifier com.Krotos.DehumaniserII.Uninstaller --version 1.3.1 "$PACKAGE_PATH/com.Krotos.DehumaniserII.Uninstaller.pkg"
rm -rf "$INSTALL_ROOT"
echo ""

# Put the packages together
# todo: Add the following line when the error log functionality is ready
# --package com.Krotos.Simple Monsters.ErrorLog.pkg
productbuild --synthesize \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.Documentation.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.Presets.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.SoundLibrary.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.VoiceSamples.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.Plugins.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.Uninstaller.pkg \
    --package "$PACKAGE_PATH"/com.Krotos.DehumaniserII.settings.pkg \
    "$PACKAGE_PATH"/distribution.dist

# Customise distribution.dist for our software
# For details see the apple website https://developer.apple.com/library/content/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html
if [[ "$TYPE" == "DEMO" ]]; then
	INSTALLER_TITLE="Dehumaniser II Demo ($ARCH-bit)"
	BACKGROUND_IMG="/Users/$USER/Documents/DEHUMANISER BUILDS/Images/Installer Window DH2 Demo.png"
	OUTPUT_PKG="Dehumaniser II Demo $ARCH.pkg"
else
	INSTALLER_TITLE="Dehumaniser II ($ARCH-bit)"
	BACKGROUND_IMG="/Users/$USER/Documents/DEHUMANISER BUILDS/Images/Installer Window DH2.png"
	OUTPUT_PKG="Dehumaniser II $ARCH.pkg"
fi

sed -i '' '3i\
\ \   <!-- Custom xml -->\
\ \   <title>'"$INSTALLER_TITLE"'</title>    \
\ \   <license file="/Users/'$USER'/Documents/DEHUMANISER BUILDS/Documentation/EULA/Dehumaniser II EULA.rtf" mime-type="" uti=""/>\
\ \   <background alignment="center" file="'"$BACKGROUND_IMG"'" scaling="none"/>\
\ \   <domains enable_localSystem="true"/>\
\ \   <options rootVolumeOnly="true" />\
	' "$PACKAGE_PATH"/distribution.dist

# Finish up
# Use the following command to test signature
#spctl --assess --verbose --type install Simple Monsters.pkg
mkdir -p "$OUTPUT_PATH"
echo productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH"/"$OUTPUT_PKG" --version 1.3.1 --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
productbuild --distribution "$PACKAGE_PATH"/distribution.dist --package-path "$PACKAGE_PATH" "$OUTPUT_PATH"/"$OUTPUT_PKG" --version 1.3.1 --sign "Developer ID Installer: Krotos LTD (3HX6KKGDLH)"
echo "Installer created at $OUTPUT_PATH/$OUTPUT_PKG"

# Clean up
rm -rf "$PACKAGE_PATH"
