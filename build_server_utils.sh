#!/bin/bash

#build_server_utils.sh
# useful little functions used across the build server scripts.

#-----------------------------------------------------------------------
# function:  echo_and_run <command>
# 
# purpose: prints a command before running it.
#    e.g.: echo_and_run sudo rm -rf "/Users/krotos/Library/Audio/Plug-Ins/VST/Dehumaniser.vst"
#
function echo_and_run {
  echo "$@" ; "$@" ;
}

#-----------------------------------------------------------------------
# function:  clean_for_windows <script>
# 
# purpose: Removes invisible characters that stops scripts running on windows
#    e.g.: clean_for_windows build_server_utils.sh"
# note:    This only seems to work when run on windows
function clean_for_windows {
  sed -i 's/\r$//' "$1"
}

#-----------------------------------------------------------------------
# function:  convert_to_proper_case <variable>
# 
# purpose: formats the given variable into proper case (lower case with upper case first letter)
#     e.g: PROJECT=`convert_to_proper_case weaponiser`
#
function convert_to_proper_case {
  echo "$1" | awk '{print toupper(substr($0,0,1))tolower(substr($0,2))}'
}

#-----------------------------------------------------------------------
# function:  convert_to_upper_case <variable>
# 
# purpose: formats the given variable into upper case
#     e.g: PROJECT=`convert_to_upper_case weaponiser`
#
function convert_to_upper_case {
    echo "$1" | awk '{print toupper($0)}'
}

#-----------------------------------------------------------------------
# function:  set_system_var 
# 
# purpose: sets $SYSTEM to "Mac" or "Win"
#    e.g.: set_system_var
#
function set_system_var {
unamestr=`uname`
if [[ "$unamestr" == "Darwin" ]]; then
    #Mac build server detected
    SYSTEM="Mac"
elif [[ "$unamestr" == "CYGWIN_NT-10.0" ]]; then
    #Windows build server detected
    SYSTEM="Win"
else
    echo "Warning: Unknown system detected: $unamestr"
    exit 1
fi
}



#-----------------------------------------------------------------------
# function:  set_job_arch_var 
# 
# purpose: sets $JOB_ARCH to "64" or "32" using $JOB_BASE_NAME which is
#          set automatically by jenkins.
#    e.g.: set_job_arch_var
#
function set_job_arch_var {
    if [[ $JOB_BASE_NAME == *"32"* ]]; then
        JOB_ARCH="32"
    elif [[ $JOB_BASE_NAME == *"64"* ]]; then
        JOB_ARCH="64"
    else
        echo "Warning: Cannot find architecture in job name: $JOB_BASE_NAME"
    fi
}


#-----------------------------------------------------------------------
# function:  set_job_project_var 
# 
# purpose: sets $JOB_PROJECT to the project type using $JOB_BASE_NAME which is
#          set automatically by jenkins.
#    e.g.: set_job_project_var
#
function set_job_project_var {
    JOB_PROJECT=`convert_to_upper_case "$JOB_BASE_NAME"`
    if [[ $JOB_PROJECT == *"WEAPONISER"* ]]; then
        JOB_PROJECT="Weaponiser"
    elif [[ $JOB_PROJECT == *"REFORMER PRO"* ]]; then
        JOB_PROJECT="Reformer Pro"
    elif [[ $JOB_PROJECT == *"REFORMER"* ]]; then
        JOB_PROJECT="Reformer"
    elif [[ $JOB_PROJECT == *"SIMPLE MONSTERS"* ]]; then
        JOB_PROJECT="Simple Monsters"
    elif [[ $JOB_PROJECT == *"DEHUMANISER"* ]]; then
        JOB_PROJECT="Dehumaniser"
    elif [[ $JOB_PROJECT == *"ANALYSIS TOOL"*  ]]; then
        JOB_PROJECT="Analysis Tool"
    elif [[ $JOB_PROJECT == *"KROTOSHOST"* ]]; then
    	JOB_PROJECT="KrotosHost"
    elif [[ $JOB_PROJECT == *"IGNITER"* ]]; then
        JOB_PROJECT="Igniter"
    elif [[ $JOB_PROJECT == *"SIMPLE CONCEPT CREATE"* ]]; then
        JOB_PROJECT="Simple Concept Create"
    elif [[ $JOB_PROJECT == *"SIMPLE CONCEPT"* ]]; then
        JOB_PROJECT="Simple Concept"
    elif [[ $JOB_PROJECT == *"CONCEPT"* ]]; then
        JOB_PROJECT="Concept"
    else
        unset JOB_PROJECT
        echo "Warning: Cannot find project in job name: $JOB_BASE_NAME"
	echo "JOB_PROJECT for $JOB_BASE_NAME is $JOB_PROJECT"
    fi
}

#-----------------------------------------------------------------------
# function:  set_job_project_version_var 
# 
# purpose: sets $JOB_PROJECT_VERSION to the project version (Release or Demo)
#          using $JOB_BASE_NAME which is set automatically by jenkins.
#    e.g.: set_job_project_version_var
#
function set_job_project_version_var {
    JOB_PROJECT_VERSION=`convert_to_upper_case "$JOB_BASE_NAME"`
    if [[ $JOB_PROJECT_VERSION == *"DEMO"* ]]; then
        JOB_PROJECT_VERSION="Demo"
    else
        JOB_PROJECT_VERSION="Release"
    fi
}

#-----------------------------------------------------------------------
# function:  set_wraptool_var 
# 
# purpose: sets $WRAPTOOL to the wraptool path based on which OS is used
#    e.g.: set_wraptool_var
#
function set_wraptool_var {
    if [[ -z $SYSTEM ]]; then set_system_var; fi
    
    if [[ $SYSTEM == "Win" ]]; then
        WRAPTOOL="/cygdrive/c/PROGRA~2/PACEAntiPiracy/Eden/Fusion/Versions/5/wraptool.exe"
    else
        WRAPTOOL="/Applications/PACEAntiPiracy/Eden/Fusion/Current/bin/wraptool"
    fi
}

#-----------------------------------------------------------------------
# function:  verify_plugin_is_wrapped 
# 
# purpose:  Checks if the given plugin is wrapped. If calling on windows use a
#           trditional path instead of a /cygdrive one.
# input:  full path to plugin or binary   
# return: 0 for success, plugin is wrapped
#         2 for failure, plugin is not wrapped
#
# e.g.:  verify_plugin "C:\Program Files\Steinberg\VstPlugins\Weaponiser.dll"
function verify_plugin_is_wrapped {
    if [[ -z $WRAPTOOL ]]; then set_wraptool_var; fi
    echo $1
    $WRAPTOOL verify --in "$1"
    exit_code=$?
    
    if [[ $exit_code == 0 ]]; then
        echo "yes that plugin has been wrapped"
    elif [[ $exit_code == 2 ]]; then
        echo "no that plugin has not been wrapped"
    elif [[ $exit_code == 4 ]]; then
        echo "Wraptool attempted to verify a directory. Possibly an aaxplugin was checked."
    else
        echo "unknown error:$exit_code"
    fi
    
    return $exit_code
}



