//
//  MyInstallerPane.m
//  WeaponiserAnalyticsPane
//
//  Created by KrotosMacMini on 21/06/2018.
//  Copyright © 2018 Krotos. All rights reserved.
//

#import "MyInstallerPane.h"

@implementation MyInstallerPane

- (NSString *)title
{
    return [[NSBundle bundleForClass:[self class]] localizedStringForKey:@"PaneTitle" value:nil table:nil];
}
- (BOOL) shouldExitPane:(InstallerSectionDirection) inDirection
{
    if (InstallerDirectionForward == inDirection)
    {
        NSString *analyticsFile = @"/tmp/installer_krotoswpn_analytics.dat";
        
        /* Set up a file manager to create/remove analytics.dat */
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        
        if (checkButton.state==NSOnState)
        {
            
            // Setting up bits and bobs needed for permission setting. 0777 here will grant all permissions 
            NSNumber *permissions;
            permissions = [NSNumber numberWithUnsignedLong:0666];
            NSDictionary *attr;
            attr = [NSDictionary dictionaryWithObject:permissions forKey:NSFilePosixPermissions];
            
            
            /* Create analytics.dat */
            [filemgr createFileAtPath: analyticsFile 
                             contents: nil 
                           attributes: attr];
            
        } else {
            /* Remove analytics.dat - in case the user goes back in the installer to uncheck the box */
            [filemgr removeItemAtPath: analyticsFile 
                                error: NULL];
        }
    }
    return YES;
}

@end
