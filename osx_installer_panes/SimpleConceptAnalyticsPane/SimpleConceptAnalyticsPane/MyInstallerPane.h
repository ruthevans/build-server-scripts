//
//  MyInstallerPane.h
//  SimpleConceptAnalyticsPane
//
//  Created by KrotosMacMini on 23/06/2020.
//  Copyright © 2020 Krotos. All rights reserved.
//

#import <InstallerPlugins/InstallerPlugins.h>

@interface MyInstallerPane : InstallerPane
{
    IBOutlet NSButton *radioButtonAgree;
    IBOutlet NSButton *radioButtonDisagree;
}

@end
