//
//  MyInstallerPane.m
//  SimpleConceptAnalyticsPane
//
//  Created by KrotosMacMini on 23/06/2020.
//  Copyright © 2020 Krotos. All rights reserved.
//

#import "MyInstallerPane.h"

@implementation MyInstallerPane

- (NSString *)title
{
    return [[NSBundle bundleForClass:[self class]] localizedStringForKey:@"PaneTitle" value:nil table:nil];
}

- (void) didEnterPane:(InstallerSectionDirection)inDirection
{
    // Disable the 'Continue' button if neither radio button is selected
    if (radioButtonAgree.state==NSOffState && radioButtonDisagree.state==NSOffState)
    {
        [self setNextEnabled:NO];
    }
}
- (BOOL) shouldExitPane:(InstallerSectionDirection)inDirection
{
    if (InstallerDirectionForward == inDirection)
    {
        NSString *analyticsFile = @"/tmp/installer_krotossimpleconcept_analytics.dat";
        // Set up a file manager to create/remove analyticsFile
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        if (radioButtonAgree.state==NSOnState)
        {
            // Setting up bits and bobs needed for permission setting. 0666 here will grant write/read permissions
            NSNumber *permissions;
            permissions = [NSNumber numberWithUnsignedLong:0666];
            NSDictionary *attr;
            attr = [NSDictionary dictionaryWithObject:permissions forKey:NSFilePosixPermissions];
            // Create analyticsFile
            [filemgr createFileAtPath: analyticsFile
                             contents: nil
                           attributes: attr];
        } else {
            // Remove analyticsFile - in case the user goes back in the installer to uncheck the box
            [filemgr removeItemAtPath: analyticsFile
                                error: NULL];
        }
    }
    return YES;
}
- (IBAction)radioButtonClicked:(id)sender {
    // Enable the 'Continue' button
    [self setNextEnabled:YES];
}

@end
