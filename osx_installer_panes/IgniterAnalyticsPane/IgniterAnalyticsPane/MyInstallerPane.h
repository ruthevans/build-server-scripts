//
//  MyInstallerPane.h
//  IgniterAnalyticsPane
//
//  Created by KrotosMacMini on 30/10/2018.
//  Copyright © 2018 Krotos. All rights reserved.
//

#import <InstallerPlugins/InstallerPlugins.h>

@interface MyInstallerPane : InstallerPane
{
    IBOutlet NSButton *radioButtonAgree;
    IBOutlet NSButton *radioButtonDisagree;
}

@end
