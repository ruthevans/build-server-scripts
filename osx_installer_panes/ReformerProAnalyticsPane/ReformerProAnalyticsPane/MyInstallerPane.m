//
//  MyInstallerPane.m
//  ReformerProAnalyticsPane
//
//  Created by KrotosMacMini on 15/03/2018.
//  Copyright © 2018 Krotos. All rights reserved.
//

#import "MyInstallerPane.h"




@implementation MyInstallerPane



- (NSString *)title
{
    return [[NSBundle bundleForClass:[self class]] localizedStringForKey:@"PaneTitle" value:nil table:nil];
}


/* Called when user clicks next from the analytics pane
 
 in the installer */
- (BOOL) shouldExitPane:(InstallerSectionDirection) inDirection
{
    if (InstallerDirectionForward == inDirection)
    {
        //NSString *analyticsDir  = @"/Library/Application Support/Krotos/Reformer Pro";
        NSString *analyticsFile = @"/tmp/installer_krotosrfp_analytics.dat";
        
        /* Set up a file manager to create/remove analytics.dat */
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        
        if (checkButton.state==NSOnState)
        {
            
            // Setting up bits and bobs needed for permission setting. 0777 here will grant all permissions 
            NSNumber *permissions;
            permissions = [NSNumber numberWithUnsignedLong:0666];
            NSDictionary *attr;
            attr = [NSDictionary dictionaryWithObject:permissions forKey:NSFilePosixPermissions];
            
            
            // Create a path for analytics.dat 
            //NSURL *newDir = [NSURL fileURLWithPath:analyticsDir];
            //[filemgr createDirectoryAtURL: newDir 
            //  withIntermediateDirectories: YES 
            //                   attributes: attr 
            //                        error: nil];
            
            // If the path already exists, make sure we have permissions to write to it 
            //[filemgr setAttributes: attr  
            //          ofItemAtPath: analyticsDir 
            //                 error: nil];
            
            
            /* Create analytics.dat */
            [filemgr createFileAtPath: analyticsFile 
                             contents: nil 
                           attributes: attr];
             
        } else {
            /* Remove analytics.dat - in case the user goes back in the installer to uncheck the box */
            [filemgr removeItemAtPath: analyticsFile 
                                error: NULL];
        }
    }
    return YES;
}


@end











