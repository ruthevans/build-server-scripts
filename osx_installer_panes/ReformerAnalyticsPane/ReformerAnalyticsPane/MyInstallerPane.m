//
//  MyInstallerPane.m
//  ReformerAnalyticsPane
//
//  Created by KrotosMacMini on 15/03/2018.
//  Copyright © 2018 Krotos. All rights reserved.
//

#import "MyInstallerPane.h"




@implementation MyInstallerPane



- (NSString *)title
{
    return [[NSBundle bundleForClass:[self class]] localizedStringForKey:@"PaneTitle" value:nil table:nil];
}


/* Called when user clicks next from the analytics pane
 
 in the installer */
- (BOOL) shouldExitPane:(InstallerSectionDirection) inDirection
{
    if (InstallerDirectionForward == inDirection)
    {
        NSString *analyticsFile = @"/tmp/installer_krotosrf_analytics.dat";
        
        /* Set up a file manager to create/remove analytics.dat */
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        
        if (checkButton.state==NSOnState)
        {
            /* Setting up bits and bobs needed for permission setting. 0777 here will grant all permissions */
            NSNumber *permissions;
            permissions = [NSNumber numberWithUnsignedLong:0777];
            NSDictionary *attr;
            attr = [NSDictionary dictionaryWithObject:permissions forKey:NSFilePosixPermissions];


            /* Create analytics.dat */
            [filemgr createFileAtPath: analyticsFile 
                             contents: nil 
                           attributes: attr];
             
        } else {
            /* Remove analytics.dat - in case the user goes back in the installer to uncheck the box */
            [filemgr removeItemAtPath: analyticsFile 
                                error: NULL];
        }
    }
    return YES;
}


@end











