[Setup]
;User definitions
#define DropboxDir "B:\Dropbox (Krotos)\Development\Synth"
#define PluginVer GetFileVersion("June.dll")
;Required definitions
AppName=Krotos June
AppVersion={#PluginVer}
DefaultDirName=C:\Program Files\Krotos\June
;Other definitions
AppPublisher=Krotos LTD
DisableDirPage=yes
OutputBaseFilename=June Installer
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\June
UninstallFilesDir=C:\Program Files\Krotos\June
LicenseFile=B:\Dropbox (Krotos)\Development\Synth\Documents\EULA\Synth License.rtf
;WizardImageFile="C:\Dev\Documents\Igniter Installers\Images\164x314.bmp"
;WizardSmallImageFile="C:\Dev\Documents\Igniter Installers\Images\55x58.bmp"

[Registry]
Root: HKLM64; Subkey: "SOFTWARE\Krotos\June"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}" ; Flags: uninsdeletekey
Root: HKLM64; Subkey: "SOFTWARE\Krotos\June"; ValueType: string; ValueName: "DisplayVersion"; ValueData: "{#PluginVer}" ; Flags: uninsdeletekey

[InstallDelete]
Type: files;          Name:{code:CopyVstDir}\June.dll
Type: files;          Name:{code:CopyVst3Dir}\June.vst3
Type: filesandordirs; Name:C:\Program Files\Common Files\Avid\Audio\Plug-Ins\June.aaxplugin
Type: files;          Name:C:\ProgramData\Krotos\June\analytics.dat

[UninstallDelete]
Type: files;          Name:C:\ProgramData\Krotos\June\analytics.dat
Type: filesandordirs; Name:C:\Program Files\Krotos\June
Type: filesandordirs; Name:C:\ProgramData\Krotos\June
Type: dirifempty;     Name:C:\ProgramData\Krotos
Type: dirifempty;     Name:C:\Program Files\Krotos

[Files]
; DOCUMENTATION
Source: {#DropboxDir}\Documents\Manual\Alpha Manual\Synth Manual.pdf;  DestDir: C:\Program Files\Krotos\June\Documentation\Manual; Flags: ignoreversion recursesubdirs
Source: {#DropboxDir}\Documents\EULA\Synth License.pdf;                DestDir: C:\Program Files\Krotos\June\Documentation\EULA;   Flags: ignoreversion recursesubdirs
; IR FILES
Source: {#DropboxDir}\IR Files\*; DestDir: C:\ProgramData\Krotos\June\IR;   Flags: ignoreversion recursesubdirs
; PRESETS
Source: {#DropboxDir}\Presets\Alpha Presets\Windows\*;  DestDir: C:\ProgramData\Krotos\June\Presets\main; Flags: ignoreversion recursesubdirs; Permissions: everyone-modify
; PLUGINS
Source: C:\Dev\Documents\June Installers\June.dll;           DestDir: {code:CopyVstDir}
Source: C:\Dev\Documents\June Installers\June.vst3;          DestDir: {code:CopyVst3Dir}
Source: C:\Dev\Documents\June Installers\June.aaxplugin;     DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins;                   Flags: ignoreversion recursesubdirs
Source: C:\Dev\Documents\aaxplugin icon files\PlugIn.ico;    DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\June.aaxplugin;    Flags: ignoreversion recursesubdirs; Attribs: hidden
Source: C:\Dev\Documents\aaxplugin icon files\desktop.ini;   DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\June.aaxplugin;    Flags: ignoreversion recursesubdirs; Attribs: hidden

[Dirs]
Name:   "C:\Program Files\Common Files\Avid\Audio\Plug-Ins\June.aaxplugin"; Attribs: system 

[Code]
var
  CopyDirPage: TInputDirWizardPage;

procedure InitializeWizard();
begin
  CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select where to save the VST files', '',  '', False, '');
  CopyDirPage.Add('VST directory:');
  CopyDirPage.Values[0] := 'C:\Program Files\Steinberg\VstPlugins'
  CopyDirPage.Add('VST3 directory:');
  CopyDirPage.Values[1] := 'C:\Program Files\Common Files\VST3'

end;

function CopyVstDir(Params: string): string;
begin
  Result := CopyDirPage.Values[0];
end;

function CopyVst3Dir(Params: string): string;
begin
  Result := CopyDirPage.Values[1];
end;