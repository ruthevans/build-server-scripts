[Setup]
;SignTool=signtool
AppName=Reformer Pro (32-bit)
AppVersion=1.1.3
DefaultDirName=C:\Program Files\Krotos\Reformer Pro
OutputBaseFilename=Reformer Pro 32
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\Reformer Pro
UninstallFilesDir=C:\Program Files\Krotos\Reformer Pro
AppPublisher=Krotos LTD
DisableDirPage=yes
UninstallDisplayName=Reformer Pro (32-bit)
LicenseFile="B:\Dropbox (Krotos)\Development\Reformer\Reformer Pro\Documentation\EULA\Reformer Pro License.rtf"
WizardImageFile="C:\Dev\Documents\Reformer Pro Installers\Images\Reformer Pro Installer for Windows 168x314.bmp"
WizardSmallImageFile="C:\Dev\Documents\Reformer Pro Installers\Images\Reformer Installer Windows 55x58.bmp"
;SignTool=krotossign         
;SignedUninstaller=yes

[Registry]
Root: HKLM32; Subkey: "SOFTWARE\Krotos\ReformerPro"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}" ; Flags: uninsdeletekey

[InstallDelete]
Type: files;          Name:"{code:VstDir}\Reformer Pro.dll"
Type: filesandordirs; Name:"C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin"
Type: files;          Name:"C:\Program Files\Krotos\Reformer Pro\ReformerProOfflineTool.exe"
Type: files;          Name:"C:\ProgramData\Krotos\Reformer Pro\analytics.dat"

[UninstallDelete]
;Type: files;          Name:"{code:VstDir}\Reformer Pro.dll"
Type: files;          Name:"C:\Program Files\Krotos\Reformer Pro\ReformerProOfflineTool.exe"
Type: filesandordirs; Name:"C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin"
Type: filesandordirs; Name:"C:\Program Files\Krotos\Reformer Pro"
Type: dirifempty;     Name:"C:\Program Files\Krotos"
Type: files;          Name:"C:\ProgramData\Krotos\Reformer Pro\analytics.dat"

[Dirs]
Name: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries; Permissions: users-modify
Name: C:\ProgramData\Krotos\Reformer Pro  ; Permissions: everyone-full
Name: C:\ProgramData\Krotos\Reformer Pro\Library  ; Permissions: everyone-full
Name: C:\ProgramData\Krotos\Reformer Pro\Library\Presets  ; Permissions: everyone-full
Name: C:\ProgramData\Krotos\Reformer Pro\Library\Presets\main  ; Permissions: everyone-full
;Name: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin; Attribs: system 

[Files]
; DLL
Source: C:\Dev\Documents\Reformer Pro Installers\Installer Release 32\Reformer Pro.dll;       DestDir: {code:VstDir}

; AAX
Source: C:\Dev\Documents\Reformer Pro Installers\Installer Release 32\Reformer Pro.aaxplugin;    DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins;                          Flags: ignoreversion recursesubdirs; Permissions: Everyone-full 
Source: C:\Dev\Documents\Reformer Pro Installers\PlugIn.ico;                                                   DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin;   Flags: ignoreversion recursesubdirs; Attribs: hidden
Source: C:\Dev\Documents\Reformer Pro Installers\desktop.ini;                                                  DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin;   Flags: ignoreversion recursesubdirs; Attribs: hidden


; OFFLINE TOOL
Source: C:\Dev\Documents\Reformer Pro Installers\Installer Release 32\AnalysisTool.exe;   DestDir: C:\Program Files\Krotos\Reformer Pro; Flags: ignoreversion

; DOCUMENTATION
Source: "B:\Dropbox (Krotos)\Development\Reformer\Reformer Pro\Documentation\*";                    DestDir: C:\Program Files\Krotos\Reformer Pro\Documentation; Flags: ignoreversion recursesubdirs

; DATABASES
;Black Leopard Pro
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\versionNumber.txt;     DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro; Flags: ignoreversion recursesubdirs ;   Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\44100\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\44100;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install44100
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\48000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\48000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install48000
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\88200\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\88200;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install88200
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\96000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\96000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install96000
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\176400\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\176400;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install176400
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard Pro\Clustering\192000\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\Clustering\192000;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install192000
;Electronic Pro
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\versionNumber.txt;     DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro; Flags: ignoreversion recursesubdirs uninsneveruninstall;  Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\44100\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\44100;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install44100
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\48000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\48000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install48000
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\88200\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\88200;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install88200
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\96000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\96000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install96000
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\176400\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\176400;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install176400
Source: B:\Sound Libraries\Reformer Libraries\Electronic Pro\Clustering\192000\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\Clustering\192000;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install192000
;Fruit Squash Pro
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\versionNumber.txt;     DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro; Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\44100\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\44100;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install44100
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\48000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\48000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install48000
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\88200\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\88200;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install88200
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\96000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\96000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install96000
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\176400\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\176400;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install176400
Source: B:\Sound Libraries\Reformer Libraries\Fruit Squash Pro\Clustering\192000\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\Clustering\192000;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install192000
;Leather Foley Pro
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\versionNumber.txt;     DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro; Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\44100\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\44100;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install44100
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\48000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\48000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install48000
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\88200\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\88200;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install88200
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\96000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\96000;   Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install96000
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\176400\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\176400;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install176400
Source: B:\Sound Libraries\Reformer Libraries\Leather Foley Pro\Clustering\192000\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\Clustering\192000;  Flags: ignoreversion recursesubdirs ;  Permissions: everyone-modify;  Check: Install192000

; SOUND LIBRARY
Source: B:\Sound Libraries\Reformer Sample Libraries\Krotos\Black Leopard\*;  DestDir: {code:SoundLibDir}\Reformer Sample Libraries\Krotos\Black Leopard;  Flags: ignoreversion recursesubdirs ;   Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Sample Libraries\Krotos\Electronic\*;     DestDir: {code:SoundLibDir}\Reformer Sample Libraries\Krotos\Electronic;     Flags: ignoreversion recursesubdirs ;   Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Sample Libraries\Krotos\Leather\*;        DestDir: {code:SoundLibDir}\Reformer Sample Libraries\Krotos\Leather;        Flags: ignoreversion recursesubdirs ;   Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Sample Libraries\Krotos\Fruit and Veg\Fruit and Veg Squash\*;  DestDir: {code:SoundLibDir}\Reformer Sample Libraries\Krotos\Fruit and Veg\Fruit and Veg Squash;  Flags: ignoreversion recursesubdirs ;   Permissions: everyone-modify

; THUMBNAILS
Source: "C:\Dev\Documents\Reformer Installers\Thumbnails\*";                       DestDir: C:\ProgramData\Krotos\Reformer\Library\Thumbnails;           Flags: ignoreversion recursesubdirs

; PRESET
Source: B:\Dropbox (Krotos)\Development\Reformer\Reformer Pro\Presets\Windows\*;       DestDir: C:\ProgramData\Krotos\Reformer Pro\Library\Presets\main;       Flags: ignoreversion recursesubdirs   ;   Permissions: everyone-full

; SETTING
Source: "C:\Dev\Documents\Reformer Installers\settings.dat";                       DestDir: {userappdata}\Krotos\Reformer Pro;      Flags: ignoreversion recursesubdirs    ;   Permissions: everyone-full

[Run]
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\NewSamplesPath.txt"" /grant Everyone:M"; Flags:runhidden shellexec
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\NewSamplesPath.txt""    /grant Everyone:M"; Flags:runhidden shellexec
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\NewSamplesPath.txt""  /grant Everyone:M"; Flags:runhidden shellexec
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\NewSamplesPath.txt"" /grant Everyone:M"; Flags:runhidden shellexec
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries"" /grant Everyone:M"; Flags:runhidden shellexec
;Filename: "{sys}\attrib.exe"; Parameters: """C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer Pro.aaxplugin"" +s"; Flags:runhidden shellexec 

[Code]
var
  CopyDirPage: TInputDirWizardPage;
  SoundLibLocationPage: TInputDirWizardPage;
  SampleRatePage: TInputOptionWizardPage;
  AnalyticsPage: TInputOptionWizardPage;
procedure InitializeWizard();

begin
  CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select where to save the VST file', '',  '', False, '');
  CopyDirPage.Add('VST directory:');
  CopyDirPage.Values[0] := 'C:\Program Files\Steinberg\VstPlugins'
  
  SampleRatePage := CreateInputOptionPage(wpInfoBefore, 'What sample rates to install Reformers sample libraries at?', 'This will allow you to use Reformers Sample Libraries at these sample rates in your DAW.', 'Sample rates:', False, False);
  SampleRatePage.Add('44100');
  SampleRatePage.Add('48000');
  SampleRatePage.Add('88200');
  SampleRatePage.Add('96000');
  SampleRatePage.Add('176400');
  SampleRatePage.Add('192000');
  // Make all checkboxes checked, initially
  SampleRatePage.Values[0] := True;
  SampleRatePage.Values[1] := True;
  SampleRatePage.Values[2] := True;
  SampleRatePage.Values[3] := True;
  SampleRatePage.Values[4] := True;
  SampleRatePage.Values[5] := True;

  SoundLibLocationPage := CreateInputDirPage(wpInfoBefore, 'Select where to save the sample library files', '',  '', False, '');
  SoundLibLocationPage.Add('Sample library directory:');
  SoundLibLocationPage.Values[0] := 'C:\Program Files\Krotos\Reformer'

  AnalyticsPage := CreateInputOptionPage(wpInfoBefore, 'Usage Analytics','', 'Help us to continue creating innovative plugins by allowing us to collect anonymous usage data. We only collect information on how you use Krotos plugins, and do not use your personal data in any way.'+#13#10+#13#10+'Participation is voluntary, and you may opt in now.', false, false);
  AnalyticsPage.Add('I agree to share my anonymous plugin usage data.');
  AnalyticsPage.Values[0] := False;
end;

Procedure CurStepChanged(CurStep: TSetupStep);
var
newline:string;
begin
  if CurStep = ssPostInstall then
  begin
    newline := #13#10
    SaveStringToFile('C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard Pro\NewSamplesPath.txt', SoundLibLocationPage.Values[0] + '\Reformer Sample Libraries\Krotos\Black Leopard' + newline , False);
    SaveStringToFile('C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Electronic Pro\NewSamplesPath.txt',    SoundLibLocationPage.Values[0] + '\Reformer Sample Libraries\Krotos\Electronic' + newline , False);
    SaveStringToFile('C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Fruit Squash Pro\NewSamplesPath.txt',  SoundLibLocationPage.Values[0] + '\Reformer Sample Libraries\Krotos\Fruit and Veg\Fruit and Veg Squash' + newline , False);
    SaveStringToFile('C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Leather Foley Pro\NewSamplesPath.txt', SoundLibLocationPage.Values[0] + '\Reformer Sample Libraries\Krotos\Leather' + newline , False);
    if AnalyticsPage.Values[0] = True then
    begin
      SaveStringToFile('C:\ProgramData\Krotos\Reformer Pro\analytics.dat','',False);
    end;
  end;
end;

function VstDir(Params: string): string;
begin
  Result := CopyDirPage.Values[0];
end;

function SoundLibDir(Params: string): string;
begin
  Result := SoundLibLocationPage.Values[0];
end;

function Install44100: Boolean;
begin;
  Result := SampleRatePage.Values[0];
end;

function Install48000: Boolean;
begin;
  Result := SampleRatePage.Values[1];
end;

function Install88200: Boolean;
begin;
  Result := SampleRatePage.Values[2];
end;

function Install96000: Boolean;
begin;
  Result := SampleRatePage.Values[3];
end;

function Install176400: Boolean;
begin;
  Result := SampleRatePage.Values[4];
end;

function Install192000: Boolean;
begin;
  Result := SampleRatePage.Values[5];
end;
