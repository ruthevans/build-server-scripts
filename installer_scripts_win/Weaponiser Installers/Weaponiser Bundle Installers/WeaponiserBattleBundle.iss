
[Setup]
AppName=Weaponiser Battle Bundle
AppVersion=1.0
DefaultDirName=C:\Program Files\Weaponiser
OutputBaseFilename=Weaponiser Battle Bundle
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\Weaponiser Bundles
UninstallFilesDir=C:\Program Files\Krotos\Weaponiser
AppPublisher=Krotos LTD
DisableDirPage=yes
UninstallDisplayName=Weaponiser Battle Bundle
LicenseFile="B:\Dropbox (Krotos)\Development\Weaponiser\Documents\EULA\License\Weaponiser License.rtf"
WizardSmallImageFile="B:\Dropbox (Krotos)\Development\Weaponiser\Installer art\Weaponiser Installer for Windows 55x58_compatibility_export.bmp"
WizardImageFile="B:\Dropbox (Krotos)\Development\Weaponiser\Installer art\Weaponiser Installer for Windows 164x314_compatibility_export.bmp"

;SignTool=krotossign         
;SignedUninstaller=yes

[Registry]
Root: HKLM64; Subkey: "SOFTWARE\Krotos\WeaponiserBattleBundle"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}" ; Flags: uninsdeletekey


[Files]

; DOCUMENTATION
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Documents\Metadata\Krotos Weaponiser Battle Bundle Metadata.pdf";   DestDir: C:\Program Files\Krotos\Weaponiser\Documentation\Metadata  

; PRESET
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Presets\Battle Bundle Presets\Windows\*";   DestDir: C:\ProgramData\Krotos\Weaponiser\Library\Presets\main;    Flags: ignoreversion recursesubdirs; Permissions: users-full

; FACTORY ASSETS
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Factory Assets\Battle Bundle Assets\*"; Excludes: "._*,*.repeaks";  DestDir: C:\ProgramData\Krotos\Weaponiser\Library\Factory Assets;  Flags: ignoreversion recursesubdirs; Permissions: users-full
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Factory Assets\Body Falls Assets\*"; Excludes: "._*,*.repeaks";  DestDir: C:\ProgramData\Krotos\Weaponiser\Library\Factory Assets;  Flags: ignoreversion recursesubdirs; Permissions: users-full

; DATABASES
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Databases\WP DB\filetags.db";   DestDir: C:\ProgramData\Krotos\Weaponiser\Databases;  Flags: ignoreversion recursesubdirs; Permissions: users-full
