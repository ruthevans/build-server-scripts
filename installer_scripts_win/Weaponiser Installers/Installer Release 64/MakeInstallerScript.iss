
[Setup]
AppName=Weaponiser (64-bit)
AppVersion=1.1
DefaultDirName=C:\Program Files\Weaponiser
OutputBaseFilename=Weaponiser 64
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\Weaponiser
UninstallFilesDir=C:\Program Files\Krotos\Weaponiser
AppPublisher=Krotos LTD
DisableDirPage=yes
UninstallDisplayName=Weaponiser (64-bit)
LicenseFile="B:\Dropbox (Krotos)\Development\Weaponiser\Documents\EULA\License\Weaponiser License.rtf"
WizardSmallImageFile="B:\Dropbox (Krotos)\Development\Weaponiser\Installer art\Weaponiser Installer for Windows 55x58_compatibility_export.bmp"
WizardImageFile="B:\Dropbox (Krotos)\Development\Weaponiser\Installer art\Weaponiser Installer for Windows 164x314_compatibility_export.bmp"


;SignTool=krotossign         
;SignedUninstaller=yes
                                                   
[InstallDelete]
Type: files;          Name:"{code:CopyDir}\Weaponiser.dll"
Type: files;          Name:"C:\Program Files\Common Files\VST3\Weaponiser.vst3"
Type: filesandordirs; Name:"C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Weaponiser.aaxplugin"


[Files]
; DLL
Source: "C:\Dev\Documents\Weaponiser Bundle Installers\Basic Plugins\Weaponiser.dll";          DestDir: {code:CopyDir}

; VST3
Source: "C:\Dev\Documents\Weaponiser Bundle Installers\Basic Plugins\Weaponiser.vst3";         DestDir: C:\Program Files\Common Files\VST3

; AAX
Source: "C:\Dev\Documents\Weaponiser Bundle Installers\Basic Plugins\Weaponiser.aaxplugin";    DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins;            Flags: ignoreversion recursesubdirs
Source: "C:\Dev\Documents\Weaponiser Installers\PlugIn.ico";  DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Weaponiser.aaxplugin;   Flags: ignoreversion recursesubdirs; Attribs: hidden
Source: "C:\Dev\Documents\Weaponiser Installers\desktop.ini"; DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Weaponiser.aaxplugin;   Flags: ignoreversion recursesubdirs; Attribs: hidden


; DOCUMENTATION
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Documents\Manual\Manual Release\Weaponiser Manual.pdf";    DestDir: C:\Program Files\Krotos\Weaponiser\Documentation\Manual; Flags: ignoreversion recursesubdirs
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Documents\EULA\License\Weaponiser License.pdf";         DestDir: C:\Program Files\Krotos\Weaponiser\Documentation\EULA; Flags: ignoreversion recursesubdirs

; PRESET
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Presets\1.1.0 presets\Windows\Demo\*";   DestDir: C:\ProgramData\Krotos\Weaponiser\Library\Presets\main;    Flags: ignoreversion recursesubdirs; Permissions: users-full

; FACTORY ASSETS
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Factory Assets\Factory Assets 1.0.0\*"; Excludes: "._*,*.repeaks";  DestDir: C:\ProgramData\Krotos\Weaponiser\Library\Factory Assets;  Flags: ignoreversion recursesubdirs; Permissions: users-full

; DATABASES
Source: "B:\Dropbox (Krotos)\Development\Weaponiser\Databases\WP DB\filetags.db";   DestDir: C:\ProgramData\Krotos\Weaponiser\Databases;  Flags: ignoreversion recursesubdirs; Permissions: users-full

; SETTING
Source: "C:\Dev\Documents\Weaponiser Installers\settings.dat";                       DestDir: {userappdata}\Krotos\Weaponiser;      Flags: ignoreversion recursesubdirs        ;   Permissions: everyone-modify


; This makes the .aaxplugin show the correct icon (same as attrib +s)
[Dirs]
Name:   "C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Weaponiser.aaxplugin"; Attribs: system 

[Code]
var
  CopyDirPage: TInputDirWizardPage;
procedure InitializeWizard();

begin
  CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select where to save the VST file', '',  '', False, '');
  CopyDirPage.Add('VST directory:');
  CopyDirPage.Values[0] := 'C:\Program Files\Steinberg\VstPlugins'
  
end;

function CopyDir(Params: string): string;
begin
  Result := CopyDirPage.Values[0];
end;
