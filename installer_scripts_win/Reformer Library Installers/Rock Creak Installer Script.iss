;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Rock Creak Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Rock Creak Sound Library Installer


[Files]
;Sound library samples
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Charcoal.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Clay Pebbles.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Crisp Long.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Crumble.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Crunch Long Low.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Crunch Long.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Crunch Short.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Dry Leaves.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Hollow.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Medium Soft.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Medium Transient.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Medium.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Small.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Destruction\Rock Creak\DTCK ROCK CRACK Tiny.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Destruction\Rock Creak;  Flags: ignoreversion recursesubdirs

;Metadata
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Destruction CK Metadata.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Destruction CK Metadata.xls; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: D:\Sound Libraries\Reformer Libraries\Rock Creak\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Rock Creak;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Libraries\Rock Creak\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Rock Creak\Clustering;  Flags: ignoreversion recursesubdirs
