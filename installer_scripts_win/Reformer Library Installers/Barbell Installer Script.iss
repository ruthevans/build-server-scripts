;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Barbell Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Barbell Sound Library Installer

[Files]
;Sound library samples
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_bar_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_clear_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_crunch_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_hammer_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell\impact_p0_barbell_solid_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Barbell;  Flags: ignoreversion recursesubdirs

;Metadata
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal CMCK Metadata_Website.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal ConstructionKit Tracklist.xls; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: D:\Sound Libraries\Reformer Libraries\Barbell\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Barbell;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Libraries\Barbell\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Barbell\Clustering;  Flags: ignoreversion recursesubdirs
