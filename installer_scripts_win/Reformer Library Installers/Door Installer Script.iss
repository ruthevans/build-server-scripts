;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Door Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Door Sound Library Installer


[Files]
;Sound library samples
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_dark_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_heavy_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Door\impact_p0_door_soft_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Door;  Flags: ignoreversion recursesubdirs

;Metadata
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal CMCK Metadata_Website.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal ConstructionKit Tracklist.xls; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: D:\Sound Libraries\Reformer Libraries\Door\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Door;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Libraries\Door\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Door\Clustering;  Flags: ignoreversion recursesubdirs
