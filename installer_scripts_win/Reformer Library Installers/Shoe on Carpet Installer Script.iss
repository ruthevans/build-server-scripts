;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Shoe on Carpet Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Shoe on Carpet Sound Library Installer


[Files]
;Sound library samples
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_01_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_01_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_01_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_01_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_01_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet\Shoe_Slide_on_Carpet_fast_02_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Shoe on Carpet;  Flags: ignoreversion recursesubdirs

;Metadata
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Metadata\Soundbits Drag and Slide List.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: B:\Sound Libraries\Reformer Libraries\Shoe on Carpet\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Shoe on Carpet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Libraries\Shoe on Carpet\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Shoe on Carpet\Clustering;  Flags: ignoreversion recursesubdirs
