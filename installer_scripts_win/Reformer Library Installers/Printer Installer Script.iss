;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Printer Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Printer Sound Library Installer


[Files]
;Sound library samples
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_13.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_14.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_15.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_16.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_17.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_18.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_19.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_20.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer\Printer_HomeOffice_21.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Electro Mechanics\Printer;  Flags: ignoreversion recursesubdirs

;Metadata
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Metadata\Soundbits Electro Mechanics Toolkit List.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: B:\Sound Libraries\Reformer Libraries\Printer\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Printer;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Libraries\Printer\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Printer\Clustering;  Flags: ignoreversion recursesubdirs
