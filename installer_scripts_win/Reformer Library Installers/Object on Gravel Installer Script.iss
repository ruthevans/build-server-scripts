;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Object on Gravel Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Object on Gravel Sound Library Installer


[Files]
;Sound library samples
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_01_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_13.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel\Object_Sliding_on_Gravel_02_14.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Object on Gravel;  Flags: ignoreversion recursesubdirs

;Metadata
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Metadata\Soundbits Drag and Slide List.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: B:\Sound Libraries\Reformer Libraries\Object on Gravel\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Object on Gravel;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Libraries\Object on Gravel\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Object on Gravel\Clustering;  Flags: ignoreversion recursesubdirs
