;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Wood on Parquet Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Wood on Parquet Sound Library Installer


[Files]
;Sound library samples
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_01_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_01_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Slide_on_Parquet_02_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_03_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_03_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_04_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_04_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_04_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_05_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_05_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_05_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_05_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_06_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_06_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_06_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_06_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_06_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_07_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_07_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_07_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_08_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_08_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenCrate_Sliding_on_Parquet_08_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenTable_Sliding_on_Parquet_01_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenTable_Sliding_on_Parquet_01_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet\WoodenTable_Sliding_on_Parquet_01_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Drag and Slide\Wood on Parquet;  Flags: ignoreversion recursesubdirs

;Metadata
Source: B:\Sound Libraries\Reformer Sample Libraries\Soundbits\Metadata\Soundbits Drag and Slide List.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\Soundbits\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: B:\Sound Libraries\Reformer Libraries\Wood on Parquet\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Wood on Parquet;  Flags: ignoreversion recursesubdirs
Source: B:\Sound Libraries\Reformer Libraries\Wood on Parquet\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Wood on Parquet\Clustering;  Flags: ignoreversion recursesubdirs
