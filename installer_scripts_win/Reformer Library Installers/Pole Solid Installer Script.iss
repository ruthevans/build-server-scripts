;This inno file has been generated by generate_library_installers.py

[Setup]
DefaultDirName={pf}
AppName=Pole Solid Sound Library
AppVersion=1.0
AppPublisher=Krotos LTD
DisableDirPage=yes
LicenseFile=C:\Dev\Documents\Reformer Sound Library Installers\Krotos Sound Library EULA.rtf
WizardImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp
WizardSmallImageFile=C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp
; Un-comment the following line to enable auto-signing
;SignTool=krotossign
OutputBaseFilename=Pole Solid Sound Library Installer

[Files]
;Sound library samples
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_01.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_02.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_03.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_04.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_05.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_06.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_07.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_08.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_09.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_10.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_11.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid\impact_p0_pole_solid_12.wav; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Cinematic Metal\Pole Solid;  Flags: ignoreversion recursesubdirs

;Metadata
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal CMCK Metadata_Website.pdf; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Sample Libraries\BOOM\Metadata\BOOM Cinematic Metal ConstructionKit Tracklist.xls; Destdir: C:\Program Files\Krotos\Reformer\Reformer Sample Libraries\BOOM\Metadata;  Flags: ignoreversion recursesubdirs

;Sound library database
Source: D:\Sound Libraries\Reformer Libraries\Pole Solid\versionNumber.txt; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Pole Solid;  Flags: ignoreversion recursesubdirs
Source: D:\Sound Libraries\Reformer Libraries\Pole Solid\Clustering\*; Destdir: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Pole Solid\Clustering;  Flags: ignoreversion recursesubdirs
