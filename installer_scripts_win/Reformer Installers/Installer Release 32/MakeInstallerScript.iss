[Setup]
;SignTool=signtool
AppName=Reformer (32-bit)
AppVersion=1.1.0
DefaultDirName=C:\Program Files\Reformer
OutputBaseFilename=Reformer 32
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\Reformer
UninstallFilesDir=C:\Program Files\Krotos\Reformer
AppPublisher=Krotos LTD
DisableDirPage=yes
UninstallDisplayName=Reformer (32-bit)
LicenseFile="C:\Dev\Documents\Reformer Installers\Documentation\EULA\Reformer License.rtf"
WizardImageFile="C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 168x314.bmp"
WizardSmallImageFile="C:\Dev\Documents\Reformer Installers\Images\Reformer Installer Windows 55x58.bmp"
;SignTool=krotossign         
;SignedUninstaller=yes

[InstallDelete]
Type: files;          Name:{code:VstDir}\Reformer.dll
Type: filesandordirs; Name:C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer.aaxplugin
Type: files;          Name:C:\ProgramData\Krotos\Reformer\analytics.dat

[UninstallDelete]
Type: files;          Name:{code:VstDir}
Type: filesandordirs; Name:C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Reformer.aaxplugin
Type: dirifempty;     Name:C:\Program Files\Krotos\Reformer
Type: files;          Name:C:\ProgramData\Krotos\Reformer\analytics.dat
Type: dirifempty;     Name:C:\ProgramData\Krotos\Reformer
Type: dirifempty;     Name:C:\ProgramData\Krotos

[Dirs]
Name: C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries; Permissions: users-modify
Name: C:\ProgramData\Krotos\Reformer  ; Permissions: everyone-modify

[Files]
; DLL
Source: "C:\Dev\Documents\Reformer Installers\Installer Release 32\Reformer.dll";       DestDir: {code:VstDir}

; AAX
Source: "C:\Dev\Documents\Reformer Installers\Installer Release 32\Reformer.aaxplugin";   DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins;      Flags: ignoreversion recursesubdirs

; DOCUMENTATION
Source: "C:\Dev\Documents\Reformer Installers\Documentation\*";        DestDir: C:\Program Files\Krotos\Reformer\Documentation; Flags: ignoreversion recursesubdirs

; DATABASES
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\versionNumber.txt;     DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard; Flags: ignoreversion recursesubdirs;   Permissions: everyone-modify
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\44100\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\44100;   Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install44100
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\48000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\48000;   Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install48000
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\88200\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\88200;   Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install88200
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\96000\*;    DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\96000;   Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install96000
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\176400\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\176400; Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install176400
Source: B:\Sound Libraries\Reformer Libraries\Black Leopard\Clustering\192000\*;   DestDir:  C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\Clustering\192000; Flags: ignoreversion recursesubdirs;  Permissions: everyone-modify;  Check: Install192000

; SOUND LIBRARY
Source: B:\Sound Libraries\Reformer Sample Libraries\Krotos\Black Leopard\*;     DestDir: {code:SoundLibDir}\Reformer Sample Libraries\Krotos\Black Leopard;   Flags: ignoreversion recursesubdirs

; THUMBNAILS
Source: "C:\Dev\Documents\Reformer Installers\Thumbnails\*";                       DestDir: C:\ProgramData\Krotos\Reformer\Library\Thumbnails;           Flags: ignoreversion recursesubdirs
Source: "C:\Dev\Documents\Reformer Sound Library Installers\new_thumbnails\*";     DestDir: C:\ProgramData\Krotos\Reformer\Library\Thumbnails;           Flags: ignoreversion recursesubdirs 
                                                                                                         
; PRESET
Source: "Presets\*";                                                    DestDir: C:\ProgramData\Krotos\Reformer\Library\Presets;        Flags: ignoreversion recursesubdirs

; SETTING
Source: "C:\Dev\Documents\Reformer Installers\settings.dat"; DestDir: C:\Users\Setup\AppData\Roaming\Krotos\Reformer;      Flags: ignoreversion recursesubdirs

[Run]
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\NewSamplesPath.txt"" /grant Everyone:M"; Flags:runhidden shellexec
Filename: "{sys}\icacls.exe"; Parameters: """C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries"" /grant Everyone:M"; Flags:runhidden shellexec

[Code]
var
  CopyDirPage: TInputDirWizardPage;
  SoundLibLocationPage: TInputDirWizardPage;
  SampleRatePage: TInputOptionWizardPage;
  AnalyticsPage: TInputOptionWizardPage;
procedure InitializeWizard();

begin
  CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select where to save the VST file', '',  '', False, '');
  CopyDirPage.Add('VST directory:');
  CopyDirPage.Values[0] := 'C:\Program Files\Steinberg\VstPlugins'
  
  SampleRatePage := CreateInputOptionPage(wpInfoBefore, 'What sample rates to install Reformers sample libraries at?', 'This will allow you to use Reformers Sample Libraries at these sample rates in your DAW.', 'Sample rates:', False, False);
  SampleRatePage.Add('44100');
  SampleRatePage.Add('48000');
  SampleRatePage.Add('88200');
  SampleRatePage.Add('96000');
  SampleRatePage.Add('176400');
  SampleRatePage.Add('192000');
  // Make all checkboxes checked, initially
  SampleRatePage.Values[0] := True;
  SampleRatePage.Values[1] := True;
  SampleRatePage.Values[2] := True;
  SampleRatePage.Values[3] := True;
  SampleRatePage.Values[4] := True;
  SampleRatePage.Values[5] := True;

  SoundLibLocationPage := CreateInputDirPage(wpInfoBefore, 'Select where to save the sample library files', '',  '', False, '');
  SoundLibLocationPage.Add('Sample library directory:');
  SoundLibLocationPage.Values[0] := 'C:\Program Files\Krotos\Reformer'

  AnalyticsPage := CreateInputOptionPage(wpInfoBefore, 'Usage Analytics','', 'Help us to continue creating innovative plugins by allowing us to collect anonymous usage data. We only collect information on how you use Krotos plugins, and do not use your personal data in any way.'+#13#10+#13#10+'Participation is voluntary, and you may opt in now.', false, false);
  AnalyticsPage.Add('I agree to share my anonymous plugin usage data.');
  AnalyticsPage.Values[0] := False;
end;

Procedure CurStepChanged(CurStep: TSetupStep);
var
newline:string;
begin
  if CurStep = ssPostInstall then
  begin
    newline := #13#10
    SaveStringToFile('C:\ProgramData\Krotos\Reformer\Library\Reformer Libraries\Black Leopard\NewSamplesPath.txt', SoundLibLocationPage.Values[0] + '\Reformer Sample Libraries\Krotos\Black Leopard' + newline , False);
    if AnalyticsPage.Values[0] = True then
    begin
      SaveStringToFile('C:\ProgramData\Krotos\Reformer\analytics.dat','',False);
    end;
  end;
end;

function VstDir(Params: string): string;
begin
  Result := CopyDirPage.Values[0];
end;

function SoundLibDir(Params: string): string;
begin
  Result := SoundLibLocationPage.Values[0];
end;

function Install44100: Boolean;
begin;
  Result := SampleRatePage.Values[0];
end;

function Install48000: Boolean;
begin;
  Result := SampleRatePage.Values[1];
end;

function Install88200: Boolean;
begin;
  Result := SampleRatePage.Values[2];
end;

function Install96000: Boolean;
begin;
  Result := SampleRatePage.Values[3];
end;

function Install176400: Boolean;
begin;
  Result := SampleRatePage.Values[4];
end;

function Install192000: Boolean;
begin;
  Result := SampleRatePage.Values[5];
end;

