[Setup]
;Required definitions
AppName=Krotos Igniter
AppVersion=1.1
DefaultDirName=C:\Program Files\Krotos\Igniter
;Other definitions
AppPublisher=Krotos LTD
DisableDirPage=yes
OutputBaseFilename=Igniter Installer
OutputDir=C:\Users\Krotos\Desktop\Jenkins Installers\Igniter
UninstallFilesDir=C:\Program Files\Krotos\Igniter
LicenseFile=B:\Dropbox (Krotos)\Development\Engines\Documents\EULA\Igniter License.rtf
WizardImageFile="C:\Dev\Documents\Igniter Installers\Images\Igniter164x314.bmp"
WizardSmallImageFile="C:\Dev\Documents\Igniter Installers\Images\Igniter55x58.bmp"
DiskSpanning=true

;Uncomment the following lines to sign the installer if this script is not being run through jenkins
;SignTool=krotossign         
;SignedUninstaller=yes
[Registry]
Root: HKLM64; Subkey: "SOFTWARE\Krotos\Igniter"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}" ; Flags: uninsdeletekey
Root: HKLM64; Subkey: "SOFTWARE\Krotos\Igniter"; ValueType: string; ValueName: "DisplayVersion"; ValueData: "1.0.0" ; Flags: uninsdeletekey

[InstallDelete]
Type: files;          Name:{code:CopyDir}\Igniter.dll
Type: filesandordirs; Name:C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Igniter.aaxplugin
Type: files;          Name:C:\ProgramData\Krotos\Igniter\analytics.dat

[UninstallDelete]
Type: files;          Name:C:\ProgramData\Krotos\Igniter\analytics.dat
Type: filesandordirs; Name:C:\Program Files\Krotos\Igniter
Type: filesandordirs; Name:C:\ProgramData\Krotos\Igniter
Type: dirifempty;     Name:C:\ProgramData\Krotos
Type: dirifempty;     Name:C:\Program Files\Krotos
[Files]

; DLL
Source: C:\Dev\Documents\Igniter Installers\Igniter.dll;           DestDir: {code:CopyDir}

; AAX
Source: C:\Dev\Documents\Igniter Installers\Igniter.aaxplugin;    DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins;                      Flags: ignoreversion recursesubdirs
Source: C:\Dev\Documents\Igniter Installers\PlugIn.ico;           DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Igniter.aaxplugin;    Flags: ignoreversion recursesubdirs; Attribs: hidden
Source: C:\Dev\Documents\Igniter Installers\desktop.ini;          DestDir: C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Igniter.aaxplugin;    Flags: ignoreversion recursesubdirs; Attribs: hidden

; DOCUMENTATION
Source: B:\Dropbox (Krotos)\Development\Engines\Documents\Manual\Manual Beta\Igniter Manual.pdf; DestDir: C:\Program Files\Krotos\Igniter\Documentation\Manual; Flags: ignoreversion recursesubdirs
Source: B:\Dropbox (Krotos)\Development\Engines\Documents\EULA\Igniter License.pdf;               DestDir: C:\Program Files\Krotos\Igniter\Documentation\EULA;   Flags: ignoreversion recursesubdirs
; PRESETS
Source: B:\Dropbox (Krotos)\Development\Engines\Presets\Release\Windows\*;  DestDir: C:\ProgramData\Krotos\Igniter\Presets\main; Flags: ignoreversion recursesubdirs; Permissions: everyone-modify

; ASSETS
Source: B:\Dropbox (Krotos)\Development\Engines\Assets\Release Assets\*;  DestDir:C:\ProgramData\Krotos\Igniter\Library\Factory Assets; Flags: ignoreversion recursesubdirs; Permissions: everyone-modify

; DATABASE
Source: B:\Dropbox (Krotos)\Development\Engines\Database\*.db; DestDir:C:\ProgramData\Krotos\Igniter\Databases; Flags: ignoreversion recursesubdirs; Permissions: everyone-modify

[Dirs]
Name:   "C:\Program Files\Common Files\Avid\Audio\Plug-Ins\Igniter.aaxplugin"; Attribs: system 

[Code]
var
  CopyDirPage: TInputDirWizardPage;
  AnalyticsPage: TInputOptionWizardPage;

procedure AnalyticsActivate(Sender: TWizardPage);
begin
  WizardForm.NextButton.Enabled := False;
end;

procedure AnalyticsRadioChanged(Sender: TObject);
begin
  WizardForm.NextButton.Enabled := True;
end;

procedure InitializeWizard();
begin
  CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select where to save the VST file', '',  '', False, '');
  CopyDirPage.Add('VST directory:');
  CopyDirPage.Values[0] := 'C:\Program Files\Steinberg\VstPlugins'

  AnalyticsPage := CreateInputOptionPage(wpInfoBefore, 'Usage Analytics','', 'Help us to continue creating innovative plugins by allowing us to collect anonymous usage data. We only collect information on how you use Krotos plugins, and do not use your personal data.'+#13#10+#13#10+'Participation is voluntary, and you may opt in now.'+#13#10+#13#10, true, false);
  AnalyticsPage.Add('I agree to share my anonymous plugin usage data.');
  AnalyticsPage.Add('I do not agree to share my anonymous plugin usage data.');
  AnalyticsPage.OnActivate := @AnalyticsActivate;
  AnalyticsPage.CheckListBox.OnClickCheck := @AnalyticsRadioChanged;
end;

function CopyDir(Params: string): string;
begin
  Result := CopyDirPage.Values[0];
end;

Procedure CurStepChanged(CurStep: TSetupStep);
begin
  if CurStep = ssPostInstall then
  begin
    if AnalyticsPage.Values[0] = True then
    begin
      SaveStringToFile('C:\ProgramData\Krotos\Igniter\analytics.dat','',False);
    end;
  end;
end;