#!/usr/bin/env python
##############################################
# Script: validate_plugin.py
# Description: runs pluginval on the given plugin
# Usage: validate_plugin.py /path/to/plugin.vst3
#############################################
import subprocess
import argparse
import platform
import os
parser = argparse.ArgumentParser()
parser.add_argument("pluginpath", help="Path to the plugin to be validated")
args = parser.parse_args()

# Get validator path
pluginvalpath=os.path.dirname(os.path.abspath(__file__))
if (platform.system()=="Windows"):
    pluginvalpath=os.path.join(pluginvalpath,"win\pluginval.exe")
elif(platform.system()=="Darwin"):
    pluginvalpath=os.path.join(pluginvalpath,"mac/pluginval.app/Contents/MacOS/pluginval")

print(pluginvalpath)

# Get plugin path
pluginpath=args.pluginpath
if not os.path.exists(pluginpath):
    print("Error. Plugin does not exist: " + pluginpath)
    quit(1)
if str.endswith(pluginpath,"aaxplugin"):
    print("Error. pluginval cannot test aaxplugins")
    quit(0)
print ("Validating plugin: " + pluginpath)
    
# Run validation
returnval = subprocess.call([pluginvalpath,"--validate",pluginpath,"--verbose", "--timeout-ms", "10000", "--skip-gui-tests"])
if not returnval == 0:
    print("Error code:" + str(returnval))
quit(returnval)
